---
layout: post
title: "Educational resources"
author: "Leonardo Leite"
permalink: /educational-resources/
lang: en
lang-ref: educational-resources
---

## Our educational resources on DevOps

This list is a collection of posts of our blog that can potentially be the input for contents of a course on DevOps. Its order is just the reverse chronological publication order.

* (Class) [DevOps seminar - DevOps definition, continuous delivery, and organizational structures]({{ site.baseurl }}/assets/files/devops-seminar-louisiana2024.pdf){:target="_blank"} (presented for students at the Louisiana State University)
* (Class 🇧🇷) [Short presentation about DevOps]({{ site.baseurl }}/assets/files/seminario-devops-ufc-2024-05-07.pdf){:target="_blank"} (for seminars with more discussion time)
* (Definition) [Operations roles according to Eoin Woods]({{ site.baseurl }}{% post_url en/2024-04-22-woods-roles-definition %}.html){:target="_blank"}
* (Text - research results) [The implications of the different ways of organizing development and infrastructure staffs]({{ site.baseurl }}{% post_url en/2022-04-06-strong-codes %}.html){:target="_blank"}
* (Class 🇧🇷) [Métodos Empíricos de Desenvolvimento de Software]({{ site.baseurl }}/assets/files/seminario-metodos-empiricos-unicamp-2023-09-01.pdf){:target="_blank"} (i.e., métodos ágeis, software livre e DevOps)
* (Text) [DevOps and Marx: culture will not transform your organization]({{ site.baseurl }}{% post_url en/2022-02-20-devops-culture-and-marx %}.html){:target="_blank"}
* (Text - research results) [Interaction patterns for platform teams]({{ site.baseurl }}{% post_url en/2022-01-31-interaction-patterns-for-platform-teams %}.html){:target="_blank"}
* (Pointers) [Recommended readings on platform teams]({{ site.baseurl }}{% post_url en/2022-01-03-platform-teams-readings %}.html){:target="_blank"}
* (Text - research results) [Platform teams dilemmas]({{ site.baseurl }}{% post_url en/2021-10-31-the-platform-teams-dilemma %}.html){:target="_blank"}
* (Definition) [What is DevOps? Our DevOps definition]({{ site.baseurl }}{% post_url pt-br/2021-08-18-devops-definition %}.html){:target="_blank"}
* (Text) [Adam Smith and the work organization in the software production]({{ site.baseurl }}{% post_url en/2021-08-12-adam-smith-pins %}.html){:target="_blank"}
* (Text - research results) [A digest of organizational structures in quest for continuous delivery]({{ site.baseurl }}{% post_url en/2021-06-20-structures-digest %}.html){:target="_blank"}
* (Class 🇧🇷) [From commit to production, continuous integration and continuous delivery in the deployment pipeline]({{ site.baseurl }}{% post_url pt-br/2021-03-24-apresentacao-pipeline %}.html){:target="_blank"}
* (Definition) [The ops in DevOps: operators?]({{ site.baseurl }}{% post_url pt-br/2021-03-16-ops-in-devops %}.html){:target="_blank"}
* (Class 🇧🇷) [Class about DevOps and Software Architecture in the Lab XP]({{ site.baseurl }}{% post_url pt-br/2020-08-26-devops-e-arquitetura %}.html){:target="_blank"}
* (Class 🇧🇷) [DevOps at the Laboratory of Complex Computational Systems]({{ site.baseurl }}{% post_url pt-br/2020-04-16-aula-sistemas-complexos %}.html){:target="_blank"}
* (Text - research results) [The DevOps conceptual maps]({{ site.baseurl }}{% post_url en/2020-01-02-devops-conceptual-maps %}.html){:target="_blank"}
* (Definition) [Delivery Performance]({{ site.baseurl }}{% post_url en/2019-11-27-delivery-performance %}.html){:target="_blank"}
* (Quick note) [100% of utilization increases lead time]({{ site.baseurl }}{% post_url en/2019-11-27-100pc-utilization %}.html){:target="_blank"}
* (Pointers) [DevOps-related readings we highly recommend]({{ site.baseurl }}{% post_url en/2019-11-06-devops-readings %}.html){:target="_blank"}

🇧🇷 = only in Portuguese
