---
layout: post
title: "About"
author: "Leonardo Leite"
permalink: /about/
lang: en
lang-ref: about
---

## Research

This blog mostly reports on Leonardo Leite's doctoral research 
about how software-producing organizations have structured the 
division of labor and the interaction between development and infrastructure professionals.
We approached this research by interviewing industry professionals.

We expect our research to help software-developing organizations 
in taking more informed decisions about organizing people for managing IT infrastructure
and operations activities.

### Blog author

* [Leonardo Leite](https://www.ime.usp.br/~leofl/){:target="_blank"}, Ph.D. (USP)

### Advisors

* [Paulo Meirelles](https://www.ime.usp.br/paulormm/){:target="_blank"}, professor at USP
* [Fabio Kon](https://www.ime.usp.br/~kon/){:target="_blank"}, full professor at USP


