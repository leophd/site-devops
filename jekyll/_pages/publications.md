---
layout: post
title: "Publications"
author: "Leonardo Leite"
permalink: /publications/
lang: en
lang-ref: publications
---

## DevOps-related publications of our research group at IME-USP

* \[Book\] Leonardo Leite, Fabio Kon, Paulo Meirelles. [How DevOps Works: Organizing People, from Silos to Platform Teams](https://www.amazon.com/How-DevOps-Works-Organizing-Platform-ebook/dp/B0DTR7SCN9/ref=sr_1_fkmr0_1){:target="_blank"}. Casa do Código. 2025.
* \[Book\] Leonardo Leite, Fabio Kon, Paulo Meirelles. [Como se faz DevOps Organizando pessoas, dos silos aos times de plataforma](https://www.casadocodigo.com.br/products/livro-como-se-faz-devops){:target="_blank"}. Casa do Código. 2024.
* Isaque Alves, Leonardo Leite, Paulo Meirelles, Fabio Kon, Carla Rocha. [Practices for Managing Machine Learning Products: A Multivocal Literature Review](https://ieeexplore.ieee.org/abstract/document/10175022){:target="_blank"}. IEEE Transactions on Engineering Management. Vol. 71, 2024.
* Jessica Díaz, Jorge Pérez, Isaque Alves, Fabio Kon, Leonardo Leite, Paulo Meirelles, Carla Rocha. [Harmonizing DevOps taxonomies — A grounded theory study](https://www.sciencedirect.com/science/article/pii/S0164121223003035){:target="_blank"}. Journal of Systems and Software. Vol. 208, 2023.
* Leonardo Leite, Alberto Marianno. <a href="https://sol.sbc.org.br/index.php/cbsoft_estendido/article/view/26042" target="_blank">Microsserviços, por que tão difícil? Um catálogo de padrões para criar serviços bons de se operar</a>. In: Anais Estendidos do XIV Congresso Brasileiro de Software: Teoria e Prática (CBSoft - Trilha da Indústria), 2023.
* Leonardo Leite. [A grounded theory of organizational structures for development and infrastructure professionals in software-producing organizations](https://www.teses.usp.br/teses/disponiveis/45/45134/tde-28062022-132626/en.php){:target="_blank"}. PhD thesis. University of São Paulo (USP), 2022.
* ⭐ Leonardo Leite, Nelson Lago, Claudia Melo, Fabio Kon, Paulo Meirelles. [A theory of organizational structures for development and infrastructure professionals](https://www.techrxiv.org/articles/preprint/A_theory_of_organizational_structures_for_development_and_infrastructure_professionals/19210347){:target="_blank"}. IEEE Transactions on Software Engineering. Vol. 49, No. 4, 2023.
* ⭐ Leonardo Leite, Gustavo Pinto, Fabio Kon, Paulo Meirelles. [The Organization of Software Teams in the Quest for Continuous Delivery: A Grounded Theory Approach](https://arxiv.org/abs/2008.08652){:target="_blank"}. Information and Software Technology, Vol. 139, 2021.
* Leonardo Leite, Fabio Kon, Paulo Meirelles. <a href="{{ site.baseurl }}/assets/files/choosing-structures-wtdsoft-camera-ready.pdf" target="_blank">Understanding context and forces for choosing organizational structures for continuous delivery</a>. In: X Workshop de Teses e Dissertações (WTDSoft), 2020.
* Leonardo Leite, Fabio Kon, Gustavo Pinto, Paulo Meirelles. [Platform Teams: An Organizational Structure for Continuous Delivery](https://dl.acm.org/doi/10.1145/3387940.3391455?cid=81413601887){:target="_blank"}. In: 6th International Workshop on Rapid Continuous Software Engineering (RCoSE), 2020.
* Leonardo Leite, Fabio Kon, Gustavo Pinto, Paulo Meirelles. <a href="{{ site.baseurl }}/assets/files/theory-software-teams-icse-2020-poster-track-camera-ready-version.pdf" target="_blank">Building a Theory of Software Teams Organization in a Continuous Delivery Context</a>. In: 42th International Conference on Software Engineering: Poster Track (ICSE), 2020.
* ⭐ Leonardo Leite, Carla Rocha, Fabio Kon, Dejan Milojicic, Paulo Meirelles.
[A Survey of DevOps Concepts and Challenges](https://dl.acm.org/doi/10.1145/3359981?cid=81413601887){:target="_blank"}.
ACM Computing Surveys, Vol. 52, No. 6, 2019.
* Rodrigo Siqueira, Diego Camarinha, Melissa Wen, Paulo Meirelles, Fabio Kon.
[Continuous Delivery: Building Trust in a Large-scale, Complex Government Organization](https://www.ime.usp.br/~kon/papers/IEEESoftware2018.pdf){:target="_blank"}. 
IEEE Software, Vol. 35, 2018.
* Leonardo Leite, Carlos Eduardo Moreira, Daniel Cordeiro, Marco Aurelio Gerosa, Fabio Kon. 
[Deploying Large-Scale Service Compositions on the Cloud with the CHOReOS Enactment Engine](https://www.ime.usp.br/~leofl/mestrado/ee_nca14.pdf){:target="_blank"}. 
In: 13th IEEE International Symposium on Network Computing and Applications (NCA), 2014.
* Daniel Cukier (precursor). 
[DevOps Patterns to Scale Web Applications Using Cloud Services](https://www.researchgate.net/profile/Daniel_Cukier/publication/262404654_DevOps_patterns_to_scale_web_applications_using_cloud_services/links/546231fc0cf2c0c6aec1aa85.pdf){:target="_blank"}. 
In Proceedings of the 2013 Companion Publication for Conference on Systems, Programming, & Applications: Software for Humanity (SPLASH ’13). ACM. 2013
