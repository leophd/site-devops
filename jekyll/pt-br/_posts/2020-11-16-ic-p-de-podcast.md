---
layout: post
title: "Conversando sobre Integração Contínua no p de Podcast"
author: "Leonardo Leite"
lang: pt-br
lang-ref: ic-p-de-podcast
hiden: false
published: true
---

No dia 9 de outubro, tive o prazer de conversar sobre Integração Contínua com o Marcio Frayze e o Julianno Martins no p de Podcast, um podcast sobre arquitetura de software.

![Logo do p de Podcast]({{ site.baseurl }}/assets/figures/p-de-podcast.png)

<br/>
<center>
<a href="https://open.spotify.com/episode/7ydeQwAw9Kk8HeMl03699o" class="button" target="_blank">Acesse via Spotify!</a>
</center>
<br/>

<br/>
<center>
<a href="https://anchor.fm/pdepodcast/episodes/Integrao-Contnua-no--apenas-automatizar-a-build-ekqsv2/a-a3g73p0" class="button" target="_blank">Acesse via Anchor!</a>
</center>
<br/>

Dentre outros tópicos, abordamos em nossa conversa:

* Definição
* Execução síncrona e assíncrona da integração contínua
* Tempo de execução da build
* Feature toggle
* Monolitos e integração contínua
* Feature branch 
* Pull requests (cenário open source)
* Banco de dados e integração contínua (Flyway!)
* Discussão de caso (Radar Parlamentar): com dependência de serviço externo na pipeline, se serviço fora do ar, falhar ou não a build? 



