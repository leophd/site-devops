---
layout: post
title: "As top 3 ferramentas DevOps e desenvolvedores"
author: "Leonardo Leite"
lang: pt-br
lang-ref: top-3-devops-tools-and-devs
hiden: false
published: true
---

No último post, recomendei às pessoas "seguindo a trilha de infraestrutura" se concentrarem nas seguintes ferramentas DevOps: Kubernetes, Kafka e Terraform. Mas e os desenvolvedores? Também deveriam aprender essas ferramentas?

![Logos of Kubernetes, Kafka, and Terraform]({{ site.baseurl }}/assets/figures/top-3-tools.png)

<p><center><i>
Figure 1 - As top 3 ferramentas DevOps 
</i></center></p>

Claro que isso depende muito do tipo de organização na qual você trabalha e do tipo de desenvolvedor que você é ou deseja ser. Para este post, assumirei que sua empresa tem um pessoal de infra para instalar, configurar e gerenciar software de infraestrutura, como o Kubernetes e o Kafka. Por outro lado, vou considerar que você faz mais do que "apenas codificar" e que, pelo menos, está preocupado com arquitetura de software.

## Kubernetes

O Kubernetes tem transformando o mundo DevOps. Parece que a maioria das equipes de infraestrutura operando como [times de plataforma]({{ site.baseurl }}{% post_url en/2020-03-17-times-de-plataforma %}.html){:target="_blank"} estão usando Kubernetes. E parece que mais mudanças estão vindo do Kubernetes com a promessa de aplicar seu "plano de controle" a outros recursos de infraestrutura além de contêineres (confira o [projeto Crossplane](https://containerjournal.com/kubeconcnc/kubernetes-true-superpower-is-its-control-plane/){:target="_blank"}).

Portanto, acredito que os desenvolvedores devem entender o que é o Kubernetes, o que ele faz e por que ele é tão especial. Provavelmente não há necessidade de aprender como instalá-lo e configurá-lo. Aprender seus comandos para criar pods pode ser útil, mas talvez não seja crucial, uma vez que esse acesso pode ser abstraído por outras camadas, como plataformas corporativas ou mesmo o Terraform.

## Terraform

O Terraform é uma forma declarativa de especificar a infraestrutura a ser provisionada. O Terraform tem vários providers para criar diferentes tipos de infraestrutura em vários fornecedores de nuvem. Ainda é possível pra uma empresa criar providers personalizados para criar infraestrutura local (não na nuvem). Em ambas as situações, um caso de uso interessante é quando um grupo de infraestrutura especializado define templates de Terraform de uso permitido na organização. É uma forma de disciplinar o uso da nuvem pública, minimizando a variação arquitetural entre as equipes. Nesse contexto, talvez os desenvolvedores não precisem criar templates de Terraform do zero. Ainda assim, eles se beneficiarão 1) em compreender por cima a arquitetura do Terraform e 2) serem capazes de ler um template de Terraform e, assim, compreender o que se passa (ou seja, entender onde a aplicação é executada).

## Kafka

Talvez saber instalar e gerenciar o Kafka não seja necessário para um desenvolvedor. No entanto, eu diria que é fundamental para os desenvolvedores entenderem quando uma arquitetura publisher/subscriber ou orientada a eventos é uma boa. Tais abordagens podem trazer um importante desacoplamento entre domínios de um sistema (ou seja, inversão de dependência). E, a princípio, o Kafka é apenas uma peça de infraestrutura para concretizar essa visão - é o broker de publisher/subscriber. No entanto, pelo o que eu entendi, o Kafka traz algumas vantagens de escalabilidade significativas em comparação com outros sistemas de mensageria.

Depois de decidir usar o Kafka (a parte difícil), os desenvolvedores devem aprender como interagir com o Kafka em alguma linguagem de programação para postar e ler mensagens. O que pode não ser fácil é que alguns ajustes (tuning) podem depender de requisitos de negócio, o que significa que esse tuning dificilmente pode ser deixado apenas pro pessoal de infra. Isso significa que mesmo que os desenvolvedores não saibam como tunar o Kafka, pode ser relevante para eles aprenderem as opções de tuning e quando aplicá-las. Um exemplo de tipo de tuning é o de garantias de entrega.

Se você acha que tem algo errado neste post, comenta lá no Twitter ([@leonardofl](https://twitter.com/leonardofl){:target="_blank"}) por favor!

