---
layout: post
title: "Fique esperto: pegadinha total do Grafana"
author: "Leonardo Leite"
lang: pt-br
lang-ref: grafana-trick
hiden: false
published: true
---

Este blog é mais sobre [minha pesquisa]({{ site.baseurl }}/publications){:target="_blank"} em estruturas organizacionais em DevOps. Contudo, acho bom também persistir aqui algumas epifanias relacionadas a tecnologias DevOps (exemplo: [post sobre o Blunder]({{ site.baseurl }}{% post_url pt/2019-11-18-bundler %}.html){:target="_blank"}). Neste post falarei de um detalhe capcioso sobre o Grafana. Se liga!

Considere a seguinte configuração de um painel do Grafana.

![Configuração de um painel Grafana, usando uma métrica counter e com a chave "Total" ligada na configuração da legenda do gráfico]({{ site.baseurl }}/assets/figures/health-alive-conf.png)

<center><i>
Figura 1 - Configuração de um painel Grafana utilizando uma métrica de contador do Prometheus
</i><p/></center>

O que vamos entender direitinho neste post é aquela configuração "Total" que está ativada no painel lateral. Mas vamos com calma.

Esse painel acessa dados de uma instância do Prometheus, mostrando no gráfico dados relativos ao volume de acesso a um determinado serviço, sendo que cada série do gráfico representa uma combinação de cliente e recurso acessado, sendo cliente aqui definido pelo IP do cliente e um nome do cliente, enquanto que recurso tem relação com à URL acessada. A expressão em PromQL (query do Prometheus) utilizada é a seguinte:

```sum(increase(counter_requisicoes{ambiente="pro"}[1m])) by (clientName, recurso, clientIp)```

E o que isso quer dizer? Importante... isso quer dizer que cada ponto do gráfico representa o incremento na quantidade de requisições no último minuto para um determinado grupo (cliente X recurso), isso tudo no ambiente de produção. Chamaremos esse "1 minuto" (especificado pelo "1m") de janela da média móvel. [Um tutorial bem bacana que explica sobre contadores no Prometheus e as queries correspondentes é esse aqui](https://www.innoq.com/en/blog/prometheus-counters/){:target="_blank"}. Bom, veja o resultado na figura abaixo.

![Painel Grafana correspondente à configuração explicada acima]({{ site.baseurl }}/assets/figures/health-alive.png)

<center><i>
Figura 2 - Painel Grafana em ação utilizando a configuração anteriormente apresentada
</i><p/></center>

No gráfico apareceriam várias séries (linhas com dados), conforme a pré-visualização visível na Figura 1. Contudo, na Figura 2 eu cliquei na legenda de uma das séries. Note que para cada grupo temos uma série no gráfico e, correspondentemente, uma legenda da série. Ao clicar na legenda da série, o Grafana mostra somente a série correspondente. Ao lado da descrição de cada legenda de série é possível associar um valor sumarizando os dados exibidos. Conforme opções no menu lateral, essas opções são: min, max, avg, current e total. O significado da maioria desses valores é bem evidente... se no gráfico eu tenho 20 pontos (que foi o que aconteceu comigo), min, por exemplo, mostrará o menor desses valores; max, por outro lado, mostrará o maior desses valores. Mas e o **total**, o que significa? Bom, total é a soma dos valores (poderia se chamar sum, né?). Mas tem um detalhe aí...

Agora vem a **pegadinha** para os desavisados. Se você seguiu com atenção, talvez não vá cair nessa, mas eu caí, então vamos lá... veja que o gráfico mostra a evolução de nossa medida nos últimos 5 minutos. Agora vem a interpretação precitada: o valor total seria o incremento na quantidade de requisições daquele grupo nos últimos 5 minutos.

Então sejamos enfáticos: **o valor total, sumarizando a série, não é o incremento na quantidade de requisições daquele grupo nos últimos 5 minutos.**

E o que é? É o que discutimos... é a soma dos valores de todos os pontos no gráfico! E o que isso significa? E aí que tá... em termos absolutos, não representada nada de interessante (embora em termos relativos possa até ser útil para comparar momentos diferentes no gráfico).

Vamos aos detalhes. Os pontos na Figura 2 apresentam os seguintes valores: 2,4 3,6 3,6 3,6 2,4 3,6 3,6 3,6 2,4 3,6 3,6 3,6 2,4 3,6 3,6 3,6 2,4 3,6 3,6 3,6. Somando-se esses valores temos 66, que é bem próximo do 69.6 mostrado no gráfico (lembre-se, nunca espere valores exatos no Grafana, há várias questões de arredondamentos e interpolações rolando aí).

Vamos agora nos concentrar nos dois primeiros pontos. Lembre-se, cada ponto representa o incremento nas requisições no tempo compreendido entre o ponto e 1 minuto pra trás daquele ponto. Se temos 20 pontos em 5 minutos, entre dois pontos temos um intervalo de ~5*60/(20-1)= ~16seg. Considere agora o seguinte esquema:

```
                     P1      P2
|---------60s--------|--16s--|
|--16s--|---------60s--------|
```

Repara, repara... o valor em P1 é o incremento de seu último minuto. O valor em P2 é o incremento de seu último minuto, o que inclui parte do tempo considerado pelo P1. Dito de outra forma, em P1 temos o acúmulo das requisições que aconteceram entre o tempo 0 e o tempo 60, enquanto que em P2 temos o acúmulo das requisições que aconteceram entre o tempo 16 e o tempo 16+60. Dessa forma, note que as requisições ocorridas entre o tempo 16 e o tempo 60 serão contabilizadas tanto no valor de P1, quanto no valor de P2. Então, se fizermos P1+P2, estamos contando esses valores duas vezes! Ou seja, a soma P1+P2 não representa o incremento das requisições nos últimos 32 segundos (32 = 2*16, sendo 16 o intervalo entre os pontos).

Portanto, repetimos: **o valor total, sumarizando a série, é a soma dos valores exibidos, o que difere do incremento na quantidade de requisições daquele grupo nos últimos 5 minutos.**

## Tá, o que eu faço então?

Bom, talvez a melhor opção seja não utilizar nenhum sumarizador na legenda da série. No fim, os outros sumarizadores também requerem alguma atenção. Mas o mais tranquilo talvez seja o current, que exibiria o incremento nas requisições no último minuto, correspondendo ao último ponto no gráfico. Contudo, só o último ponto raramente é de algum interesse, um ponto sozinho quase nunca é interessante... interessa mais o comportamento recente, os último pontos. Por isso mesmo, min e max também não são tão interessantes, principalmente o min. Caso queira usar o max, nos lembremos: iremos ver o incremento obtido no minuto com maior incremento dentro do período analisado.

Por fim, vamos ao avg, que é a média. Esse parece um sumarizador mais interessante. Mas primeiro que a média sempre tem seus perigos. Em regime estacionário de uso, com poucas flutuações, OK, a média pode ser interessante. Mas se analisarmos um dia inteiro... normalmente o período comercial tem muito mais requisições do que a madrugada. Nesse caso, talvez a média não tenha muito significado. Mas caso adote a média, lembre-se: média do que? Então, essa média é o incremento médio por minuto considerando cada minuto analisado.

Ah, ao longo da explicação, para simplificar o texto, falei sempre em "minuto", mas onde eu disse minuto entenda a janela da média móvel, que é especificada na expressão PromQL. Terminando... note que o tamanho dessa janela fica escondido do usuário do painel. Então para que ele possa entender o sumarizador, é bom destacar algo no título do gráfico. Exemplo: caso a legenda da série exponha o valor médio (avg), "Quantidade de requisições por minuto" ajuda mais do que "Quantidade de requisições".

Caso note algum equívoco neste post, agradeço se me avisar via [Twitter (@leonardofl)](https://twitter.com/leonardofl){:target="_blank"}.


