---
layout: post
title: "Lançamento do livro no Google Campus for Startups!"
author: "Leonardo Leite"
lang: pt-br
lang-ref: book-launch
hiden: false
---

Evento de Lançamento do livro mais aniversário de 12 anos da editora Casa do Código no Google Campus for Startups em São Paulo no dia 11/04/2024.

Tivemos a abertura com o Sr. Carlos da Casa do Código e na sequência um debate com ilustres convidados:

* Alexandre Freire (diretor de engenharia no Google)
* Sérgio Lopes (CTO e cofundador da Alura)
* Fabricia Doria (ex-head de TI na banQi)
* Leonardo Martins (gerente de engenharia na Pluxee)

Também estiveram presentes outros autores de livros da Casa do Código. 


![]({{ site.baseurl }}/assets/figures/lancamento-livro1.jpeg)

<p><center><i>
Figura 1 - Autores do livro Como Se faz DevOps junto dos debatedores da noite
</i></center></p>

------------------------

![]({{ site.baseurl }}/assets/figures/lancamento-livro2.jpeg)

<p><center><i>
Figura 2 - Leonardo fazendo um brevíssimo resumo do livro
</i></center></p>

------------------------

![Auditório para 100 pessoas quase cheio]({{ site.baseurl }}/assets/figures/lancamento-livro3.jpeg)

<p><center><i>
Figura 3 - Público presente
</i></center></p>

------------------------

![Fabio fala no microfone em pé, enquanto debatedores estão sentados]({{ site.baseurl }}/assets/figures/lancamento-livro4.jpeg)

<p><center><i>
Figura 4 - Debate rolando
</i></center></p>

------------------------

![]({{ site.baseurl }}/assets/figures/lancamento-livro5.jpeg)

<p><center><i>
Figura 5 - Leonardo e Fabio com o Sr. Carlos da Casa do Código
</i></center></p>

------------------------

![]({{ site.baseurl }}/assets/figures/lancamento-livro6.jpeg)

<p><center><i>
Figura 6 - Leonardo com Vivian, a responsável pela edição do livro Como se faz DevOps
</i></center></p>

------------------------

![]({{ site.baseurl }}/assets/figures/lancamento-livro7.jpeg)

<p><center><i>
Figura 7 - Leonardo com Gabriela, a primeira a pegar autógrafo!
</i></center></p>

------------------------

![]({{ site.baseurl }}/assets/figures/lancamento-livro8.jpeg)

<p><center><i>
Figura 8 - Leonardo autografando
</i></center></p>

------------------------

![Sacola, copo de café, bloco com post-its, caneta e marcador de página da Casa do Código; e mais um marcador de página personalizado (no qual está escrito "Dante")]({{ site.baseurl }}/assets/figures/lancamento-livro9.jpeg)

<p><center><i>
Figura 9 - Brindes da noite para os presentes
</i></center></p>

------------------------

![]({{ site.baseurl }}/assets/figures/lancamento-livro10.jpeg)

<p><center><i>
Figura 10 - Livros da Casa do Código
</i></center></p>

