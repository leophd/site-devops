---
layout: post
title: "As top 3 ferramentas DevOps"
author: "Leonardo Leite"
lang: pt-br
lang-ref: top-3-devops-tools
hiden: false
published: true
---

Neste mundo, existem muitas ferramentas DevOps. É impossível aprender todas elas. Então, "quais devo priorizar para estudar?", você pode perguntar.

![A Tabela Periódica DevOps da XebiaLabs com um emoji pensativo gigante; na tabela, é como se cada ferramenta DevOps correspondesse a um elemento químico.]({{ site.baseurl }}/assets/figures/which-devops-tools.png)

<p><center><i>
Figure 1 - A Tabela Periódica DevOps da XebiaLabs
</i></center></p>


Bom, pensando nas ferramentas relacionadas ao provisionamento de infraestrutura, tenho um conselho embasado...

Na minha pesquisa de doutorado, já conduzi várias entrevistas sobre DevOps com profissionais de TI (tanto de desenvolvimento quanto de infraestrutura). Até agora, entrevistei 65 profissionais em 45 empresas (em 8 países diferentes, com tamanhos diferentes, em vários domínios de negócios). O foco dessas entrevistas está nas interações humanas, não em ferramentas. No entanto, algumas discussões sobre ferramentas surgem.

Nesse contexto, as três ferramentas DevOps mais discutidas que desempenham um papel transformador nas organizações foram:

* Kubernetes

* Kafka

* Terraform

![Logotipos do Kubernetes, do Kafka, e do Terraform]({{ site.baseurl }}/assets/figures/top-3-tools.png)

<p><center><i>
Figure 2 - As top 3 ferramentas DevOps
</i></center></p>


Portanto, se você deseja trilhar o caminho da infraestrutura, estudar essas três tecnologias traz as maiores chances de retorno sobre o investimento!




