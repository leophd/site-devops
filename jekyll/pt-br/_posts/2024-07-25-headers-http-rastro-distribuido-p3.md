---
layout: post
title: "Headers HTTP para rastro distribuído (observabilidade) - Parte 3: Grafana Tempo"
author: "Leonardo Leite"
lang: pt-br
lang-ref: http-headers-distributed-tracing-pt3
hiden: false
---

Esta série de posts investiga os **headers HTTP** usados para apoiar o **rastro distribuído** (*tracing*), que é um dos pilares da **observabilidade** (composta também por logs e métricas). Embora foquemos na investigação sobre esses headers, estes posts servem também como uma introdução sobre rastro distribuído e como apresentação de algumas alternativas tecnológicas para sua implementação. Como valor agregado extra, destrinchamos e interpretamos a documentação de algumas dessas alternativas; documentação essa, francamente, não tão fácil de assimilar. Neste post analisaremos o Grafana Tempo, uma solução de visualização de rastro distribuído integrada com o já altamente popular Grafana.

## Contextualizando

O **rastro distribuído** (*tracing*) diz respeito aos processos, padrões e ferramentas que possibilitam que a equipe de desenvolvimento compreenda as requisições feitas a partir da invocação a um determinado serviço, dando a possibilidade de correlacionar erros ocorridos em diferentes serviços. Exemplo: entender que o erro interno no serviço A foi devido a um determinado problema no serviço B (sendo o serviço B invocado pelo serviço A).

Se essa recontextualização não lhe foi o suficiente, considere rever as seções iniciais de nosso [primeiro post da série]({{ site.baseurl }}{% post_url pt-br/2023-12-05-headers-http-rastro-distribuido-p1 %}.html){:target="_blank"} (i.e., as seções antes de iniciarmos a descrição sobre o Spring Cloud Sleuth).

## Grafana Tempo

E eis que em 2021 é lançado mais um projeto relacionado a rastro distribuído, o Grafana Tempo. Segundo a página inicial de seu web site [1]:

<span class="citacao">Grafana Tempo is an open source, easy-to-use, and high-scale distributed tracing backend.</span>

Como em nossa equipe utilizamos bastante a popular dobradinha Prometheus + Grafana, resolvi explorar um pouco essa opção também. Outro fator motivador é que a plataforma interna de infraestrutura de minha empresa fornece um addon para o Grafana Tempo.

A página inicial do web site diz ainda que <span class="citacao">Tempo can ingest common open source tracing protocols, including Jaeger, Zipkin, and OpenTelemetry</span> e ainda exibe o seguinte diagrama:

![Grafana Tempo no centro. Jaeger, OpenTelemetry e outros "open source tracing protocols" apontam para Tempo. Setinha dupla entre Tempo e Amazon S3 (e outras ferramentas de storage). Tempo aponta para Grafana (visualização).]({{ site.baseurl }}/assets/figures/tempo1.png)

<p><center><i>
Figura 1 - Diagrama de dependência entre o Grafana Tempo e outros sistemas. Fonte: https://grafana.com/oss/tempo/
</i></center></p>

Nesse ponto, surgem dúvidas: então o Grafana Tempo precisa puxar as dados dos traços de um outro sistema como o OpenTelemetry? Ou pode funcionar no lugar desses sistemas? A página inicial sugere que o foco do Tempo é armazenamento dos dados. Mas somente no webinar disponibilizado no site do Grafana Tempo [2] é que as coisas ficam mais claras, principalmente graças ao diagrama abaixo:

![1. Client instrumentation - Application, instrumentation layer; 2. Pipeline (optional) - agent; 3. Backend - Tempo; 3. Visualization - Grafana. "Flow of spans" flui da esquerda para a direita.]({{ site.baseurl }}/assets/figures/tempo2.png)

<p><center><i>
Figura 2 - Etapas do processo de rastro com o Grafana Tempo
</i></center></p>

Na figura vemos que a instrumentação da aplicação para o envio dos dados de rastro está fora do escopo do Tempo (porém há um componente opcional do Tempo, o coletor, que roda com a aplicação para fazer um buffer desse envio para otimização de desempenho). E, dessa forma, a propagação de contexto (assunto principal desta série de posts) também está fora do escopo do Tempo.

O ponto principal do Tempo é o armazenamento dos dados de rastro. Assim, esses dados passam a ser facilmente recuperados para a utilização em paineis do Grafana. Segue dois exemplos de visualizações no Grafana, sendo a primeira a mais típica para informações de rastro, e a segunda uma visão sobre a topologia da aplicação.

![Gráfico cheio de barrinhas, nome de serviços e tempos de resposta no Grafana.]({{ site.baseurl }}/assets/figures/tempo3.png)

<p><center><i>
Figura 3 - Visualização na qual se vê o tempo gasto em cada serviço para uma determinada transação distribuída
</i></center></p>

![Gráfico com circunferências representando serviços e com setas de dependências entre essas circunferências.]({{ site.baseurl }}/assets/figures/tempo4.png)

<p><center><i>
Figura 4 - Diagrama arquitetural (dependência entre serviços) construído automaticamente com base nos dados de rastro
</i></center></p>

Uma grande inovação do Tempo é possibilitar o acesso aos dados de rastro por meio de uma linguagem de consulta específica, a TraceQL. Segue alguns exemplos ilustrativos do uso dessa linguagem. 

![Um exemplo: { duration > 2s }]({{ site.baseurl }}/assets/figures/tempo5.png)

<p><center><i>
Figura 5 - Exemplos de TraceQL; um dos exemplo interessantes é a consulta '{.service.name = "foo"} >> {.service.name = "bar"}', que obtém as chamadas do serviço "foo" para o serviço "bar".
</i></center></p>

Outro aspecto muito interessante é a integração entre os conceitos de métricas e rastro. Com o Tempo é possível obter exemplos de requisições ("examplars") para um determinado ponto em um painel exibindo alguma métrica, conforme exemplo da figura abaixo.

![À direita gráfico com alguns pontos, eixo X: tempo, eixo y: duration. À esquerda vários dados, como traceID, instance, deployment_environment, service, spand_name etc.]({{ site.baseurl }}/assets/figures/tempo6.png)

<p><center><i>
Figura 6 - Ilustração dos examplars - detalhes de uma requisição correspondente a um ponto no gráfico de métricas (cada ponto tipicamente corresponde a um agregado de requisições)
</i></center></p>

Ah, repare como na figura acima se utiliza o termo "trace ID" para identificar o rastro de uma execução de uma transação distribuída. Destaco isso por ser o assunto desta série de posts.



## Conclusão

O Grafana Tempo parece ser uma opção promissora para a visualização de rastros de execuções de transações distribuídas. Só pelo fato de se integrar ao popular Grafana já seria uma alternativa a se considerar. Mas o Tempo traz ainda inovações interessantes, principalmente a linguagem de consulta de rastro (TraceQL). Contudo, o uso do Tempo ainda deve ser conjugado com outra ferramenta de rastro para fazer a coleta dos dados na aplicação, dados esses que devem ser enviados ao backend do Tempo.

## PS posterior (19/09/2024) - Spring Boot 3

Observability with Spring Boot 3 [3]:

<span class="citacao">The Spring Observability Team has been working on adding observability support for Spring Applications for quite some time, and we are pleased to inform you that this feature will be generally available with Spring Framework 6 and Spring Boot 3!</span>

Nesta página [3] há um tutorial que mostra como usar de forma integrada várias ferramentas de observabilidade com o Spring Boot, sendo o Tempo incluso aí.


-------------------

## Referências

[1] [Página inicial do web site do Grafana Tempo](https://grafana.com/oss/tempo/){:target="_blank"}

[2] [Webinar Introduction to Grafana Tempo, a high-scale distributed tracing backend](https://grafana.com/go/webinar/getting-started-with-tracing-and-grafana-tempo/){:target="_blank"}

[3] Observability with Spring Boot 3 [https://spring.io/blog/2022/10/12/observability-with-spring-boot-3](https://spring.io/blog/2022/10/12/observability-with-spring-boot-3){:target="_blank"}

<!--
[3] https://http.dev/x-request-id

[4] https://en.wikipedia.org/wiki/List_of_HTTP_header_fields

[5] https://developer.mozilla.org/pt-BR/docs/Web/HTTP/Headers
-->
