---
layout: post
title: "Nossa teoria sobre estruturas organizacionais de equipes de software acaba de ser publicada!"
author: "Leonardo Leite"
lang: pt-br
lang-ref: ist-publication
---

Temos o prazer de anunciar que nosso artigo **The Organization of Software Teams in the Quest for Continuous Delivery: A Grounded Theory Approach** [foi publicado na Information and Software Technology](https://www.sciencedirect.com/science/article/pii/S0950584921001324){:target="_blank"}, uma revista muito conceituada em nosso campo de pesquisa ([Qualis](https://pt.wikipedia.org/wiki/Qualis){:target="_blank"} A1).

<a href="https://www.sciencedirect.com/science/article/abs/pii/S0950584921001324" target="_blank">
![Screenshot do site ScienceDirect com a publicação oficial]({{ site.baseurl }}/assets/figures/ist-publication.png)
</a>

<p><center><i>
Figura 1 - Nosso artigo publicado pela Elsevier.
</i></center></p>

Muitas organizações desejam adotar DevOps. No entanto, geralmente, o significado de DevOps é confuso: seria sobre como tornar os desenvolvedores e o pessoal de infraestrutura mais próximos? Seria sobre equipes multifuncionais? Seria sobre a criação de uma equipe DevOps? Bem, nós atacamos essa problemática observando a prática atual da indústria.

Este estudo investiga como diferentes organizações produtoras de software estruturam suas equipes de desenvolvimento e infraestrutura, ou mais precisamente: como é a divisão do trabalho entre esses grupos e como eles interagem. Depois de analisar cuidadosamente os dados coletados em 44 entrevistas com profissionais de software, identificamos quatro estruturas organizacionais comuns: departamentos em silos, DevOps clássico, equipes multifuncionais e times de plataforma.

![Diagrama apresentando as quatro estruturas organizacionais; diagrama com caixas e setas.]({{ site.baseurl }}/assets/figures/taxonomy-ist.png)

<p><center><i>
Figura 2 - Visão de alto nível de nossa taxonomia: estruturas organizacionais descobertas e suas propriedades complementares
</i></center></p>

A seguir, fornecemos resumidamente para cada estrutura descoberta: (i) a diferenciação entre os grupos de desenvolvimento e infraestrutura em relação às atividades operacionais (implantação, configuração da infraestrutura e operação do serviço em tempo de execução); e (ii) como esses grupos interagem (integração).

* <strong>Departamentos em silos</strong>
  * <strong>Diferenciação do desenvolvimento:</strong> apenas constrói o pacote da aplicação
  * <strong>Diferenciação da infraestrutura:</strong> responsável por todas as atividades operacionais
  * <strong>Integração:</strong> colaboração limitada entre os grupos
  * <strong>Também chamada de:</strong>* departamentos de dev & infra segregados

* <strong>DevOps clássico</strong>
  * <strong>Diferenciação do desenvolvimento:</strong> participa / colabora em algumas atividades operacionais
  * <strong>Diferenciação da infraestrutura:</strong> responsável por todas as atividades operacionais
  * <strong>Integração:</strong> colaboração intensa entre os grupos
  * <strong>Também chamada de:</strong>* departamentos de dev & infra colaborativos

* <strong>Equipes multifuncionais</strong>
  * <strong>Diferenciação do desenvolvimento:</strong> responsável por todas as atividades operacionais
  * <strong>Diferenciação da infraestrutura:</strong> não existe
  * <strong>Integração:</strong> --
  * <strong>Também chamada de:</strong>* Departamento único dev/infra 

* <strong>Times de plataforma</strong>
  * <strong>Diferenciação do desenvolvimento:</strong> responsável por todas as atividades operacionais com o apoio da plataforma
  * <strong>Diferenciação da infraestrutura:</strong> fornece uma plataforma automatizando muitas das atividades operacionais
  * <strong>Integração:</strong> a interação acontece para situações específicas, não diariamente
  * <strong>Também chamada de:</strong>* departamentos de dev & infra mediados por API

Para melhor entender essas estruturas, você também pode conferir [o resumo da nossa taxonomia]({{ site.baseurl }}{% post_url pt/2021-06-20-resumo-estruturas %}.html){:target="_blank"} ou ler o artigo completo.

<br/>
<center>
<a href="https://arxiv.org/abs/2008.08652" target="_blank" class="button">Download do artigo completo (em inglês)</a>
</center>
<br/>

A principal contribuição deste estudo é uma teoria na forma de uma taxonomia que organiza as estruturas encontradas com suas propriedades. Essa teoria pode apoiar pesquisadores e profissionais a pensar sobre como estruturar melhor os profissionais de desenvolvimento e infraestrutura em organizações produtoras de software. A teoria também contribui para melhorar nossa compreensão sobre o fenômeno contemporâneo da produção de software.

\* O artigo acaba de ser aceito (junho de 2021), mas a submissão inicial foi há quase um ano (agosto de 2020). Durante esse longo tempo, temos trabalhado para evoluir nossa taxonomia com base em seu uso com profissionais. Esta é a razão pela qual temos duas versões para nomear cada uma de nossas estruturas. Esperamos apresentar a nova versão da taxonomia em uma publicação futura, considerando também o porquê de diferentes organizações adotarem estruturas diferentes.





