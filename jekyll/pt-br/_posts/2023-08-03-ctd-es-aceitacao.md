---
layout: post
title: "Nossa tese é finalista do Concurso de Teses e Dissertações em Engenharia de Software do CBSoft 2023!"
author: "Leonardo Leite"
lang: pt-br
lang-ref: ctd-es
hiden: false
---

Temos o prazer de anunciar que nossa tese ([A grounded theory of organizational structures for development and infrastructure professionals in software-producing organizations]({{ site.baseurl }}{% post_url en/2022-07-13-thesis-published %}.html){:target="_blank"}) foi selecionada para a próxima, e última, fase do Concurso de Teses e Dissertações em Engenharia de Software (CTD-ES 2023) do CBSoft (Congresso Brasileiro de Software: Teoria e Prática)!!!

![Logo do CBSoft 2023]({{ site.baseurl }}/assets/figures/cbsoft2023-logo.png)

<p><center><i>
Figura 1 - CBSoft 2023 - Congresso Brasileiro de Software: Teoria e Prática
</i></center></p>

O Concurso de Teses e Dissertações em Engenharia de Software (CTD-ES) visa divulgar e premiar as melhores teses de Doutorado e dissertações de Mestrado na área de Engenharia de Software concluídas, defendidas e aprovadas no Brasil no ano de 2022. Serão premiadas as teses e dissertações com contribuição significativa e destacada para a área científica de Engenharia de Software além de relevante impacto para a sociedade e para a indústria de software.

![Captura de tela de e-mail escrito em inglês]({{ site.baseurl }}/assets/figures/aceite-ctd-es.png)

<p><center><i>
Figura 2 - Comunicação da seleção enviada pelos chairs do CTD-ES
</i></center></p>

O concurso será realizado dentro do CBSoft 2023. O Congresso Brasileiro de Software: Teoria e Prática (CBSoft) é um evento realizado anualmente pela Sociedade Brasileira de Computação (SBC) com o objetivo de promover e incentivar a troca de experiências entre pesquisadores e profissionais da indústria e academia sobre as mais recentes pesquisas, tendências e inovações práticas e teóricas sobre software. Realizado desde 2010 como evento agregador de simpósios brasileiros promovidos pela SBC na área de software, o CBSoft tornou-se um dos principais eventos da comunidade científica brasileira na área de Computação.

A 14ª edição do CBSoft será realizada de 25 a 29 de setembro na Universidade Federal de Mato Grosso do Sul (UFMS), localizado em Campo Grande (MS). Ela integrará quatro eventos tradicionais realizados anualmente pela comunidade brasileira em Engenharia de Software: o XXXVII Simpósio Brasileiro de Engenharia de Software (SBES), o principal evento de Engenharia de Software na América Latina; o XXVII Simpósio Brasileiro de Linguagens de Programação (SBLP); o XVII Simpósio Brasileiro de Componentes, Arquiteturas e Reutilização de Software (SBCARS); e o VIII Simpósio Brasileiro de Teste de Software Sistemático e Automatizado (SAST).

![Foto do local do evento, a Universidade Federal do Mato Grosso do Sul (UFMS)]({{ site.baseurl }}/assets/figures/ufms.jpg)

<p><center><i>
Figura 3 - Foto da Universidade Federal do Mato Grosso do Sul (UFMS) em Campo Grande (MS)
</i></center></p>

A programação do CBSoft incluirá sessões técnicas com apresentações de artigos científicos, palestras proferidas por pesquisadores brasileiros e estrangeiros de renome nacional e internacional, painéis de discussão, workshops, e demonstrações de ferramentas. Todas essas diversas atividades de interesse da comunidade de Engenharia de Software e de áreas relacionadas ao desenvolvimento de sistemas de software direcionam-se para a difusão do conhecimento e discussão de questões importantes relacionadas à pesquisa, desenvolvimento e inovação, tanto no Brasil quanto no mundo.

