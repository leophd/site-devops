---
layout: post
title: "Melhorando a capacidade operacional sobre microsserviços - Trilha da Indústria CBSoft 2023"
author: "Leonardo Leite"
lang: pt-br
lang-ref: chastra-cbsoft-2023
hiden: false
---

Temos o prazer de anunciar que nosso artigo *Microsserviços, por que tão difícil? Um catálogo de padrões para criar serviços bons de se operar* foi aceito para apresentação na Trilha da Indústria do CBSoft 2023 (Congresso Brasileiro de Software: Teoria e Prática)!!!

![Logo do CBSoft 2023]({{ site.baseurl }}/assets/figures/cbsoft2023-logo.png)

<p><center><i>
Figura 1 - CBSoft 2023 - Congresso Brasileiro de Software: Teoria e Prática
</i></center></p>

## Sobre o artigo

Autores: Leonardo Leite e Alberto Marianno - ambos pelo Serviço Federal de Processamento de Dados (Serpro).

Resumo. *Apresentamos aqui um catálogo de padrões para aprimorar a operacionalização de serviços, o que reduz o tempo de reparo. Tal catálogo é baseado na prática de equipes do Serpro, estatal federal de tecnologia. Contudo, há um ônus considerável para se aplicar todos esses padrões. É possível também a má aplicação e o esquecimento deles. Assim, o catálogo traz reflexões sobre dificuldades em se produzir microsserviços (muitos serviços com constante atualização). Tais reflexões podem basear futuras pesquisas.*

Com o uso de [plataformas internas de infraestrutura]({{site.baseurl}}{% post_url en/2020-03-17-times-de-plataforma%}.html){:target="_ blank"}, os desenvolvedores passam a responder pelos incidentes de seus serviços. Do ponto de vista dos desenvolvedores, essa é uma atividade indesejada, que interrompe o fluxo de desenvolvimento, comprometendo prazos de entrega. Portanto, a questão é: o que pode a equipe de desenvolvimento fazer para aumentar sua capacidade operacional? Isto é, como desenvolver um serviço de forma que ele gere menos acionamentos (incidentes, dúvidas, etc.) e que os acionamentos gerados possam ser atendidos mais rapidamente? É esse problema que o Chastra, nosso catálogo de padrões apresentado no artigo, ataca.

<br/>
<center>
<a href="https://sol.sbc.org.br/index.php/cbsoft_estendido/article/view/26042" class="button" target="_blank">Download do texto completo aqui</a>
</center>
<br/>

Vale ressaltar ainda que o tempo de reparo para incidentes é fator que compõe o [desempenho de entrega]({{site.baseurl}}{% post_url en/2019-12-03-desempenho-de-entrega%}.html){:target="_ blank"}, capacidade que se correlaciona com o alcance das metas de alto nível das organizações, tanto para fins comerciais quanto para não comerciais.

O catálogo completo de padrões em si ainda não está público. Mas para o CBSoft, já preparamos um serviço com exemplos de implementação de alguns padrões do catálogo. Para cada padrão implementado, há um teste automatizado que realiza a demonstração. [O Chastra Service encontra-se aqui](https://gitlab.com/serpro/chastra-service){:target="_ blank"}. 

![Captura de tela da interface (Swagger) do Chastra Service]({{ site.baseurl }}/assets/figures/chastra-service.png)

<p><center><i>
Figura 2 - Chastra Service, serviço que demonstra padrões para se bem operar microsserviços
</i></center></p>

Os padrões demonstrados no Chastra Service no momento são:

* readme com URLs
* setup doc
* banco local
* swagger
* envvars checker
* supressão de binários
* idempotência
* exception handler
* trace id
* health check
* auditoria
* monitoração para o negócio

## Sobre o evento

A Trilha da Indústria do CBSoft 2023 tem por objetivo fomentar a cooperação entre a academia, o mercado e o governo, com palestras, artigos e discussões. Ela visa reunir profissionais dos três setores para compartilhar seus conhecimentos e suas experiências com a comunidade. Essa é uma excelente oportunidade para estabelecer relações de cooperação entre a comunidade acadêmica e as comunidades de profissionais. É ainda uma oportunidade para estabelecer relações de pesquisa aplicada, fomentando o desenvolvimento do mercado e a inovação com transferência de tecnologias.

O Congresso Brasileiro de Software: Teoria e Prática (CBSoft) é um evento realizado anualmente pela Sociedade Brasileira de Computação (SBC) com o objetivo de promover e incentivar a troca de experiências entre pesquisadores e profissionais da indústria e academia sobre as mais recentes pesquisas, tendências e inovações práticas e teóricas sobre software. Realizado desde 2010 como evento agregador de simpósios brasileiros promovidos pela SBC na área de software, o CBSoft tornou-se um dos principais eventos da comunidade científica brasileira na área de Computação.

A 14ª edição do CBSoft será realizada de 25 a 29 de setembro na Universidade Federal de Mato Grosso do Sul (UFMS), localizado em Campo Grande (MS). Ela integrará quatro eventos tradicionais realizados anualmente pela comunidade brasileira em Engenharia de Software: o XXXVII Simpósio Brasileiro de Engenharia de Software (SBES), o principal evento de Engenharia de Software na América Latina; o XXVII Simpósio Brasileiro de Linguagens de Programação (SBLP); o XVII Simpósio Brasileiro de Componentes, Arquiteturas e Reutilização de Software (SBCARS); e o VIII Simpósio Brasileiro de Teste de Software Sistemático e Automatizado (SAST).

![Foto do local do evento, a Universidade Federal do Mato Grosso do Sul (UFMS)]({{ site.baseurl }}/assets/figures/ufms.jpg)

<p><center><i>
Figura 3 - Foto da Universidade Federal do Mato Grosso do Sul (UFMS) em Campo Grande (MS)
</i></center></p>

A programação do CBSoft incluirá sessões técnicas com apresentações de artigos científicos, palestras proferidas por pesquisadores brasileiros e estrangeiros de renome nacional e internacional, painéis de discussão, workshops, e demonstrações de ferramentas. Todas essas diversas atividades de interesse da comunidade de Engenharia de Software e de áreas relacionadas ao desenvolvimento de sistemas de software direcionam-se para a difusão do conhecimento e discussão de questões importantes relacionadas à pesquisa, desenvolvimento e inovação, tanto no Brasil quanto no mundo.

