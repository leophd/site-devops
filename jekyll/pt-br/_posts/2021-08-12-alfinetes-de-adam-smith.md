---
layout: post
title: "Adam Smith e a organização do trabalho na produção de software"
author: "Leonardo Leite"
lang: pt-br
lang-ref: adam-smith
hiden: false
published: true
---

Em nossa pesquisa, descobrimos [quatro estruturas]({{ site.baseurl }}{% post_url pt/2021-06-20-resumo-estruturas %}.html){:target="_blank"} para organizar a divisão de trabalho e a interação entre profissionais de desenvolvimento e de infraestrutura: departamentos segregados, departamentos colaborativos, departamentos únicos e departamentos mediados por API. Também descobrimos que a estrutura de mediação por API, com seus times de plataforma, parece proporcionar melhores resultados em termos de [desempenho de entrega]({{site.baseurl}}{% post_url en/2019-12-03-desempenho-de-entrega%}.html){:target="_ blank"}. Contudo, não é totalmente claro *o porquê* disso ocorrer. Neste post, forneço uma opinião sobre o assunto.

![Uma nota de 20 libras com a face de Adam Smith]({{ site.baseurl }}/assets/figures/20pounds.jpeg)

<center><i>
Figura 1 - Na nota lê-se: "A divisão do trabalho na manufatura de alfinetes: (e o grande aumento na quantidade de trabalho resultante)"
</i></center>

<p/>

Numa aula sobre "a fábrica de alfinetes de Adam Smith" [1], o professor Paulo Gala (FGV) explica que a produtividade não depende da quantidade de trabalho individual, mas sim da forma de se organizar a produção. Mais especificamente, a produtividade decorre de pelo menos dois fatores presentes no sistema fabril: 1) **especialização produtiva** (ou "aumento da destreza dos trabalhadores") e 2) **pouca troca de contexto**. Dessa forma, 15 operários organizados, cada um com sua tarefa e pouco trocando de contexto, produzem muito mais alfinetes per capta do que 15 operários produzindo alfinetes de forma individual.

Já sabemos que é preciso muita cautela na comparação da produção de software com a produção fabril. A metáfora da "fábrica de software" já causou problemas em nossa "indústria", que, aliás, até mesmo economistas tem dificuldade em classificar entre indústria e serviços. Contudo, lições podem ser aprendidas, tanto que os princípios *lean* [2] vieram da indústria automotiva. Dessa forma, creio que faça sentido explicar a diferença observada de produtividade para as diferentes estruturas organizacionais levantadas por nossa pesquisa (aqui assumindo o desempenho de entrega como métrica de produtividade) em termos dos fatores de produtividade apontados por Adam Smith.

Nos **departamentos segregados**, temos alta especialização em busca da otimização dos "recursos". Contudo, a transferência de trabalho entre áreas, normalmente via sistema de chamados, implica em uma grande quantidade de troca de contexto para os profissionais de desenvolvimento que interrompem seu fluxo de trabalho ao abrir um chamado para a área de infraestrutura.

Os **departamentos colaborativos** procuram reduzir o tempo de espera, e assim a troca de contexto, por meio da colaboração direta entre os profissionais de desenvolvimento e os de infraestrutura. Aqui a esperança é que uma nova cultura de colaboração faça a diferença. Mas sem uma quantidade suficiente de profissionais de infra à disposição para colaborarem, o que ocorre várias vezes, nenhuma cultura fará milagre: enquanto um profissional de infra colabora com um desenvolvedor, o outro desenvolvedor deve esperar. Além disso, o sistema de colaboração reduz a especialização, uma vez que algumas responsabilidades ficam mal definidas.

Já nos **departamentos únicos**, a tentativa é eliminar totalmente o tempo de espera fazendo com que um único grupo seja responsável tanto por desenvolvimento quanto por infraestrutura. Contudo, em primeiro lugar, isso reduz a especialização do grupo desenvolvendo o software. Segundo, embora a equipe não precise esperar o centro de infraestrutura resolver o chamado, agora é a própria equipe que deve trocar de contexto (de "modo dev" para "modo infra") e investir esse tempo nas questões de infraestrutura.

Finalmente, a estrutura de **mediação por APIs** parece fornecer um bom equilíbrio entre especialização e redução de troca de contexto. A especialização se mantém: a equipe de infraestrutura oferece uma plataforma encapsulando o conhecimento de infraestrutura, enquanto que os desenvolvedores desenvolvem a aplicação. Ao mesmo tempo, a troca de contexto é baixa para os desenvolvedores, uma vez que eles operam seus serviços de forma autônoma por meio da plataforma de infraestrutura. Consideramos aqui que a plataforma fornece uma forma abstraída e fácil para os desenvolvedores operarem a infraestrutura. 

Repare que na mediação por API a especialização e a redução de troca de contexto não são maximizadas. Cada time foca em uma especialidade, mas desenvolvedores tem que entender um mínimo de infra para operarem a plataforma. Já o time de plataforma precisa de uma mentalidade e algumas habilidades de desenvolvimento, uma vez que estão oferecendo a plataforma como um produto. Ou seja, há menos especialização do que nos departamentos segregados, porém mais especialização do que nos departamentos únicos. Ao mesmo tempo, o desenvolvedor deve interromper seu trabalho de codificação e testes para operar o serviço por meio da plataforma, contudo a plataforma procura facilitar essa operação ao máximo possível. Ou seja, há, no mínimo, menos troca de contexto do que nos departamentos segregados.

Em resumo:

* Departamentos segregados favorecem a especialização, mas prejudicam a troca de contexto.
* Departamentos colaborativos tentam reduzir a troca de contexto, mas com alguma redução de especialização.
* Departamentos único reduzem a troca de contexto, mas prejudicam a especialização.
* Departamentos mediados por API fornecem um melhor equilíbrio entre especialização e troca de contexto.

Caso queira opinar, pode fazer isso via Twitter para ([@leonardofl](https://twitter.com/leonardofl){:target="_blank"}).

PS: talvez departamentos únicos com profissionais de infraestrutura dedicados seja uma maneira de maximizar a especialização e a redução de troca de contexto. Mas provavelmente essa estrutura seja menos adotada pelas organizações porque parece menos econômico contratar um profissional de infraestrutura para cada equipe de desenvolvimento.


-----------------

[1] Prof. Paulo Gala. **A fábrica de alfinetes de Adam Smith**. 2021. [https://www.youtube.com/watch?v=MQ9uNGMatw8&t=1s](https://www.youtube.com/watch?v=MQ9uNGMatw8&t=1s){:target="_blank"}

[2] Mary Poppendieck, Tom Poppendieck. **Implementing Lean Software Development**: From Concept to Cash. Addison-Wesley Professional. 2006.
