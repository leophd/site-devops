---
layout: post
title: "Participação em podcasts e eventos sobre nosso livro"
author: "Leonardo Leite"
lang: pt-br
lang-ref: podcasts-book
---

Desde a publicação de nosso livro sobre DevOps, tive a oportunidade de falar sobre o livro nos podcasts e eventos listados abaixo.

## Podcast "Tech Leadership Rocks"

Episódio #171 (28/01/2024) - *Como se faz DevOps com Leonardo Leite*

![Captura de tela da conversa no podcast]({{ site.baseurl }}/assets/figures/podcast-tech-leadership-rocks.png)

<br/>
<center>
<a href="https://techleadership.rocks/2024/01/28/como-se-faz-devops-com-leonardo-leite/" class="button" target="_blank">Áudio e vídeo aqui</a>
</center>
<br/>

## Podcast "Fronteiras da Engenharia de Software"

Episódio #44 (20/03/2024) - *Como se faz DevOps, com Leonardo Leite, Paulo Meirelles e Fabio Kon*

![Imagem de divulgação do episódio]({{ site.baseurl }}/assets/figures/fes.jpeg)

<br/>
<center>
<a href="https://fronteirases.github.io/episodios/paginas/44" class="button" target="_blank">Áudio e vídeo aqui</a>
</center>
<br/>

## 1º Meetup Tech Serpro de 2024

Evento interno na empresa Serpro (Serviço Federal de Processamento de Dados) em 25/03/2024.

![Captura de tela com os participantes do evento]({{ site.baseurl }}/assets/figures/meetup-serpro-livro.png)

## Live na comunidade WEB3DEV

Conversa com Daniel Cukier ocorrida em 02/04/2024.

![Captura de tela da live]({{ site.baseurl }}/assets/figures/web3dev.png)

<br/>
<center>
<a href="https://www.youtube.com/watch?v=yEHrppowovE" class="button" target="_blank">Áudio e vídeo aqui</a>
</center>
<br/>

## Podcast "Cultura DevOps"

Podcast Cultura DevOps T2EP3 (09/04/2024) - Como se faz DevOps? 

![Imagem de divulgação do episódio]({{ site.baseurl }}/assets/figures/podcast-cultura-devops.png)

<br/>
<center>
<a href="https://www.objective.com.br/insights/podcast-cultura-devops-t2ep3-como-se-faz-devops/" class="button" target="_blank">Áudio e vídeo aqui</a>
</center>
<br/>

## Lançamento do livro no Google Campus for Startups

Evento de Lançamento do livro mais aniversário de 12 anos da editora Casa do Código no Google Campus for Startups no dia 11/04.

Tivemos a abertura com o Sr. Carlos da Casa do Código e na sequência um debate com ilustres convidados (Fabricia Doria, Alexandre Freire, Sérgio Lopes e Leonardo Martins). Também estiveram presentes outros autores de livros da Casa do Código. 


![Foto com os autores do livro e com os debatedores do evento]({{ site.baseurl }}/assets/figures/lancamento-livro1.jpeg)

[Mais fotos do lançamento do livro!]({{site.baseurl}}{% post_url en/2024-04-25-lancamento-livro%}.html){:target="_blank"}

## XIV Semana da Computação IME-USP

Palestra para alunos de graduação em ciência da computação do IME USP em 22/04/2024.

![Foto de Leonardo palestrando]({{ site.baseurl }}/assets/figures/xiv-semanaca-computacao-ime-usp.jpg)

## Live de lançamento do livro

Live sobre o livro no canal Alura do YouTube em 23/04/2024.

![Captura de tela da live]({{ site.baseurl }}/assets/figures/live-lancamento-livro.png)

<br/>
<center>
<a href="https://www.youtube.com/live/vzsxxgEJcIc?si=osVe44YiTlVUt11S" class="button" target="_blank">Áudio e vídeo aqui</a>
</center>
<br/>

## Seminário na Unifesp

Seminário para os alunos de pós da Unifesp em 29/04/2024, a convite do professor Tiago Silva.

![Captura de tela do seminário]({{ site.baseurl }}/assets/figures/seminario-unifesp-2024-04-29.png)

## Gravando vídeo para a formação DevOps da Alura

Gravando nos estúdios Alura com Lucas Ribeiro Mata e Fabio Kon. Bate papo relacionando as tecnlogias DevOps ao conteúdo do livro. Em breve disponível na formação DevOps da Alura!

![Lucas, Leonardo e Fabio sentados em poltronas em um estúdio de gravação; ao fundo, um quadro luminoso escrito "DevOps".]({{ site.baseurl }}/assets/figures/recording-at-alura.jpeg)

## Seminário na UFC (Universidade Federal do Ceará)

Seminário para os alunos de graduação e de pós da UFC em 07/05/2024, a convite da professora Carla Bezerra.

![Captura de tela do seminário]({{ site.baseurl }}/assets/figures/seminario-ufc-2024-05-07.png)

<br/>
<center>
<a href="{{ site.baseurl }}/assets/files/seminario-devops-ufc-2024-05-07.pdf" class="button" target="_blank">Slides aqui</a>
</center>
<br/>

## Podcast "Cultura DevOps" (de novo!)

Podcast Cultura DevOps T2EP5 (14/06/2024) - Times de Plataforma

![Imagem de divulgação do episódio]({{ site.baseurl }}/assets/figures/podcast-cultura-devops2.png)

<br/>
<center>
<a href="https://www.objective.com.br/insights/podcast-cultura-devops-t2ep5-times-de-plataforma/" class="button" target="_blank">Áudio e vídeo aqui</a>
</center>
<br/>


