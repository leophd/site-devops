---
layout: post
title: "Apresentação: do commit à produção, integração contínua e entrega contínua no pipeline de implantação"
author: "Leonardo Leite"
lang: pt-br
lang-ref: pipeline-presentation
hiden: false
published: true
---

Esta apresentação é baseada em uma aula que ministrei para alunos de uma escola técnica (ETEC Taboão); é sobre:

* Os estágios em um pipeline de implantação
* Integração contínua
* Entrega contínua

![Figura principal dos slides, exibindo um esquema de pipeline, diagrama com caixas e setas.]({{ site.baseurl }}/assets/figures/pipeline.png)

<br/>
<center>
<a href="https://pt.slideshare.net/leonardoferreiraleite/do-commit-produo-integrao-contnua-e-entrega-contnua-no-pipeline-de-implantao" class="button" target="_blank">Slides aqui</a>
</center>
<br/>

