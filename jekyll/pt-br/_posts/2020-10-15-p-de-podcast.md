---
layout: post
title: "Conversando sobre DevOps no p de Podcast"
author: "Leonardo Leite"
lang: pt-br
lang-ref: p-de-podcast
hiden: false
published: true
---

No dia 11 de setembro, tive o prazer de conversar sobre DevOps com o Marcio Frayze e o Julianno Martins no p de Podcast, um podcast sobre arquitetura de software.

![Logo do p de Podcast]({{ site.baseurl }}/assets/figures/p-de-podcast.png)

<br/>
<center>
<a href="https://open.spotify.com/episode/3xNWRFRxpLsFf06WYxr8Kz?si=XUTn9P0cQrCcvQ6JVPvwlw&utm_source=native-share-menu" class="button" target="_blank">Acesse via Spotify!</a>
</center>
<br/>

<br/>
<center>
<a href="https://anchor.fm/pdepodcast/episodes/DevOps-ejff17" class="button" target="_blank">Acesse via Anchor!</a>
</center>
<br/>

Dentre outros tópicos, abordamos em nossa conversa:

* Definição de DevOps - segundo nossa pesquisa (e algumas outras visões)
* Cerne / história do DevOps - implantação deixa de ser gargalo, tem a ver com ágil?
* O papel do "engenheiro DevOps" - o que é? faz sentido? é exploração?
* Com entrega contínua: sem grandes festas, cada commit um release candidate
* Um case para implantação contínua
* Um case para não integrar continuamente
* SRE - como trabalha a pessoa de infra no Google?
* Lei de Conway - tem que falar né
* A suma importância (e dificuldade) dos testes automatizados - incumbência de cada dev (cuidado pra não ser chamado de incompetente!)
* Microsserviços e monólitos vs entrega contínua - o que tem a ver? 
* Pipelines e microsserviços - automação da automação
* Sonar e pipeline - travar ou não travar a entrega?
* E os problemas? Vale tudo isso a pena?
* Para implantar DevOps - projetão do presidente (top-down) ou guerrilha (bottom-up)?
* Um pouco sobre nossa pesquisa e entrevistas - formas de se fazer DevOps
* Times de plataforma - melhorando o desempenho de entrega
* Conflitos e incômodos com o DevOps
* Recomendação de leitura - O Projeto Fênix

