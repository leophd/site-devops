---
layout: post
title: "O que é DevOps? Nossa definição de DevOps"
author: "Leonardo Leite"
lang: pt-br
lang-ref: devops-definition
hiden: false
published: true
---

Embora se fale sobre o movimento DevOps por quase uma década, DevOps ainda carece de uma definição amplamente aceita. Ao consolidar as definições de DevOps mais citadas na literatura acadêmica, elaboramos nossa definição:

-----------

**"DevOps é um esforço colaborativo e multidisciplinar dentro de uma organização para automatizar a entrega contínua de novas versões de software, garantindo corretude e confiabilidade."**

-----------

Esta definição foi apresentada em nossa [Revisão de Literatura sobre Conceitos e Desafios de DevOps]({{ site.baseurl }}{% post_url en/2019-12-04-devops-survey-published %}.html){:target="_blank"}, publicada pelo ACM Computing Surveys, um periódico acadêmico muito conceituado em nosso campo.

Existe uma tendência atual na indústria de adotar DevOps como um papel, o "engenheiro de DevOps" [1]. Mas nossa definição de DevOps está de acordo com o espírito original do movimento DevOps, que era quebrar os silos (*"DevOps é um esforço colaborativo e multidisciplinar dentro de uma organização"*). Esse espírito foi retratado, por exemplo, pelo romance *O Projeto Fênix* [2], de autoria de uma figura proeminente do movimento DevOps. Nossa definição também está de acordo com uma visão compartilhada na literatura que relaciona fortemente DevOps à entrega contínua (*"para automatizar a entrega contínua de novas versões de software"*), evidenciada pelo título do artigo seminal sobre DevOps "Por que as empresas devem adotar DevOps para possibilitar a entrega contínua" [3]. Por fim, nossa definição também está alinhada com desdobramentos mais recentes que enfrentam o fato de que não basta entregar mais rápido, mas também que a confiabilidade é primordial (*"garantindo corretude e confiabilidade"*), como a discussão apresentada no livro *Engenharia de Confiabilidade do Google* [4].

------------------------------------------------

[1] Waqar Hussain, Tony Clear, and Stephen MacDonell. **Emerging trends for global DevOps: A New Zealand perspective**. In Proceedings of the 12th International Conference on Global Software Engineering, ICGSE 2017.

[2] Gene Kim, Kevin Behr, and George Spafford. **The Phoenix Project**: A Novel about IT, DevOps, and Helping Your Business Win. IT Revolution Press, 3rd edition, 2018.

[3] Jez Humble and Joanne Molesky. **Why enterprises must adopt DevOps to enable continuous delivery.** Cutter IT Journal, 2011.

[4] Betsy Beyer, Chris Jones, Jennifer Petoff, and Niall Richard Murphy. **Site Reliability Engineering**: How Google runs production systems. O'Reilly, 2016.
