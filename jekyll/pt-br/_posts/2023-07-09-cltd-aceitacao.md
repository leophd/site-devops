---
layout: post
title: "Nossa tese é finalista do Concurso Latino-Americano de Teses de Doutorado!"
author: "Leonardo Leite"
lang: pt-br
lang-ref: cltd
hiden: false
---

Temos o prazer de anunciar que nossa tese ([A grounded theory of organizational structures for development and infrastructure professionals in software-producing organizations]({{ site.baseurl }}{% post_url en/2022-07-13-thesis-published %}.html){:target="_blank"}) foi escolhida como **uma das três finalistas** do IX Concurso Latino-Americano de Teses de Doutorado (CLTD 2023)!!!

![Mapa da América Latina com o sul para cima, destacando La Paz, Bolívia]({{ site.baseurl }}/assets/figures/america-latina.jpg)

<p><center><i>
Figura 1 - Nossa América Latina (obra do uruguaio Antonio Torres Garcia)
</i></center></p>

Para incentivar o desenvolvimento de programas de doutorado, o Centro Latino-Americano de Estudos de Informática (CLEI) realiza desde 2016 o Concurso Latino-Americano de Teses de Doutorado (CLTD), do qual podem participar doutorandos de todas as universidades membros do CLEI.

![Captura de tela de e-mail escrito em espanhol]({{ site.baseurl }}/assets/figures/aceite-cltd.png)

<p><center><i>
Figura 2 - Comunicação dos finalistas enviada pelos chairs do CLTD
</i></center></p>

O concurso será realizado dentro da XLIX Conferência Latino-Americana de Informática (CLEI 2023) em outubro na Universidad Mayor de San Andrés (La Paz, Bolívia).

![Foto do local do evento, a Universidad Mayor de San Andrés]({{ site.baseurl }}/assets/figures/universidad-san-andres.png)

<p><center><i>
Figura 3 - Foto da Universidad Mayor de San Andrés (La Paz, Bolívia)
</i></center></p>

As conferências CLEI são realizadas desde 1974 pelo Centro Latino-Americano de Estudos de Informática (CLEI) e são o principal espaço latino-americano para o intercâmbio de ideias, experiências, resultados e aplicações entre pesquisadores, professores e estudantes de Informática e Ciência da Computação.






