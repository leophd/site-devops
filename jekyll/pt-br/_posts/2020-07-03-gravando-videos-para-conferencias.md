---
layout: post
title: "Gravando vídeos para conferências"
author: "Leonardo Leite"
lang: pt-br
lang-ref: gravado-videos-conferencias
---

Registrando aqui, no formato de checklist, como fiz para gravar os vídeos para o ICSE (International Conference on Software Engineering). Espero que essas anotações ajudem a mim mesmo no futuro e, possivelmente, alguns colegas também. Contudo, use essas indicações com ponderação, uma vez que eu não sou entendido em gravação e edição de vídeos! A ideia aqui é haver um certo equilíbrio entre alguns elementos de qualidade e tempo empregado.

## Gravando vídeos curtos (~5min)

Antes da gravação:

* Montar um roteiro.
* Pesquisar a pronúncia das palavras (assumimos aqui que os vídeos são em inglês).
* Montar uma versão fonética do roteiro pra te ajudar a lembrar das pronúncias (exemplo na Figura 1).
* Aliás, uma grande descoberta sobre pronúncia pra mim foi o som "ə". Referência: [https://www.youtube.com/watch?v=kjVp_xr0WR0](https://www.youtube.com/watch?v=kjVp_xr0WR0){:target="_blank"}.
* Criar uma notação para ênfases (ex: negrito para maior ênfase).
* Treinar bastante.
* Testar microfones: qual o seu melhor microfone? Headset, microfone do notebook, celular... Pra mim foi uma questão meio difícil, acho que ainda não achei um microfone ideal (bom volume, bom timbre e sem chiado de fundo).
* Deixar a paginação dos slides em um local que não será coberto pelo vídeo do apresentador (ex: número da página no canto inferior esquerdo e vídeo no canto inferior direito).

![Trecho do roteiro fonético, com texto usando notação fonética e negrito para ênfases.]({{ site.baseurl }}/assets/figures/roteiro-fonetico.png)

<center><i>
Figura 1 - Trecho do roteiro fonético, com texto usando notação fonética e negrito para ênfases.
</i><p/></center>

Gravação do discurso:

* Gravar primeiro apenas o seu vídeo de você falando. 
* Pra isso, usar o celular (no meu caso, melhor imagem).
* Conferir iluminação. Deixar a janela fechada e usar a luz artificial, pra deixar a iluminação mais homogênea. Também usar uma luminária próxima ao rosto ("bafo de luz").
* Deixar celular em modo avião.
* Usar app Mic Amplifier no celular pra melhorar o volume do áudio.
* Nesse vídeo, gravar lendo o roteiro fonético. 
* Contudo, não pode parecer que você está lendo. Para isso, pensar bem na posição da câmera e da tela com o texto, de forma que quando você leia o texto, pareça (razoavelmente) que você está olhando pra tela. Acho que quanto mais próximos a tela de leitura e a câmera estiverem e quanto mais longe você estiver dos dois, melhor. Também ajustar a altura da cadeira para que fique mais baixa, de forma que a altura dos olhos fique mais próxima à altura da câmera. Além disso, no Writer deixei a largura do texto mais estreita que o normal. Esse *setup* está representado nas figuras 2, 3 e 4.
* Manter o celular o mais na vertical possível. Pra isso usei um cavalete e uns livrinhos de apoio (figura 3).
* De vez em quando, olhar diretamente para a câmera. Principalmente na saudação e na despedida.
* Outra dica é sorrir para a câmera antes de começar a gravação. Sugestão adicional: logo antes ver alguns dos melhores momentos de sua série de comédia favorita (aqui usei "Todo Mundo Odeia o Cris" :).
* Gravar tudo em uma tomada só para facilitar a edição depois. Em uma das vezes, gravei em tomadas e depois apanhei bastante pra sincronizar com os slides.

![Setup de gravação - distâncias entre tela de leitura, câmera e apresentador.]({{ site.baseurl }}/assets/figures/setup-gravacao1.jpeg)

<center><i>
Figura 2 - Setup de gravação - distâncias entre tela de leitura, câmera e apresentador.
</i><p/></center>

![Setup de gravação - cavalete e livrinhos de apoio.]({{ site.baseurl }}/assets/figures/setup-gravacao2.jpeg)

<center><i>
Figura 3 - Setup de gravação - cavalete e livrinhos de apoio.
</i><p/></center>

![Setup de gravação - parte do texto (na tela de leitura) visível para o apresentador.]({{ site.baseurl }}/assets/figures/setup-gravacao3.jpeg)

<center><i>
Figura 4 - Setup de gravação - parte do texto (na tela de leitura) visível para o apresentador está apenas um pouco acima da câmara.
</i><p/></center>

Gravação dos slides:

* Depois de gravado o discurso, gravar a apresentação dos slides enquanto você ouve o discurso.
* Software pra gravar a tela: OBS.
* Se tiver gravado o discurso em trechos, primeiro unificar os *clips* do discurso pra facilitar a edição (não cheguei a fazer isso).
* Enquanto gravar a tela, capturar o som do discurso para servir de guia para a sincronização na edição.

Edição

* Software: Openshot.
* Não usar o Openshot do *apt-get*, baixar do site (pra poder ter melhor as funcionalidades de *crop*).
* Amplificar volume.
* Deixar vídeo do palestrante na canto inferior direito (pra usar o *crop*, usar a visualização avançada). Ver vídeo abaixo.
* Pra cortar as pontinhas dos *clips*: clique-direito em cima do pino azul > cortar tudo > manter lado/esquerdo.
* Se não gravar tudo em uma tomada só, quando for editar tamanho, posição e volume do *clip* do discurso, tem que fazer isso para cada trecho gravado.
* Quando for editar tamanho, posição e volume do *clip* do discurso: fazer isso no primeiro *frame* do vídeo. Se você, por exemplo, definir um volume de 1,3 no tempo 1s, o OpenShot fará uma interpolação no volume de 1 a 1,3 entre os tempos 0 e 1s.
* Em um dos vídeos coloquei música de abertura e encerramento. Acho que ficou legal. Contudo, se fizer isso, preste atenção aos direitos autorais.
* Para exportar o vídeo: perfil web, destino YouTube-HD, qualidade alta.

<p><iframe width="560" height="315" src="https://www.youtube.com/embed/CTqC6AaEXHM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>

Legendas

* Software: Aegisub.
* Tutorial massa: [https://www.youtube.com/watch?v=CGgu6-uQy68](https://www.youtube.com/watch?v=CGgu6-uQy68){:target="_blank"}.
* Site pra traduzir legendas: [https://www.syedgakbar.com/projects/dst](https://www.syedgakbar.com/projects/dst){:target="_blank"}.

Resultados - usando esses procedimentos, gravei e produzi os seguintes vídeos abaixo:

<p><iframe width="560" height="315" src="https://www.youtube.com/embed/xroC_fyGy5Q" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>

<p><iframe width="560" height="315" src="https://www.youtube.com/embed/nUtkLbxQfKg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>

## Gravando vídeos mais longos (~20min)

* Gravar com OBS capturando tela e vídeo ao mesmo tempo.
* Fazer numa tomada só, como se estivesse ao vivo (prosseguir com os erros).
* Se o erro for muito grave, dar uma pausa e retomar um pouco de trás.
* Se o som do celular for melhor que o do computador: capturar o som com o celular e com o computador ao mesmo tempo.
* Claquete no início pra ajudar a sincronizar o som na edição (no caso, eu bati palma).
* Na edição (Openshot), usar o som do celular.
* Na edição, cortar fora os erros graves (ou momentos com eventuais interferências externas).
* O meu app de gravação de áudio (no celular) ficou com uns buracos. Não sei se foi por causa das notificações do celular (esqueci de deixar em modo avião). Nesse caso, pude utilizar o som gravado pelo computador nos momentos necessários.

Resultados - usando esses procedimentos, gravei e produzi o seguinte vídeo abaixo:

<p><iframe width="560" height="315" src="https://www.youtube.com/embed/hAEKzjJBIJA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>


