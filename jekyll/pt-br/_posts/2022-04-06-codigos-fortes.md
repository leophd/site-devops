---
layout: post
title: "As implicações das diferentes formas de se organizar os profissionais de desenvolvimento e de infraestrutura"
author: "Leonardo Leite, Nelson Lago, Claudia Melo, Fabio Kon, and Paulo Meirelles"
lang: pt-br
lang-ref: strong-codes
hiden: false
published: true
---

O movimento DevOps surgiu como uma mudança cultural para quebrar os silos existentes em grandes organizações, integrando melhor as equipes de desenvolvimento e operações por meio da colaboração. No entanto, essa colaboração pode acontecer de maneiras diferentes sob uma perspectiva organizacional: os desenvolvedores e especialistas em infraestrutura podem fazer parte de departamentos diferentes ou podem estar juntos em uma única equipe. Com os avanços nas ofertas de PaaS, é possível até mesmo imaginar os próprios desenvolvedores assumindo as responsabilidades das operações.

Nossa pesquisa no IME-USP (Universidade de São Paulo) investiga como as empresas produtoras de software estão organizando suas equipes de desenvolvimento e de infraestrutura. Para isso, estamos entrevistando profissionais de software para entender como as coisas estão realmente acontecendo no mundo real. Com nossa pesquisa, esperamos fornecer uma teoria para apoiar as organizações no desenho de suas estruturas organizacionais em direção à entrega contínua e no tratamento das consequências da escolha de uma determinada estrutura.

Com base na análise criteriosa das entrevistas realizadas, elaboramos uma teoria que descreve as estruturas organizacionais utilizadas pela indústria no mundo real sobre como o trabalho dos desenvolvedores e engenheiros de infraestrutura pode ser coordenado na busca pela entrega contínua. Descrevemos essas estruturas (departamentos segregados, departamentos colaborativos, departamento único e departamentos mediados por API) em detalhes em nosso [resumo das estruturas organizacionais]({{site.baseurl}}{% post_url en/2021-06-20-resumo-estruturas%}.html){:target="_ blank"}. Aqui neste post, resumimos cada estrutura com uma figura e sua legenda.

Para entender melhor tais estruturas, buscamos desvendar por que diferentes organizações adotam estruturas diferentes. Além disso, considerando a existência de vantagens e desvantagens para cada estrutura, queríamos conhecer as estratégias adotadas pelas empresas para superar as desvantagens de cada estrutura. Assim, por meio de um processo de pesquisa, para cada estrutura organizacional, investigamos suas condições, causas, motivos para evitar, consequências e contingências, conforme definido abaixo:

* *Condições*: condições ambientais necessárias para implementar uma estrutura (ou seja, pré-requisitos).
* *Causas*: motivos/motivações/oportunidades que levaram a organização a adotar determinada estrutura e não outra.
* *Motivos para evitar*: motivos/motivações que levaram a organização a não adotar determinada estrutura.
* *Consequências*: resultados que acontecem ou se espera que aconteçam depois que uma organização adota uma estrutura, incluindo problemas inesperados.
* *Contingências*: estratégias para superar as desvantagens de uma estrutura.

Então agora listaremos as condições, causas, motivos para evitar, consequências e contingências associadas a cada estrutura. Acabamos de submeter esses resultados a um processo de revisão por pares. O artigo completo submetido está disponível [aqui](https://www.techrxiv.org/articles/preprint/A_theory_of_organizational_structures_for_development_and_infrastructure_professionals/19210347){:target="_ blank"}. Na frente de cada implicação listada, há um código (por exemplo, SC01) para se referir em discussões. Em nossa pesquisa, chamamos essas implicações de "*strong codes*", por isso as letras "SC".










## Departamentos de devs & infra segregados

![A figura mostra um operador e um desenvolvedor, cada um dentro de um círculo diferente. Eles estão de costas um para o outro. A comunicação flui por meio de cartas, mais do desenvolvedor para o operador do que do operador para o desenvolvedor.]({{ site.baseurl }}/assets/figures/siloed-departments.png)

<center><i>
Figura 3 - Com departamentos segregados, operadores e desenvolvedores estão cada um em suas próprias bolhas. Eles não interagem muito diretamente, e a comunicação flui lentamente por meios burocráticos (sistemas de tickets).
</i><p/></center>

### Consequências

* SC01 - Devs não têm autonomia e dependem do grupo de operações
* SC02 - Baixo desempenho de entrega (filas e atrasos)
* SC03 - Atrito e dedos apontados entre devs e infra





## Departamentos de devs & infra colaborativos

![A figura mostra um operador e um desenvolvedor, cada um dentro de um círculo diferente. Eles estão olhando um para o outro e estão de mãos dadas (com alguma dificuldade).]({{ site.baseurl }}/assets/figures/classical-devops.png)

<center><i>
Figura 4 - Com departamentos colaborativos, operadores e desenvolvedores em diferentes departamentos procuram trabalhar juntos, mesmo que não seja fácil, por meio de contato direto e colaboração próxima.
</i><p/></center>

### Condições

* SC04 - Pessoal de infra o suficiente para alinhar com as equipes de desenvolvimento
* SC05 - Apoio da alta administração

### Causas

* SC06 - Em uma empresa não grande / com poucos produtos, é mais fácil ser colaborativo
* SC07 - Tentando evitar o gargalo de entrega
* SC08 - Iniciativa de baixo para cima com apoio posterior da alta administração

### Consequências

* SC09 - Interação crescente entre áreas (por exemplo, compartilhamento de conhecimento)
* SC10 - Colaboração precária (pessoal de operações sobrecarregados)
* SC11 - Desconforto/frustração/atrito/ineficiência com responsabilidades confusas (as pessoas não sabem o que fazer ou o que esperar dos outros)
* SC12 - Esperas (durante trocas), infra ainda um gargalo
* SC13 - Automação suporta colaboração

### Contingências

* SC14 - Dando mais autonomia aos devs (na homologação ou mesmo na produção)










## Departamento dev/infra único

![A figura mostra um operador e um desenvolvedor, eles estão dentro do mesmo círculo.]({{ site.baseurl }}/assets/figures/cross-functional-team.png)

<center><i>
Figura 5 - Um único departamento cuida do desenvolvimento e da infraestrutura.
</i><p/></center>


### Condições
    
* SC15 - Pessoas de operações o suficiente para cada equipe de desenvolvimento
    
### Causas
    
* SC16 - Cenário de startup (empresa pequena, jovem, com requisitos de escalabilidade de infraestrutura amenos, foco no negócio, uso de serviços em nuvem para limitar custos)
* SC17 - Serviços em nuvem diminuem a necessidade de equipe de infra e operações
* SC18 - Velocidade de entrega, agilidade, projeto crítico
    
### Razões para evitar
    
* SC19 - Não é adequado para aplicação de padrões corporativos de governança 
* SC20 - Mais custos: duplicação de trabalho de infra entre equipes, altos salários para profissionais de infra, profissionais de infra subutilizados
    
### Consequências
    
* SC21 - Sem padrões (*defauls*) [de infra] entre as equipes: liberdade, mas possivelmente levando à duplicação de esforços e altos custos de manutenção
    
### Contingências
    
* SC22 - Melhora as habilidades de infra na empresa, inclusive por meio de palestras técnicas






## Departamentos de devs & infra mediados por API

![A figura mostra um operador e um desenvolvedor, cada um dentro de um círculo diferente. O operador fornece uma interface que é consumida pelo desenvolvedor (notação UML).]({{ site.baseurl }}/assets/figures/platform-team.png)

<center><i>
Figura 6 - O time de plataforma fornece serviços automatizados para serem usados, com pouco esforço, pelos desenvolvedores (a API da plataforma medeia a interação entre os desenvolvedores e o time de plataforma). O time de plataforma e os desenvolvedores estão abertos para ouvir e apoiar uns aos outros.
</i><p/></center>

### Condições
    
* SC23 - Empresa de médio e grande porte
* SC24 - Iniciativas/patrocínios de cima para baixo
* SC25 - Investimento inicial
* SC26 - Requer habilidades de codificação das pessoas de infra
    
### Causas
    
* SC27 - Gargalo de entrega na gestão de infra
* SC28 - Compatível com estruturas rígidas já existentes (baixo impacto no organograma) / Apenas algumas pessoas necessárias para formar uma equipe de plataforma
* SC29 - Promove a entrega contínua
* SC30 - Um herói ou visionário (cultura do herói)
* SC31 - Surgiu como melhor solução; outras iniciativas não tão frutíferas
* SC32 - Vários produtos / várias equipes de desenvolvimento / vários clientes (requer alto desempenho de entrega)
    
### Consequências
    
* SC33 - Interação (equipe devs x plataforma) para: dar suporte a devs, fazer as coisas funcionarem e exigir novos recursos da plataforma
* SC34 - A plataforma fornece mecanismos comuns (por exemplo: dimensionamento, faturamento, observabilidade, monitoramento)
* SC35 - Promove entrega contínua, agilidade e mudanças mais rápidas
* SC36 - Desenvolvedores responsáveis ​​pela arquitetura / preocupações de infraestrutura (por exemplo, NFR)
* SC37 - Equipe da plataforma fornece consultoria e documentação para desenvolvedores
* SC38 - Adicionar devs não requer adicionar [proporcionalmente] mais pessoas infra
* SC39 - Gargalo anterior eliminado
* SC40 - Equipe de plataforma pequena (centro de excelência)
* SC41 - Altos custos ao usar nuvens públicas
* SC42 - As habilidades dos desenvolvedores estão muito focadas nas necessidades corporativas, faltando conhecimento básico de infra (ruim para os próprios desenvolvedores, não para a empresa)
* SC43 - O custo de gerenciamento da plataforma (mesmo usando software de código aberto) é alto
* SC44 - Risco: plataforma é mágica para devs; eles negligenciam a qualidade porque confiam demais na plataforma, qualquer problema eles culpam a plataforma e não sabem o que fazer, mesmo para problemas simples ou quando o problema está na própria aplicação
* SC45 - Devs possivelmente incapazes de entender a infra ou contribuir para a plataforma
    
### Contingências
    
* SC46 - Decidir o quanto os devs devem ser expostos aos detalhes internos da infra (em alguns lugares mais, em outros menos)











