---
layout: post
title: "TDC São Paulo 2020 - Times de plataforma: o estado-da-arte e da prática para acelerar a entrega contínua"
author: "Leonardo Leite"
lang: pt-br
lang-ref: tdc-2020
hiden: false
published: true
---

Hoje tive a honra e prazer de dar uma palestra sobre times de plataforma na <a href="https://thedevconf.com/tdc/2020/sampaonline/trilha-devops" target="_blank">trilha de DevOps</a> do The Developer Conference (TDC) São Paulo 2020, o maior evento sobre desenvolvimento de software do Brasil.

<br/>

![Captura de tela de sessão virtual do TDC; além de Leonardo, aparecem dois homens e duas mulheres; uma das mulheres está segurando o livro Team Topologies; pessoas sorrindo.]({{ site.baseurl }}/assets/figures/tdc2020.jpeg)

<br/>
<center>
<a href="https://pt.slideshare.net/leonardoferreiraleite/times-plataformatdc2020" class="button" target="_blank">Slides aqui</a>
</center>
<br/>

Sinopse da palestra:

*Como organizar as interações (humanas) entre devs e ops num contexto de entrega contínua? Em uma pesquisa com 46 pessoas, de 44 empresas, em 8 países, em meu doutorado na USP, identifiquei 4 estruturas organizacionais. Entre elas, destaca-se a que denominamos de **times de plataforma**: o time de infra passa a fornecer serviços altamente automatizados para empoderar os desenvolvedores, que por sua vez se tornam responsáveis pela operação de seus serviços. Apesar dos desafios, esse esquema contribui para um alto desempenho de entrega. Nesta palestra, você entenderá o que é um time de plataforma e suas consequências; somada à minha experiência no Serpro, discutirei essa abordagem para as empresas.*

<br/>

![Logo do TDC]({{ site.baseurl }}/assets/figures/tdc-logo.jpg)

