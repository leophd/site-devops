---
layout: post
title: "Aula sobre DevOps e Arquitetura de Software no Lab XP"
author: "Leonardo Leite"
lang: pt-br
lang-ref: devops-architecture
hiden: false
published: true
---

Há um ano, em agosto de 2019, apoiei a prof. Thatiane de Oliveira Rosa em uma aula sobre arquitetura de software, falando sobre “DevOps e Arquitetura de Software”. A aula foi para alunos de graduação e pós-graduação, no curso Laboratório de Programação Extrema, ministrado pelo prof. Alfredo Goldman.

Segue aqui a apresentação (em formato texto) que utilizei.

## Arquitetura de software e DevOps

Encomenda:

* Ideia geral do que é DevOps.
* Como as práticas de integração e entrega contínua (CI/CD) beneficiam a agilidade na criação de novas funcionalidades

### O que é DevOps?

DevOps is a collaborative and multidisciplinary effort within an organization to
automate continuous delivery of new software versions, while guaranteeing their
correctness and reliability.

DevOps engineer? DevOps team?


### O problema que DevOps resolve

* Estrutura em silos
* Phoenix project [4,5]
* Ágil resolveu no desenvolvimento, mas infra continuava tradicional


### Why Enterprises Must Adopt Devops to Enable Continuous Delivery [1]

Artigo defendem times cross-funcionais e automação.

Amazon: You build it, you run it.

"The most effective way to provide value with IT systems is through the creation of cross-functional product teams that manage services throughout their lifecycle, along with automation of the process of building, testing, and deploying IT systems."

Gerência por projeto vs gerência por produto


### DevOps

"Devops is about aligning the incentives of everybody involved in delivering software. 
Achieving both frequent, reliable deployments and a stable production environment." [1]

Culture (collaboration), automation, measurement, sharing

"improve collaboration and share responsibilities" [1]


### Three ways (The DevOps Handbook [2])

1. Mapping the value stream for global optimization, not local optimization.

2. Amplifying continuous feedback loops to support necessary corrections.

3. Improving daily work through a culture promoting frequent experimentation, risk-taking, learning
from mistakes, and knowing that practice and repetition are prerequisites to mastery.


### Entrega contínua [3]

Entrega contínua x implantação contínua

Para que a TI não seja o gargalo

"Continuous delivery enables businesses to reduce cycle time so as to get faster feedback from users, reduce the risk and cost of deployments, get better visibility into the delivery process itself, and manage the risks of soft- ware delivery more effectively. Continuous delivery means knowing that you can release your system on demand with virtually no technical risk." [1]



### O pipeline de implantação

https://gitlab.com/radar-parlamentar/radar/pipelines

https://gitlab.com/serpro/fatiador/pipelines

Para muitos, devops = entrega contínua ou devops = pipeline


### Delivery performance [9]

* Frequência de implantação
* Tempo de entrega
* Tempo de recuperação
* Frequência de falhas

São os objetivos do Devops


### Resultados do DevOps [9]

Lean, continuous delivery e delivery performance estão correlacionado a

* Less burnout
* Less deployment pain
* Less rework
* Job satisfaction
* Organizational performance
* Noncomercial performance

Além disso, frequência de falhas não importa tanto quanto tempo de recuperação.

Pesquisa feita por meio de questionários em 2016 (25,000 respondentes) e 2017 (27,000 respondentes).


### SRE [6]

Mas não adianta só entregar rápido... tem que funcionar... SRE

Aí começamos a falar de arquitetura!


### Tabela periódica devops

https://xebialabs.com/periodic-table-of-devops-tools/

### ...

Infraestrutura como código

Evita snowflakes (gambiarras irreprodutíveis)

### ...

Ferramentas mudam o tempo todo.

Se atenham aos conceitos.

[Quadrantes conceituais]({{ site.baseurl }}{% post_url pt/2020-01-02-mapas-conceituais-devops %}.html){:target="_blank"}: process, people, delivery, runtime


### 12 app factor

https://12factor.net

Dev/prod semelhantes

Configurações


### Outras "práticas DevOps" para melhorar a disponibilidade

* Roll-back
* Feature flag
* Canary release



### Monolito

Testes demoram muito e exige muita memória


### Microsserviços

Problema: não dá pra demorar meses pra implantar cada microsserviço.

Não só deploy tem que ser fácil, mas também a criação de ambiente.



### Resolvendo os problemas de microsserviços

Uso de nuvem e/ou plataforma

Log aggregator / correlation ID's

Monitoração (prometheus / grafana)

Monitoração no nível do negócio

Cuidado com muitos alertas

Cuidado com muita reusabilidade [8]
(pode complicar desenvolvimento independente de cada microsserviço)



### Chaos engineering [7]

Netflix

Macaco do caos


### Referências

[1] Jez Humble and Joanne Molesky. Why Enterprises Must Adopt Devops to Enable Continuous Delivery. Cutter IT Journal. 2011

[2] Gene Kim, Jez Humble, Patrick Debois, John Willis, John Allspaw. The DevOps Handbook:: How to Create World-Class Agility. 2016

[3] Jez Humble, David Farley. Continuous Delivery: Reliable Software Releases through Build, Test, and Deployment Automation. 2010

[4] Gene Kim, Kevin Behr, George Spafford. The Phoenix Project: A Novel about IT, DevOps, and Helping Your Business Win. 2018

[5] Eliyahu M. Goldratt, Jeff Cox. The Goal: A Process of Ongoing Improvement. 30th Anniversary Edition. 2014

[6] Niall Richard Murphy, Betsy Beyer, Chris Jones, Jennifer Petoff Site Reliability Engineering: How Google Runs Production Systems. 2016

[7] Ali Basiri, Niosha Behnam, Ruud de Rooij, Lorin Hochstein, Luke Kosewski, Justin Reynolds, and Casey Rosenthal (Netflix) Chaos Engineering. IEEE Software. 2016

[8] Mojtaba Shahin, Muhammad Ali Babar, and Liming Zhu. The Intersection of Continuous Deployment and
Architecting Process: Practitioners' Perspectives. In Proceedings of the 10th ACM/IEEE International Symposium on Empirical Software Engineering and Measurement. 2016.

[9] Nicole Forsgren PhD, Jez Humble, Gene Kim. Accelerate: The Science of Lean Software and DevOps: Building and Scaling High Performing Technology Organizations. 2018


