---
layout: post
title: "A melhor tese de engenharia de software do Brasil em 2022!!!"
author: "Leonardo Leite"
lang: pt-br
lang-ref: ctd-es-first-place
hiden: false
---

Temos o prazer de anunciar que nossa tese ([A grounded theory of organizational structures for development and infrastructure professionals in software-producing organizations]({{ site.baseurl }}{% post_url en/2022-07-13-thesis-published %}.html){:target="_blank"}) recebeu a primeira colocação no Concurso de Teses e Dissertações em Engenharia de Software (CTD-ES 2023) do CBSoft (Congresso Brasileiro de Software: Teoria e Prática)!!!

![Leonardo e Paulo segurando o certificado; Paulo comemorando com punho erguido; atrás telão exibindo certificado da premiação.]({{ site.baseurl }}/assets/figures/ctd-es1.jpg)

<p><center><i>
Figura 1 (acima) - Leonardo Leite e Paulo Meirelles (orientador) recebendo a premiação no CTD-ES do CBSoft 2023
</i></center></p>

------------------------

![Cenas do CTD, incluindo Leonardo apresentando e uma onça de amigurumi lendo a tese premiada.]({{ site.baseurl }}/assets/figures/ctd-es2.jpg)

<p><center><i>
Figura 2 (acima) - Cenas do CTD-ES do CBSoft 2023
</i></center></p>

O Concurso de Teses e Dissertações em Engenharia de Software (CTD-ES) visa divulgar e premiar as melhores teses de Doutorado e dissertações de Mestrado na área de Engenharia de Software concluídas, defendidas e aprovadas no Brasil no ano de 2022. 

O Congresso Brasileiro de Software: Teoria e Prática (CBSoft) tem o objetivo de promover e incentivar a troca de experiências entre pesquisadores e profissionais da indústria e academia sobre as mais recentes pesquisas, tendências e inovações práticas e teóricas sobre software. O CBSoft incorpora, dentre outros subeventos, o SBES (Simpósio Brasileiro de Engenharia de Software), o mais importante evento sobre pesquisa em engenharia de software do Brasil. CBSoft incorporates, among other sub-events, the SBES (Brazilian Software Engineering Symposium), the most important event on software engineering research in Brazil.

A 14ª edição do CBSoft foi realizada de 25 a 29 de setembro na Universidade Federal de Mato Grosso do Sul (UFMS), localizado em Campo Grande (MS), sendo a primeira edição presencial do CBSoft desde a pandemia de Covid-19. Dessa forma, o evento foi um fórum privilegiado de encontro e discussão para a comunidade. 

![Fotos de Leonardo com diversos participantes do CBSoft; foto de Leonardo apresentando; no centro uma onça de amigurumi e uma camiseta de bebê com o desenho de um jacaré.]({{ site.baseurl }}/assets/figures/cbsoft2023-1.jpg)

<p><center><i>
Figura 3 (acima) - Cenas do CBSoft 2023 - dia 2
</i></center></p>

------------------------

![Fotos de Leonardo com diversos participantes do CBSoft; foto do restaurante universitário com grandes desenhos de temática indígena.]({{ site.baseurl }}/assets/figures/cbsoft2023-2.jpg)

<p><center><i>
Figura 4 (acima) - Cenas do CBSoft 2023 - dia 3
</i></center></p>

------------------------

![Fotos de Leonardo com participante do CBSoft; várias fotos com capivaras.]({{ site.baseurl }}/assets/figures/cbsoft2023-3.jpg)

<p><center><i>
Figura 5 (acima) - Cenas do CBSoft 2023 - dia 4
</i></center></p>

------------------------

![Fotos de Leonardo com diversos participantes do CBSoft; foto de um sobá; foto em um aquário.]({{ site.baseurl }}/assets/figures/cbsoft2023-4.jpg)

<p><center><i>
Figura 6 (acima) - Cenas do CBSoft 2023 - dia 5
</i></center></p>

No contexto da Terceira Escola de Engenharia de Software (LATAM School), ocorrida no contexto do CBSoft, também realizei uma apresentação de poster sobre nossa pesquisa.

<a href="{{ site.baseurl }}/assets/figures/poster-latam-school2023.png" target="_blank">
<img alt="Nosso poster na LATAM School" src="{{ site.baseurl }}/assets/figures/poster-latam-school2023.png"/>
</a>

<center><i>
Figure 7 - Nosso poster na LATAM School
</i><p/></center>









