---
layout: post
title: Apresentando sobre "como melhorar a capacidade operacional de microsserviços" na Trilha da Indústria CBSoft 2023
author: "Leonardo Leite"
lang: pt-br
lang-ref: chastra-presentation-cbsoft-2023
hiden: false
---

Foi com alegria que apresentamos nosso artigo *Microsserviços, por que tão difícil? Um catálogo de padrões para criar serviços bons de se operar* na Trilha da Indústria do CBSoft 2023 (Congresso Brasileiro de Software: Teoria e Prática)!!!

![Leonardo e Alberto, apresentadores do trabalho, com um banner do CBSoft]({{ site.baseurl }}/assets/figures/chastra-cbsoftt2023-1.jpg)

<p><center><i>
Figura 1 - Leonardo Leite e Alberto Marianno, autores e apresentadores do referido artigo
</i></center></p>

## Slides da apresentação

<iframe src="https://www.slideshare.net/slideshow/embed_code/key/rURWEECQtmIcqo?hostedIn=slideshare&page=upload" width="714" height="600" frameborder="0" marginwidth="0" marginheight="0" scrolling="no"></iframe>

## Fotos da apresentação

![Foto da apresentação]({{ site.baseurl }}/assets/figures/chastra-cbsoftt2023-2.jpg)

![Foto da apresentação]({{ site.baseurl }}/assets/figures/chastra-cbsoftt2023-3.jpg)

![Foto da apresentação]({{ site.baseurl }}/assets/figures/chastra-cbsoftt2023-4.jpg)

![Foto da apresentação]({{ site.baseurl }}/assets/figures/chastra-cbsoftt2023-5.jpg)

## Sobre o artigo

Autores: Leonardo Leite e Alberto Marianno - ambos pelo Serviço Federal de Processamento de Dados (Serpro).

Resumo. *Apresentamos aqui um catálogo de padrões para aprimorar a operacionalização de serviços, o que reduz o tempo de reparo. Tal catálogo é baseado na prática de equipes do Serpro, estatal federal de tecnologia. Contudo, há um ônus considerável para se aplicar todos esses padrões. É possível também a má aplicação e o esquecimento deles. Assim, o catálogo traz reflexões sobre dificuldades em se produzir microsserviços (muitos serviços com constante atualização). Tais reflexões podem basear futuras pesquisas.*

Com o uso de [plataformas internas de infraestrutura]({{site.baseurl}}{% post_url en/2020-03-17-times-de-plataforma%}.html){:target="_ blank"}, os desenvolvedores passam a responder pelos incidentes de seus serviços. Do ponto de vista dos desenvolvedores, essa é uma atividade indesejada, que interrompe o fluxo de desenvolvimento, comprometendo prazos de entrega. Portanto, a questão é: o que pode a equipe de desenvolvimento fazer para aumentar sua capacidade operacional? Isto é, como desenvolver um serviço de forma que ele gere menos acionamentos (incidentes, dúvidas, etc.) e que os acionamentos gerados possam ser atendidos mais rapidamente? É esse problema que o Chastra, nosso catálogo de padrões apresentado no artigo, ataca.

<br/>
<center>
<a href="https://sol.sbc.org.br/index.php/cbsoft_estendido/article/view/26042" class="button" target="_blank">Download do texto completo aqui</a>
</center>
<br/>

[Mais sobre nosso catálogo de padrões para melhorar a operação de microsserviços se encontra em nosso post anterior sobre o referido artigo]({{site.baseurl}}{% post_url pt/2023-09-14-chastra-no-cbsoft%}.html){:target="_blank"}.

## Sobre o evento

A Trilha da Indústria do CBSoft 2023 tem por objetivo fomentar a cooperação entre a academia, o mercado e o governo, com palestras, artigos e discussões. Ela visa reunir profissionais dos três setores para compartilhar seus conhecimentos e suas experiências com a comunidade. Essa é uma excelente oportunidade para estabelecer relações de cooperação entre a comunidade acadêmica e as comunidades de profissionais. É ainda uma oportunidade para estabelecer relações de pesquisa aplicada, fomentando o desenvolvimento do mercado e a inovação com transferência de tecnologias.

O Congresso Brasileiro de Software: Teoria e Prática (CBSoft) é um evento realizado anualmente pela Sociedade Brasileira de Computação (SBC) com o objetivo de promover e incentivar a troca de experiências entre pesquisadores e profissionais da indústria e academia sobre as mais recentes pesquisas, tendências e inovações práticas e teóricas sobre software. Realizado desde 2010 como evento agregador de simpósios brasileiros promovidos pela SBC na área de software, o CBSoft tornou-se um dos principais eventos da comunidade científica brasileira na área de Computação. A 14ª edição do CBSoft foi realizada em setembro de 2023 na Universidade Federal de Mato Grosso do Sul (UFMS), localizado em Campo Grande (MS).

