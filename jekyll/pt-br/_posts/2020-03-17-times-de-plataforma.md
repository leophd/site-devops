---
layout: post
title: "Times de Plataforma: uma Estrutura Organizacional para a Entrega Contínua"
author: "Leonardo Leite"
lang: pt-br
lang-ref: platform-teams
---

Temos o prazer de anunciar que nosso artigo **Platform Teams: An Organizational Structure
for Continuous Delivery** foi aceito para publicação no [6º International Workshop on Rapid Continuous Software Engineering (RCoSE 2020)](http://continuous-se.org/RCoSE2020/){:target="_blank"}.

Após entrevistarmos 27 profissionais de TI e analisarmos cuidadosamente essas conversas, iniciamos a elaboração de uma taxonomia com quatro padrões de estruturas organizacionais: (1) departamentos em silos, (2) DevOps clássico, (3) equipes cross-funcionais e (4) times de plataforma. **Times de plataforma são equipes de infraestrutura que fornecem serviços de infraestrutura altamente automatizados que são utilizados pelos desenvolvedores para a implantação de novos serviços.** Observamos que a estrutura de time de plataforma é a classificação mais distintiva de nossa taxonomia, tendo resultados promissores relativos ao [desempenho da entrega]({{ site.baseurl }}{% post_url pt/2019-12-03-desempenho-de-entrega %}.html){:target="_blank"}. Assim, **neste artigo, nos concentramos em descrever a estrutura de time de plataforma** (Figura 1). Nós somos os primeiros a apresentar esse conceito emergente em um trabalho acadêmico.

![Ilustração conceitual de times de plataforma; diagrama com caixas e flechas.]({{ site.baseurl }}/assets/figures/platform-teams-concepts.png)

<center><i>
Figura 1 - Ilustração conceitual de times de plataforma
</i><p/></center>

<br/>
<center>
<a href="https://dl.acm.org/doi/10.1145/3387940.3391455?cid=81413601887" class="button" target="_blank"> Download do texto completo aqui (em inglês)</a>
</center>
<br/>

O workshop RCoSE é realizado em conjunto com a International Conference on Software Engineering (ICSE), a conferência mais conceituada sobre engenharia de software do mundo. Nosso trabalho também foi aceito para a <a href="https://conf.researchr.org/track/icse-2020/icse-2020-poster" target="_blank">trilha de pôsteres do ICSE 2020 </a>.

O ICSE e o RCoSE foram planejados para maio na Coreia do Sul. No entanto, devido à crise do Covid-19, os eventos foram adiados para outubro.


