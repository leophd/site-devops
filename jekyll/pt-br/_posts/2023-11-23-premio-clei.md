---
layout: post
title: "Segundo lugar no Concurso Latino-Americano de Teses de Doutorado!"
author: "Leonardo Leite"
lang: pt-br
lang-ref: clei-award
hiden: false
---

Temos o prazer de anunciar que nossa tese ([A grounded theory of organizational structures for development and infrastructure professionals in software-producing organizations]({{ site.baseurl }}{% post_url en/2022-07-13-thesis-published %}.html){:target="_blank"}) recebeu o prêmio de segundo lugar no IX Concurso Latino-Americano de Teses de Doutorado (CLTD 2023)!!!

![Com amigos bolivianos]({{ site.baseurl }}/assets/figures/clei10.jpg)

<p><center><i>
Figura 1 - Durante o concurso
</i></center></p>

------------------------

![Contendo uma mesa com autoridades]({{ site.baseurl }}/assets/figures/clei11.jpg)

<p><center><i>
Figura 2 - Recebendo o prêmio
</i></center></p>

------------------------

![]({{ site.baseurl }}/assets/figures/clei12.jpg)

<p><center><i>
Figura 3 - Os três finalistas - eu, Gabriela Cajamarca (Equador) e Rodrigo Silva (Unicamp / UFABC)
</i></center></p>

## A conferência

As conferências CLEI são realizadas desde 1974 pelo Centro Latino-Americano de Estudos de Informática (CLEI) e são o principal espaço latino-americano para o intercâmbio de ideias, experiências, resultados e aplicações entre pesquisadores, professores e estudantes de Informática e Ciência da Computação.

A CLEI 2023 foi sediada em La Paz, Bolívia, na Universidad Mayor de San Andrés (*Computación análisis y semántica, adelante informática!!!*).

[Algumas entrevistas sobre o CLEI 2023 podem ser conferidas no TikTok da Carrera de Informática da UMSA](https://www.tiktok.com/@carrera.informatica/){:target="_blank"}.

![]({{ site.baseurl }}/assets/figures/clei20.jpg)

<p><center><i>
Figura 4 - Amigos de Tarija e UMSA
</i></center></p>

------------------------

![]({{ site.baseurl }}/assets/figures/clei21.jpg)

<p><center><i>
Figura 5 - LAWCC – XV Congreso de la Mujer Latinoamericana en la Computación
</i></center></p>

------------------------

![Contendo um mapa mostrando blocos econômicos (por exemplo, Mercosul)]({{ site.baseurl }}/assets/figures/clei22.jpg)

<p><center><i>
Figura 6 - Excelente palestra sobre complexidade econômica
</i></center></p>

------------------------

![Com garrafas de vinho!]({{ site.baseurl }}/assets/figures/clei23.jpg)

<p><center><i>
Figura 7 - Terminando
</i></center></p>

## Cultural 

![Artistas tocando e dançando]({{ site.baseurl }}/assets/figures/clei50.jpg)

<p><center><i>
Figura 8 - Quermesse com Huayna Wila
</i></center></p>

------------------------

![Artistas tocando e dançando; eu com eles]({{ site.baseurl }}/assets/figures/clei51.jpg)

<p><center><i>
Figura 9 - Quermesse com Jach'a Malku
</i></center></p>

------------------------

![Artistas tocando e dançando; eu com vinho]({{ site.baseurl }}/assets/figures/clei52.jpg)

<p><center><i>
Figura 10 - Abertura da CLEI
</i></center></p>

------------------------

![Eu com o pessoal da CLEI]({{ site.baseurl }}/assets/figures/clei53.jpg)

<p><center><i>
Figura 11 - Jantar da CLEI e outras cenas
</i></center></p>

------------------------

![Artistas tocando e dançando; eu com eles]({{ site.baseurl }}/assets/figures/clei54.jpg)

<p><center><i>
Figura 12 - Festa da CLEI com Huayna Wila (sim, de novo, mas agora ainda melhor!)
</i></center></p>

------------------------

![Artistas tocando e dançando]({{ site.baseurl }}/assets/figures/clei55.jpg)

<p><center><i>
Figura 13 - Encerramento da CLEI
</i></center></p>

## Turismo em La Paz

![]({{ site.baseurl }}/assets/figures/clei60.jpg)

<p><center><i>
Figura 14 - Indo para El Alto de teleférico (La Paz tem a maior rede de teleférico do mundo)
</i></center></p>

------------------------

![]({{ site.baseurl }}/assets/figures/clei61.jpg)

<p><center><i>
Figura 15 - Mirante Killi Killi com Danner (de Santa Cruz de la Sierra) e Gabriel (USP)
</i></center></p>

------------------------

![Temas: instrumentos musicais, trabalho nas minas, máscaras folclóricas e gorros tradicionais]({{ site.baseurl }}/assets/figures/clei62.jpg)

<p><center><i>
Figura 16 - O excelente Museo Nacional de Etnografía y Folkore em La Paz
</i></center></p>

## Lembranças

![Pulseira de temática Boliviana em meu pulso e lhama de brinquedo na mão de um bebê]({{ site.baseurl }}/assets/figures/clei71.jpg)

<p><center><i>
Figura 17 - Lembranças
</i></center></p>
















