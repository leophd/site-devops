---
layout: post
title: "Nosso livro sobre DevOps foi publicado!"
author: "Leonardo Leite"
lang: pt-br
lang-ref: book-published
---

Temos o prazer de anunciar que nosso livro, derivado de nossos resultados de pesquisa e intitulado **Como se faz DevOps: Organizando pessoas, dos silos aos times de plataforma**, foi publicado pela Casa do Código, a editora de livros de TI mais popular no Brasil!

![Eu segurando o livro]({{ site.baseurl }}/assets/figures/foto-livro-devops.jpg)


<br/>
<center>
<a href="https://www.casadocodigo.com.br/products/livro-como-se-faz-devops" target="_blank" class="button">Adquira o livro aqui!</a>
</center>
<br/>

Se preferir ler no Kindle:

<br/>
<center>
<a href="https://www.amazon.com.br/Como-faz-DevOps-Organizando-plataforma-ebook/dp/B0CSF6NSTQ/ref=sr_1_1" target="_blank" class="button">Adquira o livro para Kindle aqui!</a>
</center>
<br/>


O livro é escrito em português e tem como objetivo principal impactar a indústria brasileira. Assim, embora o livro se baseie em resultados de pesquisa acadêmica, a escrita (além de ser em português) é adaptada para que o texto seja proveitoso e fluído para os profissionais da indústria (devs, ops e gestores).

![A capa do livro]({{ site.baseurl }}/assets/figures/capa-livro-devops.jpg)



