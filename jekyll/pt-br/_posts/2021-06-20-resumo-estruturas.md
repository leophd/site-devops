---
layout: post
title: "Um resumo das estruturas organizacionais em busca da entrega contínua"
author: "Leonardo Leite, Fabio Kon, Gustavo Pinto, and Paulo Meirelles"
lang: pt-br
lang-ref: structures-digest-ist
hiden: false
published: true
---

O **movimento DevOps**, conforme retratado vividamente no livro *O Projeto Fênix*, surgiu como uma mudança cultural para **quebrar os silos** em grandes organizações, integrando melhor as equipes de desenvolvimento e operações por meio da **colaboração**. No entanto, essa colaboração pode acontecer de maneiras diferentes de uma perspectiva organizacional: os desenvolvedores e especialistas em infraestrutura podem fazer parte de departamentos diferentes ou podem estar juntos em uma única equipe **multifuncional**. Com os avanços nas ofertas de PaaS, é possível até mesmo imaginar os próprios desenvolvedores assumindo as responsabilidades das operações.

Nossa pesquisa no IME-USP (Universidade de São Paulo) tem como objetivo investigar como as empresas produtoras de software estão organizando suas equipes de desenvolvimento e operações. Para isso, estamos entrevistando profissionais de software para entender como as coisas estão realmente acontecendo no mundo real. Com nossa pesquisa, esperamos fornecer uma teoria para apoiar as organizações no desenho de suas estruturas organizacionais em direção à entrega contínua e fornecer orientação sobre as consequências da escolha de uma determinada estrutura organizacional.

Com base na análise criteriosa das entrevistas realizadas, elaboramos uma teoria que descreve as **estruturas organizacionais** utilizadas pela indústria no mundo real sobre como o trabalho dos desenvolvedores e engenheiros de infraestrutura pode ser coordenado na busca pela entrega contínua. Neste resumo, apresentamos nosso entendimento atual de tais estruturas organizacionais: departamentos segregados, departamentos de colaboração, departamentos mediados por API e departamentos únicos. Cada uma dessas estruturas possui **propriedades centrais**, comumente vistas em organizações com uma determinada estrutura, e **propriedades suplementares**, que podem ou não estar presentes, mas que quando presentes dão suporte à explicação da estrutura.

![Imagens no estilo homem palito representando as quatro estruturas organizacionais de nossa taxonomia (explicadas neste post).]({{ site.baseurl }}/assets/figures/4structures.png)

<center><i>
Figura 1 - As quatro estruturas organizacionais de nossa taxonomia
</i><p/></center>

A apresentação desta taxonomia já foi publicada na [Information and Software Technology](https://www.sciencedirect.com/science/article/abs/pii/S0950584921001324){:target="_blank"}, uma revista acadêmica muito conceituada em nosso campo de pesquisa.

<br/>
<center>
<a href="https://arxiv.org/abs/2008.08652" target="_blank" class="button">Download do artigo completo aqui (em inglês)</a>
</center>
<br/>

Entre a submissão e a aceitação do artigo (quase um ano), temos trabalhado para evoluir nossa taxonomia com base em seu uso com profissionais. As mudanças mais relevantes a partir dessa evolução são renomeações de conceitos, visando mais objetividade e menos confusão para os usuários da taxonomia. Neste resumo, apresentamos a taxonomia em sua versão mais recente (junho de 2021).

![Diagrama com as quatro estruturas organizacionais e suas propriedades complementares, todas explicadas neste post. Diagrama com caixas e setas.]({{ site.baseurl }}/assets/figures/taxonomy-v12.png)

<center><i>
Figura 2 - Visão de alto nível de nossa taxonomia em sua última versão (junho de 2021)
</i><p/></center>

A seguir, apresentamos cada estrutura com suas propriedades centrais e suplementares.

## Departamentos de devs & infra segregados

Departamentos segregados, ou em silos, é a estrutura "pré-DevOps" existente em grandes organizações, apresentando colaboração limitada entre departamentos e barreiras para entrega contínua. Essa estrutura é considerada o problema que o DevOps veio resolver.

![A figura mostra um operador e um desenvolvedor, cada um dentro de um círculo diferente. Eles estão de costas um para o outro. A comunicação flui por meio de cartas, mais do desenvolvedor para o operador do que do operador para o desenvolvedor.]({{ site.baseurl }}/assets/figures/siloed-departments.png)

<center><i>
Figura 3 - Com departamentos segregados, operadores e desenvolvedores estão cada um em suas próprias bolhas. Eles não interagem muito diretamente, e a comunicação flui lentamente por meios burocráticos (sistemas de tickets).
</i><p/></center>

Propriedades centrais:

* Os desenvolvedores e operadores têm funções diferentes e bem definidas.
* Os desenvolvedores têm uma visão muito limitada do que acontece na produção.
* O monitoramento e o tratamento dos incidentes são feitos principalmente pela equipe de infraestrutura.
* Os desenvolvedores frequentemente negligenciam os requisitos não funcionais (FNR).
* São muitos os conflitos entre os silos, pois cada departamento visa seus próprios interesses, buscando a otimização local, não a otimização global de toda a organização.
* As iniciativas de DevOps são centradas na adoção de ferramentas de integração contínua, em vez de melhorar a colaboração entre os silos.
* A falta de testes automatizados adequados pode prejudicar o [desempenho de entrega]({{site.baseurl}}{% post_url en/2019-12-03-desempenho-de-entrega%}.html){:target="_ blank"} e a confiabilidade.

## Departamentos de devs & infra colaborativos

Essa estrutura foca na colaboração entre os desenvolvedores e a equipe de infraestrutura. É assim que o livro "The Phoenix Project" retrata o DevOps.

![A figura mostra um operador e um desenvolvedor, cada um dentro de um círculo diferente. Eles estão olhando um para o outro e estão de mãos dadas (com alguma dificuldade).]({{ site.baseurl }}/assets/figures/classical-devops.png)

<center><i>
Figura 4 - Com departamentos colaborativos, operadores e desenvolvedores em diferentes departamentos procuram trabalhar juntos, mesmo que não seja fácil, por meio de contato direto e colaboração próxima.
</i><p/></center>

Propriedades centrais:

* Existe uma cultura de colaboração e comunicação entre as áreas, que atuam de forma alinhada.
* As responsabilidades sobre RNFs são compartilhadas entre os desenvolvedores e a equipe de infraestrutura.
* A equipe de infraestrutura ainda está na linha de frente no monitoramento e tratamento de incidentes.
* Enquanto os desenvolvedores se sentem aliviados porque podem contar com a equipe de infraestrutura, o estresse pode persistir em
altos níveis para a equipe de infraestrutura.
* Não há correlação aparente entre essa estrutura e o desempenho de entrega.

Propriedades suplementares:

* *Pessoas de infra como colaboradores de desenvolvimento*: engenheiros de infraestrutura têm habilidades de codificação avançadas e contribuem com a base de código da aplicação para melhorar as propriedades não funcionais da aplicação.
* *Pessoas de infra ocasionalmente incorporadas em equipes de desenvolvimento centradas no produto*: às vezes, a colaboração ocorre com um profissional de infraestrutura que passa algum tempo em estreita colaboração com uma equipe de desenvolvimento, especialmente no início de um novo projeto.

## Departamentos de devs & infra mediados por API

Nesse caso, existe uma equipe de infraestrutura (também chamada de time de plataforma), e ela fornece serviços de infraestrutura altamente automatizados ("a plataforma") abstraindo a infrasestrutura para empoderar as equipes de produto. 

![A figura mostra um operador e um desenvolvedor, cada um dentro de um círculo diferente. O operador fornece uma interface que é consumida pelo desenvolvedor (notação UML).]({{ site.baseurl }}/assets/figures/platform-team.png)

<center><i>
Figura 6 - O time de plataforma fornece serviços automatizados para serem usados, com pouco esforço, pelos desenvolvedores (a API da plataforma medeia a interação entre os desenvolvedores e o time de plataforma). O time de plataforma e os desenvolvedores estão abertos para ouvir e apoiar uns aos outros.
</i><p/></center>

Propriedades centrais:

* A existência de uma plataforma de entrega permite que a equipe de produto opere seus próprios serviços de negócios em produção e minimiza a necessidade de especialistas em infraestrutura na equipe de produto.
* Embora a equipe do produto se torne totalmente responsável pelos RNFs de seus serviços, isso não é um fardo significativo, pois a plataforma abstrai a infraestrutura subjacente e lida com várias questões de RNFs.
* A equipe do produto é a primeira a ser chamada em caso de incidente; o pessoal de infraestrutura é escalado se o problema estiver relacionado a algum serviço de infraestrutura.
* As equipes de produto se desacoplam dos membros do time de plataforma.
* Normalmente, a comunicação entre a equipe de desenvolvimento e o time de plataforma acontece quando os membros da infraestrutura fornecem consultoria para os desenvolvedores ou quando os desenvolvedores exigem novos recursos para a plataforma.
* A equipe de infraestrutura não é mais solicitada para tarefas operacionais.
* A plataforma pode não ser suficiente para atender a requisitos específicos; normalmente, a plataforma é adaptada para o caso típico de funcionalidade construída na organização.
* Os especialistas em infraestrutura possuem habilidades de codificação.
* Organizações com equipes de plataforma apresentam os melhores resultados em termos de [desempenho de entrega]({{site.baseurl}}{% post_url en/2019-12-03-desempenho-de-entrega%}.html){:target="_ blank"}.
* Não é para organizações pequenas.

Propriedades suplementares:

* *Fachada para a nuvem com API especializada*: a organização fornece uma plataforma que consome uma nuvem pública (por exemplo, AWS ou Google Cloud), mas de uma forma que os desenvolvedores podem nem estar cientes disso.
* *Plataforma de código aberto administrada internamente*: a organização gerencia uma plataforma de código aberto, como o Rancher.
* *Plataforma customizada*: a organização construiu sua própria plataforma devido às necessidades específicas da organização.

## Departamento dev/infra único

Um único departamento é responsável pelo desenvolvimento de software e pelo gerenciamento da infraestrutura. Está mais alinhado com o lema da Amazon "*Você construiu, você cuida*", dando mais liberdade para a equipe e também muita responsabilidade.

![A figura mostra um operador e um desenvolvedor, eles estão dentro do mesmo círculo.]({{ site.baseurl }}/assets/figures/cross-functional-team.png)

<center><i>
Figura 5 - Um único departamento cuida do desenvolvimento e da infraestrutura.
</i><p/></center>

Propriedades centrais:

* A comunicação e a padronização entre equipes multifuncionais dentro de uma única organização devem ser bem gerenciadas para evitar esforços duplicados.
* Um desafio na formação de equipes multifuncionais é garantir que elas tenham membros com as habilidades necessárias.
* Não prevalente em grandes organizações.
* Embora essa estrutura seja comumente associada à utilização de serviços de nuvem, tal utilização não é obrigatória.

Propriedades suplementares:

* *Profissionais de infraestrutura dedicados*: a equipe conta com pelo menos um especialista em infraestrutura. Pode ser também que o departamento tenha uma equipe de infraestrutura dedicada a um grupo de desenvolvedores. Nesse caso, a diferença para departamentos colaborativos é a hierarquia comum (mesmo chefe) para o pessoal de desenvolvimento e de infraestrutura.
* *Desenvolvedores com background e atribuições em infraestrutura*: a equipe possui pelo menos um desenvolvedor com conhecimentos avançados de infraestrutura.
* *Pouco esforço de infra*: não há necessidade de conhecimentos avançados em infraestrutura.

## Other supplementary properties

* *Equipe facilitadora*: fornece consultoria e ferramentas para equipes de produtos, mas não possui nenhum serviço ou infraestrutura no ambiente de produção. A consultoria pode ser, por exemplo, sobre desempenho e segurança. As ferramentas fornecidas pelas equipes facilitadoras incluem o pipeline de implantação, mecanismos de alta disponibilidade e ferramentas de monitoramento. Outro tipo de equipe facilitadora são os comitês para coordenar o trabalho dos grupos de desenvolvimento e infraestrutura. Nós os encontramos em todas as estruturas organizacionais.
* *Com uma plataforma*: a organização possui uma plataforma capaz de fornecer automação de implantação, mas sem seguir os padrões de interação humana e colaboração descritos pelas propriedades centrais dos departamentos mediados por API. Isso inclui organizações com uma única equipe multifuncional construindo uma plataforma e organizações com interações em silos entre os desenvolvedores e o time de plataforma.

Para saber os detalhes sobre como descobrimos essas estruturas, leia o artigo completo!

<br/>
<center>
<a href="https://arxiv.org/abs/2008.08652" target="_blank" class="button">Download do artigo completo aqui (em inglês)</a>
</center>
<br/>

## Recapitulando

Agora fornecemos resumidamente para cada estrutura descoberta: (i) a diferenciação entre os grupos de desenvolvimento e de infraestrutura em relação às atividades operacionais (implantação, configuração da infraestrutura e operação do serviço em tempo de execução); e (ii) como esses grupos interagem (integração).

* <strong>Departamentos de dev & infra segregados</strong>
  * <strong>Diferenciação do desenvolvimento:</strong> apenas constrói o pacote da aplicação
  * <strong>Diferenciação da infraestrutura:</strong> responsável por todas as atividades operacionais
  * <strong>Integração:</strong> colaboração limitada entre os grupos

* <strong>Departamentos de dev & infra colaborativos</strong>
  * <strong>Diferenciação do desenvolvimento:</strong> participa / colabora em algumas atividades operacionais
  * <strong>Diferenciação da infraestrutura:</strong> responsável por todas as atividades operacionais
  * <strong>Integração:</strong> colaboração intensa entre os grupos

* <strong>Departamento dev/infra único</strong>
  * <strong>Diferenciação do desenvolvimento:</strong> responsável por todas as atividades operacionais
  * <strong>Diferenciação da infraestrutura:</strong> não existe
  * <strong>Integração:</strong> --

* <strong>Departamentos de dev & infra mediados por API</strong>
  * <strong>Diferenciação do desenvolvimento:</strong> responsável por todas as atividades operacionais com o apoio da plataforma
  * <strong>Diferenciação da infraestrutura:</strong> fornece uma plataforma automatizando muitas das atividades operacionais
  * <strong>Integração:</strong> a interação acontece para situações específicas, não diariamente

## Mais algumas considerações

Em primeiro lugar, se alguém classificar uma organização específica como aderente a uma das estruturas acima, não significa que a empresa em particular irá necessariamente seguir todas as características da estrutura. Cada organização é única e possui adaptações ao seu contexto. No entanto, o modelo pode auxiliar na compreensão das organizações e no apoio à tomada de decisões em relação às mudanças organizacionais.

Por ora, acreditamos que nossa taxonomia fornece os seguintes benefícios principais:

* Oferece conceitos para distinguir claramente departamentos colaborativos e departamentos únicos. Em diferentes cenários, as pessoas usam a palavra DevOps para descrever os dois, o que pode confundir e desorientar as organizações que desejam "adotar DevOps".
* Distingue "departamentos mediados por API" como uma estrutura organizacional a parte, deixando claro que ela tem um conjunto diferente de consequências quando comparado a departamentos colaborativos e departamentos únicos.

Outra característica distintiva de nosso modelo é que ele é baseado em dados recuperados de profissionais de software no mundo real.






