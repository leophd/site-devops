---
layout: post
title: "Headers HTTP para rastro distribuído (observabilidade) - Parte 2:  OpenTracing e OpenTelemetry"
author: "Leonardo Leite"
lang: pt-br
lang-ref: http-headers-distributed-tracing-pt2
hiden: false
---

Esta série de posts investiga os **headers HTTP** usados para apoiar o **rastro distribuído** (*tracing*), que é um dos pilares da **observabilidade** (composta também por logs e métricas). Embora foquemos na investigação sobre esses headers, estes posts servem também como uma introdução sobre rastro distribuído e como apresentação de algumas alternativas tecnológicas para sua implementação. Como valor agregado extra, destrinchamos e interpretamos a documentação de algumas dessas alternativas; documentação essa, francamente, não tão fácil de assimilar.

## Retomando o assunto

O **rastro distribuído** (*tracing*) diz respeito aos processos, padrões e ferramentas que possibilitam que a equipe de desenvolvimento compreenda as requisições feitas a partir da invocação a um determinado serviço, dando a possibilidade de correlacionar erros ocorridos em diferentes serviços. Exemplo: entender que o erro interno no serviço A foi devido a um determinado problema no serviço B (sendo o serviço B invocado pelo serviço A).

Na solução caseira de rastro distribuído de minha equipe, usamos alguns headers na invocação de um serviço para o outro: o `X-Request-ID`, que identifica a transação distribuída em execução; o `CLIENT_APPLICATION_NAME`, que transporta o nome do serviço cliente que realizou a invocação; e o `CLIENT_CHAIN`, com a lista dos nomes dos serviços que fizeram parte da cadeia de invocações até o momento.

E aí surge a indagação: o que fizemos seria muito diferente do preconizado pelas soluções de mercado? Diante disso, **nossa questão é: poderíamos ter uma solução caseira utilizando headers de apoio a rastro distribuído que fossem compatíveis com soluções de mercado?** Ou seja, não estou muito interessado em adotar uma biblioteca ou framework específico, mas acharia interessante que os headers utilizados na solução caseira fossem padronizados. E uma alternativa que me pareceu promissora em relação a "padronizar o rastro distribuído" foi o OpenTracing, que vamos analisar agora.

Ah, se essa recontextualização não lhe foi o suficiente, considere rever as seções iniciais de nosso [post anterior]({{ site.baseurl }}{% post_url pt-br/2023-12-05-headers-http-rastro-distribuido-p1 %}.html){:target="_blank"} (i.e., as seções antes de iniciarmos a descrição sobre o Spring Cloud Sleuth).

## Introduzindo o OpenTracing

Na documentação do Spring Cloud Sleuth [1], encontramos um parágrafo sobre o OpenTracing, no qual se declara que <span class="citacao">"The OpenTracing initiative is an effort to standardize the vocabulary and concepts of modern tracing for multiple languages and platforms"</span>.

Note que a padronização proposta pelo OpenTracing é sobre “vocabulário e conceitos”. Ou seja, para mim que imaginava o OpenTracing como algum tipo de padrão, fiquei bem surpreso ao descobrir que não há especificação de headers de propagação de contexto para rastro distribuído no OpenTracing!

Mas vamos ao OpenTracing por ele mesmo [2]:

<span class="citacao">What is Distributed Tracing?</span>

<span class="citacao">Distributed tracing, also called distributed request tracing, is a method used to profile and monitor applications, especially those built using a microservices architecture. Distributed tracing helps pinpoint where failures occur and what causes poor performance.</span>

<span class="citacao">What is OpenTracing?</span>

<span class="citacao">OpenTracing is not a download or a program. OpenTracing is not a standard. OpenTracing is comprised of an API specification, frameworks and libraries that have implemented the specification, and documentation for the project. OpenTracing allows developers to add instrumentation to their application code using APIs that do not lock them into any one particular product or vendor.</span>

Apesar dessa descrição do OpenTracing focar na especificação de APIs, na sequência a documentação retoma o aspecto conceitual afirmando que o OpenTracing é composto da <span class="citacao">"OpenTracing Semantic Specification"</span> e de <span class="citacao">"Semantic Conventions"</span>.

No cenário, em que eu me encontrava, em que não necessariamente desejava adotar alguma biblioteca de mercado para rastro distribuído, me pareceu interessante investigar esses conceitos do OpenTracing. Assim, poderíamos ter uma implementação com termos minimamente compatíveis (em termos de semântica) com alguma referência mais bem estabelecida (digo "referência" já que não podemos chamar o OpenTracing de padrão). Então, vamos lá cavar mais.

## The OpenTracing Semantic Specification

A especificação semântica do OpenTracing [3] contém:

* <span class="citacao">The OpenTracing Data Model</span>
  * <span class="citacao">traces, spans, span tags (strings), span logs (key:value maps)</span>
  * <span class="citacao">references: ChildOf and FollowsFrom within SpanContexts</span>
* <span class="citacao">Classes da API: Traces, Span, SpanContext...</span>
  * <span class="citacao">The Tracer interface creates Spans and understands how to Inject (serialize) and Extract (deserialize) their metadata across process boundaries.</span>

O principal aqui é que temos então a definição de termos como "trace" e "span", que são os mesmos termos usados pelo Spring Cloud Sleuth. Lembrando: "trace" (rastro) diz respeito a um conjunto de processamentos ocorridos em diferentes serviços em uma transação distribuída (i.e., uma sequência de invocações entre serviços); já "span" diz respeito ao processamento ocorrido em um serviço específico durante a execução de uma transação distribuída.

Outros termos também definidos nessa especificação e usados no Sleuth é o de "SpanContext" (o que contém o trace id e o span id) e o de "baggage" (<span class="citacao">which are just key:value pairs that cross process boundaries</span>). Além da bagagem, que é transferida entre serviços, cada span também possui em seu contexto um conjunto de chaves-valores denominados de "tags". 

Até aqui temos o reforço de que pode ser boa ideia utilizar certos termos (como trace, span, context, baggage e tags) em soluções caseiras, uma vez que não são termos específicos de apenas um framework.

## Semantic Conventions

Nas convenções semânticas do OpenTracing [4] encontramos que:

* <span class="citacao">The OpenTracing Specification describes the overarching language-neutral data model and API guidelines for OpenTracing. That data model includes the related concepts of Span Tags and (structured) Log Fields; though these terms are defined in the specification, there is no guidance there about standard Span tags or logging keys. Those semantic conventions are described by this document.</span>
* <span class="citacao">Span tags table (component, db.instance, db.statement, db.user, error, http.method, http.status_code, http.url...)</span>

Aqui há uma sugestão de um conjunto de valores para serem usados como chaves das tags que formam o contexto do span. Exemplos: component, db.instance, db.statement, db.user, error, http.method, http.status_code, http.url. Além disso, há uma sugestão de campos que devem aparecer em cada log, que seriam: error.kind, error.object, event, message, stack.

Embora o título "semantic conventions" parecesse promissor, achei essas convenções menos úteis para meu propósito. Vamos pular essa parte.

## Carrier

<!-- continuar aqui -->

Para nós que estamos preocupados com a concretização da propagação de contexto por meio de headers HTTP, um conceito importante do OpenTracing é o de carrier (em português seria “portador”) [5]:

<span class="citacao">A Carrier is an interface or data structure that’s useful for inter-process communication (IPC); that is, the Carrier is something that “carries” the tracing state from one process to another.</span>

<span class="citacao">What the OpenTracing implementations choose to store in these Carriers is not formally defined by the OpenTracing specification, ...</span>

<span class="citacao">Interoperability of OpenTracing implementations across process boundaries: There is no expectation that different OpenTracing implementations Inject and Extract SpanContexts in compatible ways. Though OpenTracing is agnostic about the tracing implementation across an entire distributed system, for successful inter-process handoff it’s essential that the processes on both sides of a propagation use the same tracing implementation.</span>

<span class="citacao">The precise representation of the “Carrier formats” may vary from platform to platform.</span>

Daí que concluímos que OpenTracing não padroniza headers HTTP para a propagação de contexto de rastro distribuído.

## Spans e traces no OpenTracing

Vejamos a visão do OpenTracing sobre esses dois importantes conceitos [6]:

<span class="citacao">The OpenTracing model of computation specifies two primary object types, Spans and Traces, but does not specify identifiers for these objects.</span>

<span class="citacao">Before discussing changes to the OpenTracing specification, it’s worth reviewing several popular wire protocols which contain these trace identifiers.</span>

* <span class="citacao">Trace-Context HTTP Headers (W3C): trace-id and span-id</span>
  * <span class="citacao">Trace-Context HTTP headers are in the process of being standardized via the w3c. The tracing community has voiced strong support in implementing these headers for use in tracing interop.</span>
* <span class="citacao">B3 HTTP Headers: TraceId and SpanId</span>
  * <span class="citacao">The B3 HTTP headers are widely adopted, mostly by Zipkin-like tracing systems. The B3 protocol includes X-B3-TraceId and X-B3-SpanId as required headers, which contain the TraceId and SpanId values, respectively.</span>

Ou seja, a documentação do OpenTracing cita os mesmos padrões de headers HTTP para rastro distribuído que são suportados pelo Spring Cloud Sleuth. Além disso, apresenta os termos trace e span de forma compatível ao utilizado no Spring.

## Relação entre o Spring Cloud Sleuth e o OpenTracing

Em resumo, o Spring Cloud Sleuth [1] promete que:

<span class="citacao">We will keep Spring Cloud Sleuth conceptually compatible with this effort</span>

Ou seja, o Sleuth adota de forma consistente a terminologia promovida pelo OpenTracing, principalmente no que diz respeito aos termos trace e span.

## OpenTelemetry

<span class="citacao">The OpenTracing project is archived. Learn more. Migrate to OpenTelemetry today!</span>

Pois é, segundo a documentação do OpenTelemetry [7], acontece que:

<span class="citacao">OpenTelemetry is a Cloud Native Computing Foundation (CNCF) project that is the result of a merger between two prior projects, OpenTracing and OpenCensus. Both of these projects were created to solve the same problem: the lack of a standard for how to instrument code and send telemetry data to an Observability backend.</span>

Ou seja, o OpenTracing agora é o OpenTelemetry, mas que considera não apenas a questão do rastro distribuído, mas também os outros pilares da observabilidade (métricas e logs). O projeto provê especificações, protocolos, convenções semânticas, bibliotecas e ferramentas para <span class="citacao">"generation, collection, management, and export of telemetry"</span>. E aí esses dados expostos de telemetria podem ser consumidos por outras ferramentas, como o Jaeger (rastro distribuído) e o Prometheus (métricas), para a visualização por humanos.

Mas parece que em relação aos conceitos, as coisas mudaram um tanto. Segundo a página de migração do OpenTracing para o OpenTelemetry [8], parece que termos do Open Tracing como "tags" e "baggage" ficaram defasados ou possuem uma aplicação diferenciada no OpenTelemetry, uma vez que <span class="citacao">OpenTelemetry has an improved model for instrumenting software</span>. Por exemplo, os dados que contextualizam um span não são mais chamados de tags (como era no OpenTracing), mas de "attributes".

Por outro lado, as opções de headers HTTP para propagação de contexto continuam sendo as mesmas (B3 e W3C) [9].

Ou seja, por um lado a padronização terminológica ambicionada pelo OpenTracing não vingou, sendo oficialmente sobrescrita pelo modelo evoluído do OpenTracing. Contudo, por outro lado, essa terminologia do OpenTracning continua lá no Spring Sleuth.

Ah, o próprio OpenTelemetry oferece também bibliotecas para instrumentação personalizada de código. Assim o programador pode definir diferentes trechos de código como correspondentes a diferentes spans [10], além de atribuir "attributes" a esses spans. Fazendo uma mescla de exemplos da documentação [11], teríamos algo como:

```java
import io.opentelemetry.api.trace.Span;

// ...

    Span span = tracer.spanBuilder("myBusinessWork").startSpan();

    span.setAttribute("http.method", "GET"); // attributes, não tags!
    span.setAttribute("http.url", url.toString());

    // Make the span the current span
    try (Scope scope = span.makeCurrent()) {

        // trecho de código que faz alguma coisa a ser instrumentada
        // exemplos: processamento de uma requisição http,
        // mesmo uma chamada http a outro serviço
        // ou algum processamento mais pesado, como manipulação de arquivo.
      
    } finally {
      span.end();
    }
```

## Conclusão

Em essência OpenTracing provê um modelo de rastro distribuído. I.e., define a terminologia dessa atividade, especialmente sobre os termos "trace" e "span". Essa terminologia é abraçada pelo Spring Cloud Sleuth, de forma que podemos dizer que o Sleuth é compatível com o OpenTracing. 

No mais, em particular, o OpenTracing não define os nomes dos headers HTTP que devem transferir de um serviço para o outro as informações relativas ao rastro distribuído. A documentação do OpenTracing acaba citando os padrões da W3C e o B3, que são as opções disponíveis no Spring Sleuth.

Contudo, o OpenTracing está agora obsoleto e foi absorvido pelo projeto OpenTelemetry, que possui um modelo evoluído, de forma a não necessariamente se manter a compatibilidade com a terminologia pregada pelo OpenTracing. Esse desfecho nos deixa mais a vontade para adotarmos em soluções caseiras os termos que acharmos mais convenientes, independentemente dos termos usados em eventuais padrões, uma vez que essa parece ser uma área ainda não tão consolidada.

Na próxima parte dessa série vamos pontuar mais algumas referências que podem nortear as escolhas de nomes de termos e headers de HTTP em soluções caseiras de rastro distribuído. 

-------------------

## Referências

[1] Documentação sobre o Spring Cloud Sleuth citando por cima o OpenTracing: [https://spring.io/blog/2016/02/15/distributed-tracing-with-spring-cloud-sleuth-and-spring-cloud-zipkin#the-opentracing-initiative](https://spring.io/blog/2016/02/15/distributed-tracing-with-spring-cloud-sleuth-and-spring-cloud-zipkin#the-opentracing-initiative){:target="_blank"}

[2] Documentação do OpenTracing sobre "What is Distributed Tracing?": [https://opentracing.io/docs/overview/what-is-tracing/](https://opentracing.io/docs/overview/what-is-tracing/){:target="_blank"}

[3] The OpenTracing Semantic Specification: [https://github.com/opentracing/specification/blob/master/specification.md](https://github.com/opentracing/specification/blob/master/specification.md){:target="_blank"}

[4] OpenTracing Semantic Conventions: [https://github.com/opentracing/specification/blob/master/semantic_conventions.md](https://github.com/opentracing/specification/blob/master/semantic_conventions.md){:target="_blank"}

[5] OpenTracing - Inject, Extract, and Carriers: [https://opentracing.io/docs/overview/inject-extract/](https://opentracing.io/docs/overview/inject-extract/){:target="_blank"}

[6] OpenTracing Specification - Trace Identifiers: [https://github.com/opentracing/specification/blob/master/rfc/trace_identifiers.md](https://github.com/opentracing/specification/blob/master/rfc/trace_identifiers.md){:target="_blank"}

[7] What is OpenTelemetry?: [https://opentelemetry.io/docs/what-is-opentelemetry/](https://opentelemetry.io/docs/what-is-opentelemetry/){:target="_blank"}

[8] Migrating from OpenTracing: [https://opentelemetry.io/docs/migration/opentracing/](https://opentelemetry.io/docs/migration/opentracing/){:target="_blank"}

[9] Observability in Java: Getting Started with OpenTelemetry (em 27'04''): [https://youtu.be/C374RMCK3kY?si=DnjNsWB3A2uNQt4V](https://youtu.be/C374RMCK3kY?si=DnjNsWB3A2uNQt4V){:target="_blank"}

[10] Observabilidade e Open Telemetry na Prática (a partir do 22'06''): [https://youtu.be/Y0gNpjHwx2M?si=dZ82PAewrZK3D_pN](https://youtu.be/Y0gNpjHwx2M?si=dZ82PAewrZK3D_pN){:target="_blank"}

[11] Manual instrumentation for OpenTelemetry Java (seções "Create Spans" e "Span Attributes"): [https://opentelemetry.io/docs/languages/java/instrumentation/](https://opentelemetry.io/docs/languages/java/instrumentation/){:target="_blank"}

