---
layout: post
title: "TDC São Paulo 2022 - Interação entre times de plataforma e desenvolvedores: como deve ser? "
author: "Leonardo Leite"
lang: pt-br
lang-ref: tdc-2022
hiden: false
published: true
---

Nesta semana tive a honra e prazer de dar uma palestra sobre times de plataforma na <a href="https://thedevconf.com/tdc/2022/business/trilha-devops" target="_blank">trilha de DevOps</a> do The Developer Conference (TDC) São Paulo 2022, o maior evento sobre desenvolvimento de software do Brasil. Muito agradecido pelo acolhimento da Fabricia Dora e de toda a coordenação, assim como pelo suporte de toda a equipe. Foi fantástico! Em especial, conseguimos realizar uma sessão altamente interativa, em que o público compartilhou suas experiências com times de plataforma.

<br/>

![Painel de fotos do TDC com Leonardo palestrando, Leonardo ao lado de outros participantes e público assistindo a palestra.]({{ site.baseurl }}/assets/figures/tdc2022-1.jpg)

<br/>

![Painel de fotos do TDC com Leonardo ao lado de outros participantes.]({{ site.baseurl }}/assets/figures/tdc2022-2.jpg)

<br/>

Sinopse da palestra:

*Times de plataforma têm sido um caminho para lidar com os dilemas da divisão do trabalho entre os grupos de infraestrutura e de desenvolvimento, resultando na aceleração da entrega contínua. Nesse paradigma, desenvolvedores usam a plataforma de forma autônoma para implantar e operar seus serviços. Assim, eles não precisam interagir com a equipe da plataforma diariamente. Porém, ainda assim, não adianta ter a plataforma sem que haja certos padrões de colaboração entre o time de plataforma e os desenvolvedores. São esses padrões de colaboração, apresentados nesta palestra baseada em minha pesquisa de doutorado na USP, que tornam o paradigma de times de plataforma saudável e sustentável.*

<br/>
<center>
<a href="https://pt.slideshare.net/leonardoferreiraleite/interao-entre-times-de-plataforma-e-desenvolvedores-como-deve-ser" class="button" target="_blank">Slides aqui</a>
</center>
<br/>

![Logo do TDC]({{ site.baseurl }}/assets/figures/slides-tdc-2022.png)

## Vídeo

<iframe width="560" height="315" src="https://www.youtube.com/embed/luibfvF2lY0?si=DyOr-uH6sWi0YGhG" title="Vídeo da palestra no TDC" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>



