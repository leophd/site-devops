---
layout: post
title: "Padrões de interação para times de plataforma"
author: "Leonardo Leite"
lang: pt-br
lang-ref: interaction-patterns-platform-teams
---

<!-- summary: Para tarefas diárias, os desenvolvedores não devem incomodar as pessoas da equipe da plataforma; eles devem usar os serviços de infraestrutura de forma autônoma. No entanto, há momentos em que essas equipes devem interagir diretamente. Vamos apresentar esses padrões de interação. #devops -->

Em nossa pesquisa, dizemos que as equipes de plataforma e as equipes de desenvolvimento são organizadas de acordo com a estrutura organizacional dos departamentos mediados por API. Aqui, estrutura organizacional define a divisão do trabalho e os modos de interação entre esses dois grupos - a equipe de plataforma (ou seja, a equipe de infraestrutura) e os desenvolvedores. Neste post, vamos nos concentrar na questão das interações.

A estrutura de departamentos mediados por API é muito mais sobre o "mediados" do que sobre o "API". Isso significa que, para tarefas diárias, os desenvolvedores não devem pedir diretamente ou via ticket às pessoas de infraestrutura para fornecer coisas. Os desenvolvedores devem utilizar a infraestrutura de forma autônoma, ou seja, utilizando a plataforma automatizada. Essa propriedade é o que torna os departamentos mediados por API tão poderosos: dessa forma, o time de infraestrutura deixa de ser um gargalo no caminho de entrega e, assim, o desempenho da entrega aumenta.

![A figura mostra um operador e um desenvolvedor, cada um dentro de um círculo diferente. O operador fornece uma interface que é consumida pelo desenvolvedor (notação UML).]({{ site.baseurl }}/assets/figures/platform-team.png)

<center><i>
Figura 1 - O time de plataforma fornece serviços automatizados para serem usados, com pouco esforço, pelos desenvolvedores (a API da plataforma medeia a interação entre os desenvolvedores e a equipe da plataforma). Desta forma, apoiados pela plataforma, os desenvolvedores operam seus serviços.
</i><p/></center>

No entanto, há momentos em que essas equipes devem interagir diretamente. Vamos agora apresentar esses padrões de interação.

**Resolvendo incidentes conjuntamente**: quando surgem problemas difíceis de resolver, as equipes trabalham juntas para contornar a situação. No entanto, o problema é tratado inicialmente apenas pela equipe de desenvolvimento, que convoca as pessoas do time de plataforma apenas se necessário.

**Consultoria para desenvolvedores**: a plataforma é um produto interno e, como um produto, vem com suporte. Portanto, a equipe de infraestrutura passa algum tempo ensinando as melhores práticas da plataforma aos desenvolvedores. Documentação e e-mails são abordagens para aliviar tutorias individuais.

**Demandando novos recursos**: os desenvolvedores em algum momento demandarão novos recursos da plataforma (por exemplo, monitoramento pronto para uso), assim como geralmente os clientes exigem a evolução de qualquer produto de software. A equipe da plataforma também pode ser proativa e sondar as maiores necessidades dos desenvolvedores por meio de pesquisas. De uma forma ou de outra, a equipe de infraestrutura faz a curadoria do backlog da plataforma.

**Conflitos**: claro, sempre existirão conflitos entre grupos que interagem. Mas esse conflito é diferente dos conflitos tradicionais em uma estrutura em silos. Sem a mediação da API, os conflitos geralmente surgem após uma tragédia, com foco na busca por culpados. Em um cenário mais colaborativo, com espírito DevOps, também surgem conflitos em relação à divisão do trabalho: não fica claro quem faz o quê. Por fim, em uma estrutura mediada por API, esperamos que os conflitos precedam os problemas em produção, pois serão mais em torno de questões de priorização para o aprimoramento da plataforma. São, portanto, conflitos de cliente versus fornecedor, que acontecem para qualquer produto de software.








