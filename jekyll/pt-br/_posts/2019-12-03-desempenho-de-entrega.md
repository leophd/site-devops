---
layout: post
title: "Desempenho de Entrega"
author: "Leonardo Leite"
lang: pt-br
lang-ref: delivery-performance
---

O desempenho de entrega é definido por Forsgren et al. [1] como uma combinação de três métricas:

* **frequência de implantação**,
* **tempo do commit até a produção**, e
* **tempo médio de recuperação**.

Essa definição procura evitar os problemas das métricas anteriormente usadas para medir desempenho de equipes de software, como linhas de código ou velocidade. Essas métricas anteriores geralmente sofrem de duas desvantagens: foco na *produção* (*outputs*) em vez de foco nos *resultados* (*outcomes*) e foco nos resultados *locais* e não nos *globais*. Por outro lado, Forsgren et al. definem desempenho de entrega de forma a focar nos resultados globais, garantindo que as equipes não sejam jogadas uma contra as outras.

([Para uma melhor distinção entre *outputs* e *outcomes* ver esse artigo aqui](https://www.jornaldenegocios.pt/opiniao/detalhe/quotoutput_vs_outcomequot){:target="_blank"}.)

Com base em uma pesquisa com 27.000 respostas em
2017, Forsgren et al. aplicaram uma análise de cluster nessas métricas
e descobriram três grupos: alto, médio e baixo desempenho.

**Alto desempenho**:

* têm várias implantações por dia;
* commits levam menos de 1 hora para atingir a produção;
* reparam incidentes em menos de 1 hora.

**Médio desempenho**:

* implantam entre uma vez por semana e uma vez por mês;
* têm tempo de commit a produção entre uma semana e um mês;
* levam menos de um dia para reparar incidentes.

**Baixo desempenho**:

* implantam entre uma vez por semana e uma vez por mês;
* têm tempo de commit a produção entre uma semana e um mês;
* levam entre um dia e uma semana para reparar os incidentes.

A pesquisa de Forsgren et al. também descobriu que o nível de desempenho de entrega se correlaciona com o alcance das metas de alto nível das organizações, tanto para fins comerciais quanto para não comerciais.

Em nossa pesquisa, no `ccsl / devops`, não estamos interessados ​​na distinção médio vs baixo desempenho.
Estamos interessados ​​apenas em distinguir as organizações de alto desempenho das que não tem alto desempenho.
Mas os clusters acima são problemáticos para o nosso propósito, pois há uma lacuna
entre os clusters de alto e médio desempenho.

Contornamos esse problema considerando uma organização como de alto desempenho
se *i)* estiver dentro dos limites do cluster de
alto desempenho acima definido ou *ii)* viola
no máximo um limiar de alto desempenho em apenas um ponto no
escala adotada para a métrica.

Aqui segue a escala para cada métrica.

Frequência de implantação:

* sob demanda (várias implantações por dia)
* entre uma vez por hora e uma vez por dia
* entre uma vez por dia e uma vez por semana
* entre uma vez por semana e uma vez por mês
* entre uma vez por mês e uma vez a cada seis meses
* menos de uma vez a cada seis meses

Tempo do commit até a produção:

* menos de uma hora
* menos de um dia
* entre um dia e uma semana
* entre uma semana e um mês
* entre um mês e seis meses
* mais de seis meses

Tempo médio para recuperação:

* menos de uma hora
* menos de um dia
* entre um dia e uma semana
* entre uma semana e um mês
* entre um mês e seis meses
* mais de seis meses

-----------------

[1] Nicole Forsgren, Jez Humble, and Gene Kim. 2018. **Measuring Performance.** In **Accelerate: The Science of Lean Software and DevOps: Building and Scaling High Performing Technology Organizations.** IT Revolution Press.

