---
layout: post
title: "DevOps e Marx: cultura não vai transformar sua organização"
author: "Leonardo Leite"
lang: pt-br
lang-ref: edvops-culture-and-marx
published: true
---

Davis e Daniels (2016) definem "DevOps efetivo" como:

*"Uma organização que abraça a mudança cultural para alterar a forma como os indivíduos pensam sobre o trabalho, que valoriza todos os diferentes papéis que os indivíduos têm, que acelera o valor do negócio e que mede os efeitos da mudança."*

Eles também dizem que:

*"Devops é uma prescrição para a cultura. (...) Devops é sobre encontrar maneiras de adaptar e inovar a estrutura social, a cultura e a tecnologia em conjunto para trabalhar de forma mais eficaz."*

<!--
No cultural movement exists in a vacuum; social structure and culture are inherently intertwined. The hierarchies within organizations, industry connections, and globalization influence culture, as well as the values, norms, beliefs, and artifacts that reflect these areas. 
-->

Embora os autores elaborem mais em outros trechos, pode-se resumir os pontos anteriores como **mudar a cultura para acelerar o negócio**. E, de fato, fala-se muito sobre cultura nos mundos DevOps e Agile. Por exemplo, já ouvi de um consultor: *"Os devs que tão lá vão ter essa mentalidade ágil? Se não, não adianta nada"*. Da mesma forma, Peter Senge et al. (2011), num livro sobre mudança organizacional, têm uma posição semelhante: *"Não é suficiente mudar estratégias, estruturas e sistemas, a menos que o pensamento que produziu essas estratégias, estruturas e sistemas também mude."* Além disso, o "terceiro caminho do DevOps" [the third DevOps way] está relacionado à melhoria do trabalho diário por meio de um tipo específico de cultura (Kim, 2021). Porém, vamos considerar outra abordagem possível neste post. Mas antes... o que é cultura?

O livro Effective DevOps define cultura como *"as ideias, costumes e comportamento social de um povo ou sociedade"*. Ainda no contexto DevOps, o livro Accelerate (Forsgren, 2018) adota um modelo de cultura focado em como a informação flui pela organização; nesse contexto, uma cultura pode ser patológica (orientada pelo poder, envolvendo medo e ameaça), burocrática (orientada por regras) ou generativa (orientada para o desempenho).

Então, devemos nos concentrar em mudar essas coisas (por exemplo, comportamento social) para construir melhores sistemas de software?

Bem, vamos agora considerar os conceitos marxianos de infraestrutura e superestrutura (Marx, 1859):

*O modo de produção da vida material [a infraestrutura] condiciona o processo geral da vida social, política e intelectual [a superestrutura]. (...) As mudanças na infraestrutura econômica levam mais cedo ou mais tarde à transformação de toda a imensa superestrutura.*

Isso significa que o modo de produção de uma sociedade determina sua cultura, e não o contrário.

![Marx com óculos DevOps, só por diversão.]({{ site.baseurl }}/assets/figures/devops-marx.png)

Claro, essa visão é discutível (os autores de Effective DevOps também sabem que o contrário não seria tão simples). Mas vamos assumir essa hipótese marxiana por um momento. Talvez faça sentido em alguns casos. Considere uma empresa que muda o processo de avaliação de centrado no indivíduo para centrado na equipe. O processo de avaliação faz parte do sistema de produção, e com certeza terá um grande impacto na forma como as pessoas se comportam e interagem. A estrutura organizacional também pode influenciar o comportamento. Considere a existência de uma equipe de QA (garantia de qualidade). A presença dessa estrutura pode incentivar os desenvolvedores a não serem tão cuidadosos em seus testes, possivelmente gerando mais aplicações com bugs.

No contexto DevOps, responsabilizar os desenvolvedores pelo gerenciamento de infraestrutura pode promover uma cultura de aprendizado (não há pessoas para quem empurrar os problemas). Por outro lado, tentar estabelecer uma cultura de colaboração entre os desenvolvedores e o pessoal de infraestrutura (anteriormente trabalhando em silos) sem alterar as estruturas pode causar conflitos por causa de responsabilidades não bem definidas.

A ideia de que as mudanças organizacionais impactam a cultura é colocada por Forsgren et al. (2018) como *"também descobrimos que é possível influenciar e melhorar a cultura implementando práticas de DevOps."* Em sua pesquisa, eles descobriram que práticas enxutas [lean] de desenvolvimento de produtos (por exemplo, trabalhar em pequenos lotes) e de gerenciamento enxuto (por exemplo, limitar o trabalho em andamento [wip]) promovem uma cultura generativa. No mundo ágil, é comum dizer que o ágil tem a ver com cultura, não com práticas ou ferramentas. No entanto, Martin (2014) afirma que *"Você não pode ter uma cultura sem práticas; e as práticas que você segue identificam sua cultura."*

Outra visão que corrobora "cultura como produto" é a de Pflaeging (2020), em seu livro sobre complexidade nas organizações:

*A cultura não é um fator de sucesso, mas um efeito do sucesso ou do fracasso. (...) Por isso também não pode ser influenciada diretamente. (...) A cultura é observável, mas não controlável. (...) Uma empresa não pode escolher sua cultura, ela tem exatamente a cultura que merece. A cultura é algo como a memória viva de uma organização. (...) Ela dota algo como um "estilo" comum no qual todos podem confiar. (...) A cultura, nesse sentido, é autônoma. A cultura não é uma barreira à mudança, nem encoraja a mudança. Ele pode fornecer dicas sobre o que uma organização deve aprender. Mudar a cultura é impossível, mas a observação da cultura é valiosa. (...) A cultura nos permite observar indiretamente a qualidade dos esforços de mudança: a mudança permeia a cultura.*

Em seu livro, Pflaeging defende uma mudança drástica na abordagem gerencial (para uma abordagem não gerencial!), afirmando que isso é necessário para as empresas sobreviverem e vencerem a concorrência. É até divertido pensar que podemos interpretar isso no sentido marxiano de que o que leva à mudança (revolução!) são as contradições do atual sistema de produção: é preciso mudar ou ser derrotado por outras formas superiores de produção.

Pois bem, este post não pretende chegar a nenhuma conclusão definitiva. Pretende simplesmente alertar que antes de tentar mudar o comportamento das pessoas (sua cultura), é fundamental investigar os motivos que levaram essas pessoas a se comportarem de determinada forma.

Grato pela atenção.

PS: Se quiser debater, por favor mande um tweet para [@leonardofl](https://twitter.com/leonardofl){:target="_blank"}.


-------------------------------

Jennifer Davis, Ryn Daniels. **Effective DevOps**: Building a Culture of Collaboration, Affinity, and Tooling at Scale. O'Reilly Media. 2016.

Gene Kim, Jez Humble, Patrick Debois, John Willis, and Nicole Forsgren. **The DevOps Handbook**: How to Create World-Class Agility, Reliability, & Security in Technology Organizations. 2nd ed. IT Revolution Press. 2021.

Peter M. Senge, Art Kleiner, Bryan Smith, Charlotte Roberts, Geroge Roth, Richard Ross. **The Dance of Change**: The Challenges of Sustaining Momentum in Learning Organizations. Nicholas Brealey Publishing. 2011.

Nicole Forsgren, Jez Humble, and Gene Kim. **Accelerate**: The Science of Lean Software and DevOps: Building and Scaling High Performing Technology Organizations. IT Revolution Press. 2018.

Karl Marx. Preface to **A Contribution to the Critique of Political Economy**. 1859. Available at [https://www.marxists.org/archive/marx/works/1859/critique-pol-economy/preface.htm](https://www.marxists.org/archive/marx/works/1859/critique-pol-economy/preface.htm){:target="_blank"}.

Robert C. Martin. **The True Corruption of Agile**. 2014. Available at [https://blog.cleancoder.com/uncle-bob/2014/03/28/The-Corruption-of-Agile.html](https://blog.cleancoder.com/uncle-bob/2014/03/28/The-Corruption-of-Agile.html){:target="_blank"}.

Niels Pflaeging. **Organizing for complexity**: How to get life back into work to build the high-performance organization. 5th revised edition. Betacodex Publishing. 2020

