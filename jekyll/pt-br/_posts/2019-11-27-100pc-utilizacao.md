---
layout: post
title: "100% de utilização aumenta o lead time"
author: "Leonardo Leite"
lang: pt-br
lang-ref: 100pc-utilization
---

Trecho do livro **Accelerate** [1]:

<blockquote>
A teoria de filas da matemática nos diz que conforme a utilização se aproxima de 100%,
o lead time se aproxima do infinito - em outras palavras, uma vez que você alcança
níveis muito altos de utilização, o time leva exponencialmente mais tempo para conseguir
concluir qualquer coisa.
</blockquote>

Obs: lead time (algo como tempo de entrega) é uma medida de quão rápido um trabalho pode ser concluído.

Uma pista sobre o porquê disso acontecer: quando "recursos" (trabalhadores) estão 100% ocupados e comprometidos com prazos, é impossível tratar incidentes (inesperados) e manter os prazos. E incidentes (inesperados) acontecem.

-----------------

[1] Nicole Forsgren, Jez Humble, and Gene Kim. 2018. **Measuring Performance.** In **Accelerate: The Science of Lean Software and DevOps: Building and Scaling High Performing Technology Organizations.** IT Revolution Press.
