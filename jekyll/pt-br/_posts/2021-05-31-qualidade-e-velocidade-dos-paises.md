---
layout: post
title: "Qualidade e velocidade dos países na produção de software"
author: "Leonardo Leite"
lang: pt-br
lang-ref: quality-and-speed-of-countries
hiden: false
published: true
---

Em pelo menos duas das entrevistas que realizei para minha pesquisa, os entrevistados expressaram a ideia de que as empresas de software na Alemanha se preocupam mais com qualidade do que as empresas nos Estados Unidos, enquanto que as americanas se preocupam mais com isso do que as brasileiras. Por outro lado, esses entrevistados também afirmaram que as empresas brasileiras estão mais preocupadas em entregar rápido do que as empresas dos EUA, enquanto as empresas dos EUA se preocupariam mais com essa velocidade do que as alemãs.

![Em qualidade: Alemanha > EUA > Brasil. Em velocidade: Brasil > EUA > Alemanha]({{ site.baseurl }}/assets/figures/paises.png)

<p><center><i>
Figura 1 - Diferentes países preocupando-se de forma diferente com qualidade e velocidade na produção de software
</i></center></p>

Outro entrevistado me contou que as empresas no Japão tiveram dificuldades para adotar a mentalidade Ágil. Em particular, era difícil aceitar o lançamento de um MVP, ou seja, um produto não completo, o que implica mais apreço pela qualidade do que pela velocidade de lançamento. Mas quando tivemos essa conversa, no início de 2019, esse problema já estava superado.

Claro que esses relatos referem-se à experiência desses entrevistados. Mas será que isso poderia ser verdade? Ou seria pelo menos verdade que a cultura do país poderia afetar a maneira como diferentes países desenvolvem software? Se você tem alguma experiência internacional em produção de software, compartilhe no Twitter (@leonardofl) o que você acha, por favor!

Além disso, independentemente da sua experiência, compartilhe com a gente se você acredita que qualidade e velocidade são de fato um trade-off a ser equilibrado!



