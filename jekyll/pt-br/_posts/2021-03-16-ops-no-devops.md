---
layout: post
title: "Ops no DevOps: operadores?"
author: "Leonardo Leite"
lang: pt-br
lang-ref: operators
hiden: false
published: true
---

DevOps é uma amálgama de desenvolvimento e operações. Mas também pode ser interpretado como uma amálgama de desenvolvedores e operadores. No entanto, neste texto, gostaria de explicar por que a palavra "operador" é problemática.

![Operadoras mulheres em uma fábrica operando máquinas grandes, um homem supervisionando, foto preto e branco, pessoas vestindo trajes da década de 40 ou 50.]({{ site.baseurl }}/assets/figures/operators.jpg)

<br/>


(Mas antes um aviso ⚠️: talvez eu esteja atacando um [espantalho](https://pt.wikipedia.org/wiki/Fal%C3%A1cia_do_espantalho){:target="_blank"}, pois acho que as pessoas geralmente não têm o cargo de "operador" no contexto de TI. No entanto, alguns textos trazem essa palavra.)

Quando escrevo sobre DevOps, prefiro usar o termo "profissionais de infraestrutura" em vez de simplesmente operadores. Mas, como acontece com muitas funções e títulos em nossa indústria, não é simples. Woods (2016), por exemplo, propõe a seguinte terminologia:

* Pessoal de operações: recebem e operam software novo e alterado em produção e são responsáveis ​​por seu nível de serviço.
* Engenheiros de infraestrutura: fornecem os serviços de infraestrutura dos quais o sistema depende.

Em outras indústrias, é comum aplicar o termo operador a trabalhadores braçais que controlam máquinas. Portanto, teríamos engenheiros projetando as máquinas e operadores operando-as. Se a "máquina" construída por engenheiros de software é o sistema de software, os operadores podem ser os usuários do sistema. Pode soar estranho aplicar esse termo no contexto geral, no qual os usuários finais são os clientes (ex: webmail). Ainda assim, parece bem adequado para situações em que os usuários são funcionários do cliente (que demandou o sistema), como seria o caso de caixas de supermercado.

No entanto, no sentido de "operador de máquina", podemos considerar a função do operador de TI proposta por Woods: trabalhadores que executam scripts para criar ambientes e disparam comandos de reinicialização quando o sistema é desativado. Mas, é claro, o trabalho em infraestrutura está cada vez mais próximo da definição de "engenheiro de infraestrutura" fornecida por Woods. Fornecer a infraestrutura requer planejamento, dimensionamento e automação. Além disso, quando as coisas não estão funcionando, habilidades investigativas são exigidas dos profissionais de infraestrutura.

Portanto, de uma forma geral, entendo que a palavra “operador” diminui o papel dos profissionais de infraestrutura. Claro, algumas empresas ainda podem contratar uma força de trabalho barata para executar continuamente comandos simples, mas essa é a mentalidade de trabalho que tem sido superada pelas propostas do movimento DevOps.

No entanto, não estou tão certo sobre o que quero dizer. Se você discorda ou tem outras perspectivas sobre o assunto, por favor, comente no Twitter para [@leonardofl](https://twitter.com/leonardofl){:target="_blank"}. Tenho algumas ideias adicionais sobre a "função do operador" [neste outro post]({{ site.baseurl }}{% post_url en/2021-01-09-future-work %}.html){:target="_blank"} também.

-----------------

## References

Eoin Woods. Operational The Forgotten Architectural View. IEEE Software. May/June 2016. 

## Foto

Foto do <a href="https://unsplash.com/@birminghammuseumstrust?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText" target="_blank">Birmingham Museums Trust</a> no <a href="https://unsplash.com/photos/9GSGllMJCeA" target="_blank">Unsplash</a>.

