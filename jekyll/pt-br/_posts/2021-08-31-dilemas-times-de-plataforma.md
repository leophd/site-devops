---
layout: post
title: "Dilemas sobre times de plataforma"
author: "Leonardo Leite"
lang: pt-br
lang-ref: platform-teams-dilemma
hiden: false
published: true
---

Os times de plataforma são equipes de infraestrutura que fornecem serviços de infraestrutura altamente automatizados que podem ser consumidos por desenvolvedores para a implantação de serviços. A API da plataforma é o meio de interação entre as equipes de desenvolvimento e infraestrutura, permitindo que os desenvolvedores operem de forma autônoma (implantando, monitorarando, etc.) seus serviços. Nossa pesquisa [1, 2] tem mostrado que organizações com times de plataforma têm melhor [desempenho de entrega]({{site.baseurl}}{% post_url en/2019-12-03-desempenho-de-entrega%}.html){:target="_ blank"}.

![A figura mostra um profissional de infra e um dev, cada um dentro de um círculo diferente. O de infra fornece uma interface que é consumida pelo dev (notação UML).]({{ site.baseurl }}/assets/figures/platform-team.png)

<center><i>
Figura 1 - O time de plataforma fornece serviços automatizados para serem utilizados, com pouco esforço, pelos desenvolvedores.
</i><p/></center>

Embora as plataformas de infraestrutura promovam a produtividade, geralmente são customizadas para o caso de uso típico da organização. Os desenvolvedores de projetos mais inovadores podem se sentir restringidos pela plataforma. Uma solução existente é deixar que os desenvolvedores ignorem a abstração da plataforma e trabalhem diretamente com a infraestrutura subjacente quando necessário. Essa opção promove a flexibilidade, mas pode custar as "proteções" (*guardrails*) que evitam que os desenvolvedores desrespeitem inconscientemente as boas práticas ou os padrões da empresa.

Outra solução para a incompatibilidade entre plataforma e aplicação são desenvolvedores demandando evoluções na plataforma. Esse caminho beneficia não apenas os demandantes, mas todos os clientes da plataforma. No entanto, pode levar algum tempo para o time da plataforma liberar novas versões.

Outro dilema relacionado é: quão abstrata deve ser a abstração da infraestrutura fornecida pela plataforma? Quanto mais abstrato, mais fácil e produtivo é o uso da plataforma. Mas, novamente: ao custo de flexibilidade para a arquitetura da aplicação.

Além da flexibilidade, uma API de infraestrutura muito abstrata tem outra desvantagem: os desenvolvedores podem não aprender noções básicas de infraestrutura e usar a plataforma como uma "coisa mágica". Nesse cenário, os desenvolvedores podem negligenciar a qualidade porque confiam demais na plataforma: qualquer problema e eles culpam a plataforma, não sabendo o que fazer, mesmo em questões simples ou problemas na própria aplicação. Para lidar com essa questão, a organização deve incentivar os desenvolvedores a aprender como usar a plataforma corretamente e a melhorar suas habilidades básicas de infraestrutura.

De qualquer forma, a premissa da adoção de uma plataforma é que o conhecimento dos desenvolvedores sobre infraestrutura pode ser superficial. Esse fato leva a outra desvantagem em potencial: os desenvolvedores se concentram em necessidades corporativas específicas - devem aprender sobre uma plataforma que não existe fora da organização. Portanto, os desenvolvedores não adquirem um conhecimento mais geral e avançado de infraestrutura que poderia fomentar suas carreiras. Este não é um problema para a organização, mas sim para as carreiras dos trabalhadores no longo prazo. Por outro lado, algumas pessoas não veem problemas aqui: alguns desenvolvedores preferem se concentrar na codificação e aprender o mínimo necessário sobre infraestrutura. Além disso, deve-se considerar a evolução das abstrações que suportam o desenvolvimento de software. Assim como hoje, tipicamente, os programadores não precisam se preocupar com transistores ou instruções de máquina, o futuro deve encapsular em camadas algumas das preocupações que hoje estão expostas a esses programadores.

Em resumo, decidir o nível de abstração da plataforma de infraestrutura e a possibilidade de contornar essa abstração trazem dilemas. Uma alta abstração pode trazer alta produtividade, baixa flexibilidade e limitações para a carreira dos trabalhadores. Essas preocupações não são necessariamente motivos para evitar a adoção de times de plataforma. No entanto, esperamos que as pessoas cientes de tais questões discutam de forma mais assertiva sobre como lidar com elas.

<!--
-- Tweet:
-- O dilema de times de plataforma - o quanto a plataforma deve abstrair a infraestrutura? Uma alta abstração pode trazer alta produtividade, baixa flexibilidade e limitações para a carreira dos trabalhadores.
-->
-----------------------------------------------------

[1] Leonardo Leite, Fabio Kon, Gustavo Pinto, Paulo Meirelles. [Platform Teams: An Organizational Structure for Continuous Delivery]({{site.baseurl}}{% post_url en/2020-03-17-times-de-plataforma %}.html){:target="_ blank"}. In: 6th International Workshop on Rapid Continuous Software Engineering (RCoSE), 2020.

[2] Leonardo Leite, Gustavo Pinto, Fabio Kon, Paulo Meirelles. [The Organization of Software Teams in the Quest for Continuous Delivery: A Grounded Theory Approach]({{site.baseurl}}{% post_url en/2021-06-19-estruturas-organizacionais-publicado %}.html){:target="_ blank"}. Information and Software Technology, Vol. 139, 2021.

