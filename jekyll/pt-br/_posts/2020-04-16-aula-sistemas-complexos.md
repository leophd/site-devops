---
layout: post
title: "DevOps no Laboratório de Sistemas Computacionais Complexos"
author: "Leonardo Leite"
lang: pt-br
lang-ref: complex-systems-2020
hiden: false
published: true
---

Ministrei uma aula sobre DevOps em 16 de outubro de 2020 para o Laboratório de Sistemas Computacionais Complexos da Universidade de São Paulo (USP). Este post é só para compartilhar os slides usados na aula.

A aula é sobre DevOps, cobrindo principalmente:

* cultura,
* entrega contínua,
* integração contínua,
* práticas em tempo de execução e
* microsserviços.


<br/>
<center>
<a href="https://pt.slideshare.net/leonardoferreiraleite/sistemas-complexosdevops20200416" class="button" target="_blank">Slides aqui</a>
</center>
<br/>

