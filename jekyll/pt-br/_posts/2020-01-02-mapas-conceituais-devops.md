---
layout: post
title: "Mapas Conceituais DevOps"
author: "Leonardo Leite"
lang: pt-br
lang-ref: devops-conceptual-maps
---

Em nosso [Survey of DevOps Concepts and Challenges]({{ site.baseurl }}{% post_url en/2019-12-04-devops-survey-published %}.html){:target="_blank"}, nós analisamos minuciosamente 50 artigos acadêmicos relevantes sobre DevOps. A partir dessa análise, fundamentada na literatura, estruturamos um conjunto de conceitos relevantes sobre DevOps no formato de cinco **mapas conceituais**.

Em nosso artigo, usamos esses mapas para:

* Estudar e classificar ferramentas DevOps;
* Identificar implicações práticas de DevOps e segmentar essas implicações para engenheiros, gerentes e pesquisadores; e
* Discutir os principais desafios em aberto sobre DevOps.

Esses mapas conceituais podem ser usados por profissionais da indústria (gerentes e engenheiros) para:

* Orientação sobre educação continuada em DevOps (o que estudar em seguida?);
* Obtenção de uma visão mais conceitual das ferramentas DevOps, dando suporte ao processo de escolha de novas ferramentas na organização;
* Suporte à reflexão e melhorias contínuas (a retrospectiva da jornada do DevOps).

## Os mapas conceituais

![Categorias dos conceitos DevOps (Fazendo o download dos PDF é possível ler os conceitos no mapa)]({{ site.baseurl }}/assets/figures/conceitos-categorias.png)

<center><i>
Figura 1 - Mapa global das categorias de conceitos DevOps
</i><p/></center>

<br/><br/>

![Mapa conceitual - Processo (Fazendo o download dos PDF é possível ler os conceitos no mapa)]({{ site.baseurl }}/assets/figures/conceitos-processos.png)

<center><i>
Figura 2 - Mapa conceitual - Processo
</i><p/></center>

<br/><br/>

![Mapa conceitual - Pessoas (Fazendo o download dos PDF é possível ler os conceitos no mapa)]({{ site.baseurl }}/assets/figures/conceitos-pessoas.png)

<center><i>
Figura 3 - Mapa conceitual - Pessoas
</i><p/></center>

<br/><br/>

![Mapa conceitual - Pessoas (Fazendo o download dos PDF é possível ler os conceitos no mapa)]({{ site.baseurl }}/assets/figures/conceitos-entrega.png)

<center><i>
Figura 4 - Mapa conceitual - Entrega
</i><p/></center>

<br/><br/>

![Mapa conceitual - Tempo de execução (Fazendo o download dos PDF é possível ler os conceitos no mapa)]({{ site.baseurl }}/assets/figures/conceitos-tempo-de-execucao.png)

<center><i>
Figura 5 - Mapa conceitual - Tempo de execução
</i><p/></center>

## Retrospectiva da jornada DevOps - dinâmica

Cada turno é definido pela combinação de um membro da equipe e um dos quatro mapas conceituais grandes (processo, pessoas, entrega e tempo de execução). As pessoas e os mapas são alternados sequencialmente.

Em sua vez, o membro da equipe escolhe um conceito do mapa da vez. Todos os membros da equipe baixam um dos seguintes cartões:

* Conseguimos e estamos felizes por isso.
* Conseguimos, mas parece que não importa muito.
* Não conseguimos, mas queremos!
* Não conseguimos, mas nem vemos valor nisso.

Então as pessoas discutem sobre a avaliação feita.

As rodadas continuam até o final do time-box predefinido (por exemplo, uma hora).

<br/><br/>

![Os quatro cartões para a retrospectica DevOps]({{ site.baseurl }}/assets/figures/cartoes-retrospectiva-devops.png)

<center><i>
Figure 6 - Cartões para a retrospectica DevOps
</i><p/></center>

Exemplo:

* A equipe tem Alice, Bernardo, Carlos e Dorival.
* Na primeira rodada, Alice precisa escolher um conceito no mapa Processos.
* Ela seleciona "Ciclos curtos de feedback".
* Todo mundo baixa suas cartas.
* Todos concordam que a equipe não consegue trabalhar em ciclos curtos de feedback ...
* Mas Alice, Bernardo e Carlos acreditam que é um objetivo necessário.
* Por outro lado, Dorival acredita que não é viável incomodar o cliente com muitos pedidos de feedback.
* Portanto, a equipe discute sobre a questão para descobrir o que faz sentido em seu contexto.
* Próximo turno: Bernardo escolhe um conceito no mapa Pessoas.
* E assim por diante...

Para essa dinâmica, você pode usar os mapas na versão PDF; é bem melhor que dá pra dar zoom no mapa!

<br/>
<center>
<a href="{{ site.baseurl }}/assets/files/mapas-conceituais-devops.zip" class="button">Download dos PDFs dos mapas conceituais em português</a>
</center>
<br/>




