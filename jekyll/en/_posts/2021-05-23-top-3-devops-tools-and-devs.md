---
layout: post
title: "The top 3 DevOps tools and developers"
author: "Leonardo Leite"
lang: en
lang-ref: top-3-devops-tools-and-devs
hiden: false
published: true
---

In the last post, I recommended people "tracking the infrastructure path" to focus on the following DevOps tools: Kubernetes, Kafka, and Terraform. And about developers? Should they pay attention to these tools too?


![Logos of Kubernetes, Kafka, and Terraform]({{ site.baseurl }}/assets/figures/top-3-tools.png)

<p><center><i>
Figure 1 - The top 3 DevOps tools
</i></center></p>

Off-course, it wildly depends on the type of organization in which you work and the kind of developer you are or want to be. For this post, I'll assume that your company has infrastructure people to install, configure, and manage infrastructure software such as Kubernetes and Kafka. On the other hand, I'll consider you do more than "only code" and that, at least, you are concerned with software architecture.

## Kubernetes

Kubernetes is changing the DevOps landscape. It seems most of the infrastructure teams operating as [platform teams]({{ site.baseurl }}{% post_url en/2020-03-17-platform-teams %}.html){:target="_blank"} are using Kubernetes. And it seems more changes are coming from Kubernetes with the promise of applying its control plane to other infrastructure resources beyond containers (see the [Crossplane project](https://containerjournal.com/kubeconcnc/kubernetes-true-superpower-is-its-control-plane/){:target="_blank"}).

Therefore, I believe developers should understand what Kubernetes is, what it does, and why it is so special. Probably there is no need to learn how to install and configure it. Learning its commands to create pods may be helpful but maybe not crucial since such access has been abstracted by other layers, such as corporate platforms or even Terraform.

## Terraform

Terraform is a declarative way of specifying the infrastructure to be provisioned. Terraform has several providers to create different types of infrastructure in various cloud vendors. It is even possible for a company to create custom providers to create on-premises infrastructure. In both cases, an interesting use case is when a specialized infrastructure group defines allowed Terraform templates to be used within the organization. It's a way to discipline the use of a public cloud, minimizing architectural variation across teams. In this context, maybe developers don't need to be able to create Terraform templates from scratch. Still, they will benefit from 1) understanding the high-level Terraform architecture and 2) being able to read a Terraform template and, thus, understanding what is going on (i.e., where the application will run).

## Kafka

Maybe knowing how to install and manage Kafka is not necessary for a developer. However, I would say it is fundamental to developers to understand when a publisher/subscriber or an event-driven architecture is desirable. Such approaches can bring important decoupling among domains of a system (i.e., dependency inversion). And, at first, Kafka is just an infrastructure piece to concretize this vision -- it is the publisher/subscriber broker. However, as long as I understood, Kafka brings some significant scalability advantages compared to other messaging systems.

After deciding to use Kafka (the hard part), developers must learn how to interact with Kafka in some programming language to post and read messages. What may be not easy is that some tuning may depend on business requirements, which means such tuning can hardly be left to infrastructure people alone. This means that even developers don't know how to tune Kafka, it may be relevant to them to learn the tuning options and when to apply them. One example of a tuning class is about delivery guarantees.

If you believe there is some mistake in this post, please let me know ([@leonardofl](https://twitter.com/leonardofl){:target="_blank"} at Twitter)!

