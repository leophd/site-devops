---
layout: post
title: "Hypotheses about Organizational Structures for Continuous Delivery"
author: "Leonardo Leite"
lang: en
lang-ref: hypotheses
hiden: true
published: false
---

We have developed some hypotheses related to [the organizational structures]({{ site.baseurl }}{% post_url en/2019-11-23-structures %}.html){:target="_blank"} we found in our interviews with IT professionals. These hypotheses support the understanding of the structures and are part of our in-development theory.

## Hypothesis 1 

*Collaboration and delivery automation,
critical values of the DevOps movement, are not enough for reaching
high [delivery performance]({{ site.baseurl }}{% post_url en/2019-11-27-delivery-performance %}.html){:target="_blank"}.* 

We saw organizations embracing them,
but still limiting delivery due to bureaucratic processes or release
windows. One of the possible reasons for that is the lack of proper
test automation, a hard goal to achieve since nearly all developers
must master it. Release windows may also be a result of the monolith
architectural style, which is discussed in our Hypotheses
6 and 7.

## Hypothesis 2 

*Composing cross-functional teams with
infrastructure specialists is not common.* 

One possible reason is that
platform capabilities reduce the scope of cross-functional teams,
avoiding the need for each one of them to have infrastructure
specialists. In other contexts, companies are being successful in 
improving collaboration among developers and infrastructure experts
located outside the product team. 

## Hypothesis 3 

*Having a platform team is a promising
way to achieve high delivery performance.* 

This structure decouples
the infrastructure and the product teams, preventing the infrastructure 
team from becoming a bottleneck in the delivery path. 
It also contributes to increasing services reliability 
by placing the product team in the front-line of handling
non-functional requirements and incidents.


## Hypothesis 4 

*The decoupling between the platform
and product teams does not imply the absence of collaboration among
these groups.* 

Quite the opposite. We observed organizations in
which the platform team helped developers in mastering non-
functional concerns, as well as developers actively demanding 
improvements in the platform.

## Hypothesis 5 

*Balancing delivery performance and
quality becomes a trade-off when the application lacks automated
tests.* 

In one organization, we saw the scenario of decelerating 
delivery frequency to favor product quality, whereas we saw other
organization claiming the lack of tests was due to rapid change and
market pressure. Interestingly, these observations seem to 
contradict previous findings that there is no trade-off among
quality and delivery performance [1]. Thus, this Hypothesis 
conciliates this apparent contradiction. Nonetheless, we also
observed that, in some sectors, it is just harder to fully automate
testing, such as in games and IoT.

## Hypothesis 6 

*The usage of a monolith architecture
hinders high delivery performance.* 

The main reason for this is that
the monolith can induce organizations to adopt release windows,
preventing the deployment at any time. One interviewee claimed
that the release windows exist because the times for running the
building and automated tests are too long. 

## Hypothesis 7 

*A monolith architecture does not benefit
so much from the adoption of an infrastructure platform.*

It occurs
because the main benefit that the platform provides is enabling
product teams to create new environments; this ability is essential
for handling microservices, but not for monoliths. Even without a
highly-automated infrastructure platform, it is possible to set-up an
automated delivery pipeline that enables product teams to deliver
new versions of a monolith without hand-offing to operations.

## Hypothesis 8 

*Enabler teams, providing consulting
and tools for product teams, correspond to another dimension of the
organizational structures.* 

This is due to the fact that enabler teams
can exist in all structures we found. One structure the reader could 
expect us to find is [SRE]({{ site.baseurl }}{% post_url en/2019-11-27-sre %}.html){:target="_blank"}. 
However, SRE did not emerge as a
new structure since we did not observe a case matching 
how SRE is defined by the book [2]. The most similar we saw were a “performance
team” and a “productivity team” providing consulting and tools to product teams.

-----------------

[1] Nicole Forsgren, Jez Humble, and Gene Kim. 2018. **Measuring Performance.** In **Accelerate: The Science of Lean Software and DevOps: Building and Scaling High Performing Technology Organizations.** IT Revolution Press.

[2] Betsy Beyer, Chris Jones, Jennifer Petoff, and Niall Richard Murphy. 2016. **Site Reliability Engineering: How Google Runs Production Systems.** O’Reilly Media.
