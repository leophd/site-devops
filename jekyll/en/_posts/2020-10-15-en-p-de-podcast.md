---
layout: post
title: "Talking about DevOps in p de Podcast"
author: "Leonardo Leite"
lang: en
lang-ref: p-de-podcast
hiden: false
published: true
---

On September 11th, I had the pleasure of talking about DevOps with Marcio Frayze and Julianno Martins on "p de Podcast", a podcast about software architecture.

![p de Podcast logo]({{ site.baseurl }}/assets/figures/p-de-podcast.png)

<br/>
<center>
<a href="https://open.spotify.com/episode/3xNWRFRxpLsFf06WYxr8Kz?si=XUTn9P0cQrCcvQ6JVPvwlw&utm_source=native-share-menu" class="button" target="_blank">Access via Spotify (Portuguese)</a>
</center>
<br/>



