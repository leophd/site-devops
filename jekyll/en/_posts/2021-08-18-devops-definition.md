---
layout: post
title: "What is DevOps? Our DevOps definition"
author: "Leonardo Leite"
lang: en
lang-ref: devops-definition
hiden: false
published: true
---

Even though the DevOps movement has been discussed for nearly a decade, DevOps still lacks a widely accepted definition. By consolidating the most cited definitions of DevOps in the academic literature, we crafted our definition:

-----------

**"DevOps is a collaborative and multidisciplinary effort within an organization to automate continuous delivery of new software versions, while guaranteeing their correctness and reliability."**

-----------

This definition was accepted by the ACM Computing Surveys journal, a very reputable academic journal in our field, within our [Survey of DevOps Concepts and Challenges]({{ site.baseurl }}{% post_url en/2019-12-04-devops-survey-published %}.html){:target="_blank"}.

There is a current trend in the industry of adopting DevOps as a role, the "DevOps engineer" [1]. But our DevOps definition is according to the original spirit of the DevOps movement, which was about breaking down the silos (*"DevOps is a collaborative and multidisciplinary effort within an organization"*). This spirit was portrayed, for example, by the novel *The Phoenix Project* [2], authored by a prominent figure of the DevOps movement. Our definition is also in accordance with a shared view in the literature that closely relates DevOps to continuous delivery (*"to automate continuous delivery of new software versions"*), evidenced by the title of the seminal paper on DevOps "Why enterprises must adopt DevOps to enable continuous delivery" [3]. Finally, our definition is also aligned with more recent developments that face the fact that it is not enough to deliver faster, but also that reliability is paramount (*"while guaranteeing their correctness and reliability"*), such as the discussion presented in the *Site Reliability Engineering* book [4].

------------------------------------------------

[1] Waqar Hussain, Tony Clear, and Stephen MacDonell. **Emerging trends for global DevOps: A New Zealand perspective**. In Proceedings of the 12th International Conference on Global Software Engineering, ICGSE 2017.

[2] Gene Kim, Kevin Behr, and George Spafford. **The Phoenix Project**: A Novel about IT, DevOps, and Helping Your Business Win. IT Revolution Press, 3rd edition, 2018.

[3] Jez Humble and Joanne Molesky. **Why enterprises must adopt DevOps to enable continuous delivery.** Cutter IT Journal, 2011.

[4] Betsy Beyer, Chris Jones, Jennifer Petoff, and Niall Richard Murphy. **Site Reliability Engineering**: How Google runs production systems. O'Reilly, 2016.
