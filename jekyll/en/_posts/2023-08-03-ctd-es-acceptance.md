---
layout: post
title: "Our thesis is a finalist in the Software Engineering Doctoral and Master Theses Competition of CBSoft 2023!"
author: "Leonardo Leite"
lang: en
lang-ref: ctd-es
hiden: false
---

We are excited to announce that our thesis ([A grounded theory of organizational structures for development and infrastructure professionals in software-producing organizations]({{ site.baseurl }}{% post_url en/2022-07-13-thesis-published %}.html){:target="_blank"}) was selected for the next, and final, phase in the Software Engineering Doctoral and Master Theses Competition (CTD-ES 2023) of CBSoft (Brazilian Conference on Software: Practice and Theory)!!!

![CBSoft 2023 logo]({{ site.baseurl }}/assets/figures/cbsoft2023-logo.png)

<p><center><i>
Figure 1 - CBSoft 2023 - Brazilian Conference on Software: Practice and Theory
</i></center></p>

The Software Engineering Doctoral and Master Theses Competition (CTD-ES) aims to disseminate and award the best Doctoral and Master theses in Software Engineering that were concluded, presented, and approved in Brazil in 2022. CTD-ES will award theses with a significant impact on society and organizations, as well as a significant, outstanding contribution to the scientific area of Software Engineering.

![Email screenshot]({{ site.baseurl }}/assets/figures/aceite-ctd-es.png)

<p><center><i>
Figure 2 - Selection communication sent by the CTD-ES chairs
</i></center></p>

The contest will be held within CBSoft 2023. The Brazilian Conference on Software: Practice and Theory (CBSoft) is an event annually promoted by the Brazilian Computing Society (SBC) aiming at fostering the exchange of experience among researchers and practitioners from industry and academia about the most recent research, tendencies, and theoretical and practical innovations on software. Held since 2010 as an aggregating conference of Brazilian symposia promoted by SBC on software, CBSoft has become one of the main conferences of the Brazilian scientific community on Computing.

The 14th edition of CBSoft will be held from September 25th to 29th at the Federal University of Mato Grosso do Sul, located in Campo Grande, MS, Brazil. It will integrate four traditional symposia annualy promoted by the Brazilian community on Software Engineering: the 37th Brazilian Symposium on Software Engineering (SBES), the premier conference on Software Engineering in Latin America; the 27th Brazilian Symposium on Programming Languages (SBLP); the 17th Brazilian Symposium on Software Components, Architectures, and Reuse (SBCARS); and the 8th Brazilian Symposium on Systematic Automated Software Testing (SAST).

![Photo of the event location, the Federal University of Mato Grosso do Sul]({{ site.baseurl }}/assets/figures/ufms.jpg)

<p><center><i>
Figure 3 - Federal University of Mato Grosso do Sul picture (Campo Grande, MS, Brazil)
</i></center></p>

The CBSoft programme will include technical sessions with presentation of scientific papers, keynotes by nationally and internationally renowned Brazilian and foreign researchers, discussion panels, workshops, and tool demonstrations. All these activites of interest of the Software Engineering community and fields related to the development of software systems go towards the diffusion of knowledge and discussion of important issues related to research, development, and innovation in both Brazil and the world.







