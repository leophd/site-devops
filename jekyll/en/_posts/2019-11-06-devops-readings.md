---
layout: post
title: "DevOps-related readings we highly recommend"
author: "Leonardo Leite"
lang: en
lang-ref: devops-readings
---

Out there, one can find hundreds of books and articles about DevOps. It's impossible to read all of them! So, what of them to read? Here we present a little curated list of readings on DevOps.

## Books

* Nicole Forsgren, Jez Humble, and Gene Kim. Accelerate: **The Science of Lean Software and DevOps: Building and Scaling High Performing Technology Organizations**. 2018
*  Gene Kim, Kevin Behr, and George Spafford. **The Phoenix Project: A Novel about IT, DevOps, and Helping Your Business Win**. 3rd edition. 2018
* Sam Newman. **Building Microservices: Designing Fine-Grained Systems**. 2015
* Eliyahu Goldratt and Jeff Cox. **The Goal: A Process of Ongoing Improvement**. 30th Anniversary Edition. 2014
*  Jez Humble and David Farley. **Continuous Delivery: Reliable Software Releases through Build, Test, and Deployment Automation**. 2010
* Michael Nygard. **Release It! Design and Deploy Production-Ready Software**. 2009.

## Articles

* A. Basiri, N. Behnam, R. de Rooij, L. Hochstein, L. Kosewski, J. Reynolds, and C. Rosenthal. **Chaos Engineering**. IEEE Software 33, 3 (2016).
* Dror G Feitelson, Eitan Frachtenberg, and Kent L Beck. **Development and deployment at Facebook**. IEEE Internet Computing 17, 4 (2013).
* Jez Humble and Joanne Molesky. **Why enterprises must adopt DevOps to enable continuous delivery**. Cutter IT Journal 24, 8 (2011).
* James Hamilton. **On Designing and Deploying Internet-Scale Services**. In Proceedings of the 21st Large Installation System Administration Conference (LISA ’07). USENIX. 2007.
* Jim Gray. 2006. **A conversation with Werner Vogels**. ACM Queue 4, 4 (2006).

## Others

* Adam Wiggins. 2011. **The Twelve-Factor App**. 2011. https://12factor.net/

