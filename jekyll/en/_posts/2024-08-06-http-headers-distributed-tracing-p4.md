---
layout: post
title: "HTTP headers for distributed tracing (observability) - Part 4: HTTP headers listings in the web"
author: "Leonardo Leite"
lang: en
lang-ref: http-headers-distributed-tracing-pt4
hiden: false
---

This series of posts investigates the **HTTP headers** used to support **tracing**, which is one of the pillars of **observability** (also composed of logs and metrics). Although we focus on investigating these headers, these posts also serve as an introduction to distributed tracing and as a presentation of some technological alternatives for its implementation. 

In particular, **the question is: could we produce an in-house solution for distributed tracing with HTTP headers compatible with market solutions?** Such an approach could bring portability and interoperability gains.

In this final post of the series, we examine some listings of known HTTP headers and try to reached a verdict about our question.

[The complete post]({{ site.baseurl }}{% post_url pt-br/2024-08-06-headers-http-rastro-distribuido-p4 %}.html){:target="_blank"} is available only in Portuguese.

## Post digest

Searching for lists of HTTP headers, we found some headers somehow related to distributed tracing:

* http.dev: `X-Request-ID`.
* Wikipedia: `X-Request-ID`, but deprecated in favor of `traceparent`.
* Mozilla MDN Web Docs: nothing related found.

Based on this study, it could be a good option to rename our headers as the following:

* `X-Request-ID` becomes `Trace-ID`.
* `CLIENT_APPLICATION_NAME` becomes `Client-Application-Name`.
* `CLIENT_CHAIN` becomes `Client-Chain`.
