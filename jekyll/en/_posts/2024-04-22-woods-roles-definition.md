---
layout: post
title: "Operations roles according to Eoin Woods"
author: "Leonardo Leite"
lang: en
lang-ref: woods-roles-definition
hiden: false
---

Eoin Woods lists and defines the following roles concerning the operation of a system [1]:

* **Operations Staff**, who accept new and changed software into production, operate the system in production and are responsible for its service levels;
* **Infrastructure Engineers**, who are responsible for providing the infrastructure services the system relies upon.
* **Developers**, who are responsible for the software, its smooth transition to production, and ultimately accountable for its success.
* **Testers**, who need to be able to verify that the software will operate correctly in production and that the production environment as a whole will operate correctly.
* **Communicators**, who in the context of developing a product for installation on client premises, need to explain the operation of the system to clients.
* **Assessors**, who need to be satisfied that the risks of operating the system in production are acceptable and managed.


-----------------

[1] Eoin Woods. **Operational: The Forgotten Architectural View.** IEEE Software, v. 33, n. 3, p. 20-23. 2016.

