---
layout: post
title: "DevOps and Marx: culture will not transform your organization"
author: "Leonardo Leite"
lang: en
lang-ref: edvops-culture-and-marx
published: true
hidden: false
---

Davis and Daniels (2016) define "effective DevOps" as:

*"An organization that embraces cultural change to affect how individuals think about work, value all the different roles that individuals have, accelerate business value, and measure the effects of the change."*

They also say that:

*"Devops is a prescription for culture. (...) Devops is about finding ways to adapt and innovate social structure, culture, and technology together in order to work more effectively."*

<!--
No cultural movement exists in a vacuum; social structure and culture are inherently intertwined. The hierarchies within organizations, industry connections, and globalization influence culture, as well as the values, norms, beliefs, and artifacts that reflect these areas. 
-->

Although the authors elaborate more in other excerpts, one can summarize the above ones as **change culture to accelerate business**. And, indeed, a lot about culture is talked in the DevOps and Agile worlds. For example, I heard from a consultant: *"Are the devs out there going to have that agile mindset? If not, it's no use."* Similarly, Peter Senge et al. (2011), in their book on organizational change, have a similar stand: *"It is not enough to change strategies, structures, and systems, unless the thinking that produced those strategies, structures, and systems also changes."* Moreover, the "third DevOps way" is related to improving daily work through a specific kind of culture (Kim, 2021). But let's consider another possible approach in this post. But before... what is culture?

The Effective DevOps book defines culture as *"the ideas, customs, and social behavior of a people or society"*. Still in the DevOps context, the Accelerate book (Forsgren, 2018) adopts a model of culture focused on how information flows through an organization; in this context, a culture can be pathological (power-oriented, involving fear and threat), bureaucratic (rule-oriented), or generative (performance-oriented).

So, should we focus on changing these things (e.g., social behavior) to build better software? 

Well, let's now consider the Marxian concepts of base and superstructure (Marx, 1859):

*The mode of production of material life [the base] conditions the general process of social, political, and intellectual life [the superstructure]. (...) The changes in the economic foundation lead sooner or later to the transformation of the whole immense superstructure.*

This means that the production mode of a society determines its culture, and not the other way around.

![Marx with DevOps glasses, just for fun.]({{ site.baseurl }}/assets/figures/devops-marx.png)

Off-course, such a view is disputable (the authors of Effective DevOps also know the other way around would be not so simple). But let's assume this Marxian hypothesis for granted for a moment. Maybe in some cases, it seems to make sense. Consider a company that changes the evaluation process from individual-centered to team-centered. The evaluation process is part of the production system, and for sure it will highly impact how people behave and interact. Organizational structure may also influence behavior. Consider the existence of a QA (quality assurance) team. The presence of this structure may encourage developers to be not so careful in their tests, possibly generating more buggy applications. 

In the DevOps context, making developers responsible for infrastructure management may promote a learning culture (there are no people to push the problems to). On the other hand, trying to establish a culture of collaboration among developers and the infrastructure group (previously working in silos) without changing structures may cause conflicts over blurred responsibilities.

The idea that organizational changes impact culture is put by Forsgren et al. (2018) as *"we also discovered that it is possible to influence and improve culture by implementing DevOps practices."* In their research, they discovered that practices of lean product development (e.g., work in small batches) and lean management (e.g., limit work in progress [wip]) promote a generative culture. In the agile world, it's common to say that agile is about culture, not practices or tools. However, Martin (2014) states that *"You can’t have a culture without practices; and the practices you follow identify your culture."*

Another view corroborating "culture as a product" is the one of Pflaeging (2020), in his book about complexity in organizations:

*Culture is not a success factor, but an effect of success or failure. (...) That is why it also cannot be influenced directly. (...) Culture is observable but no controllable. (...) A company cannot choose its culture, it has exactly the culture it deserves. Culture is something like the restless memory of an organization. (...) It endows something like a common "style" which all can rely on. (...) Culture, in this sense, is autonomous. Culture neither is a barrier to change, nor does it encourage change. It can provide hints to what an organization must learn. To change culture is impossible, but culture observation is valuable. (...) Culture allows us to indirectly observe the quality of change efforts: change trickles through into the culture.*

In his book, Pflaeging advocates a drastic change in the management approach (to a non-management approach!), stating this is necessary for companies to survive and beat the competition. Amusingly, one can read this in the Marxian sense that what leads to change (revolution!) are the contradictions of the current production system: one must change or be defeated by other superior ways of production.

Well, this post is not meant to draw some firm conclusion. Instead, it intends to alert that before trying to change people's behavior (their culture), it's fundamental to investigate the reasons that drove such people to behave in a certain way. 


Thank you for your attention.

PS: If you want to debate on this, please tweet to [@leonardofl](https://twitter.com/leonardofl){:target="_blank"}.


-------------------------------

Jennifer Davis, Ryn Daniels. **Effective DevOps**: Building a Culture of Collaboration, Affinity, and Tooling at Scale. O'Reilly Media. 2016.

Gene Kim, Jez Humble, Patrick Debois, John Willis, and Nicole Forsgren. **The DevOps Handbook**: How to Create World-Class Agility, Reliability, & Security in Technology Organizations. 2nd ed. IT Revolution Press. 2021.

Peter M. Senge, Art Kleiner, Bryan Smith, Charlotte Roberts, Geroge Roth, Richard Ross. **The Dance of Change**: The Challenges of Sustaining Momentum in Learning Organizations. Nicholas Brealey Publishing. 2011.

Nicole Forsgren, Jez Humble, and Gene Kim. **Accelerate**: The Science of Lean Software and DevOps: Building and Scaling High Performing Technology Organizations. IT Revolution Press. 2018.

Karl Marx. Preface to **A Contribution to the Critique of Political Economy**. 1859. Available at [https://www.marxists.org/archive/marx/works/1859/critique-pol-economy/preface.htm](https://www.marxists.org/archive/marx/works/1859/critique-pol-economy/preface.htm){:target="_blank"}.

Robert C. Martin. **The True Corruption of Agile**. 2014. Available at [https://blog.cleancoder.com/uncle-bob/2014/03/28/The-Corruption-of-Agile.html](https://blog.cleancoder.com/uncle-bob/2014/03/28/The-Corruption-of-Agile.html){:target="_blank"}.

Niels Pflaeging. **Organizing for complexity**: How to get life back into work to build the high-performance organization. 5th revised edition. Betacodex Publishing. 2020

