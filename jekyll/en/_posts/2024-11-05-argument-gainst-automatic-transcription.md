---
layout: post
title: "An argument against automatic transcription for Grounded Theory research based on interviews"
author: "Leonardo Leite"
lang: en
lang-ref: argument-against-automatic-translation
---

Research that follows the Grounded Theory methodology is based on the analysis of a set of qualitative data. In software engineering research, this data is typically the transcription of interviews conducted by researchers with professionals in the field. In general, many interviews must be conducted; there is no set number, but in practice, something greater than 30. In addition, the interviews can be long... at least half an hour, but generally around an hour.

In order to analyze the interviews, it is common for researchers to transcribe the interviews. And, at first, it seems very convenient to perform automatic transcription with the help of some good tool. However, there is at least one argument against this, and I will present it here.

The core of Grounded Theory practice, in the described context, consists of developing *codes* (abstract concepts) from excerpts from interviews and constantly comparing these *codes*, so that they evolve throughout the analysis (i.e., created, changed, eliminated, and merged).

![A set of interviews (rectangles) generates a set of concepts emerging from these interviews (circles); this is represented with arrows connecting some rectangles to some circles. In addition, lines connect the rectangles in all possible combinations; these lines label is: the constant comparison in the researchers' minds.]({{ site.baseurl }}/assets/figures/constant-comparison.png)
                                                                                    
<center><i>
Figure 1 - Interviews must be compared one by one so that concepts can emerge.
</i></center>

<br/>

For this *constant comparison* to be more effective, I argue that researchers should have the facts and allegations narrated in each interview by heart (indeed, *in their hearts*). But given the large number of interviews, this can be difficult. How can one retain all these interviews in memory? In general, the first interviews are more striking and the last ones are remembered because they are more recent, but perhaps the intermediate interviews run the risk of being more neglected.

Well, as arduous (i.e., time-consuming and tedious) as it may be, I claim that manual transcription (transcribing the text while listening to the interview recording) is the way researchers have to strengthen this memory of what they experienced and thus boost their analytical capabilities during the process of constant comparison.

PS: loooking to Figure 1, in principle, we could say a researcher should compare the interview under analysis with the codes already stablished, and not with the content of previous interviews. But I believe that, in practice, it is unavoidable to researchers to think directly on the interviews, that is their own live experience.

## Excellent complement of Professor Breno de França (Unicamp)

*Sure, manual transcription makes the researchers more intimate with the collected data, i.e., what some researchers call "being familiar with your data." However, manual transcription is not the only way to achieve this. In my group, we do use automated transcription, but we then listen to the whole interview and review the transcription. This way, you are still familiar with the data but with less typing!*

*Addendum: Recently, I heard (from several sources) the idea of coding based on LLM. This would make researchers lose contact with data and, consequently, understanding! So, as a qualitative researcher, I'm strongly against this practice...*

Thank you for sharing your thoughts Prof. Breno!


