---
layout: post
title: "Our theory on organizational structures of software teams has just been published!"
author: "Leonardo Leite"
lang: en
lang-ref: ist-publication
---

We are happy to announce that our article **The Organization of Software Teams in the Quest for Continuous Delivery: A Grounded Theory Approach** [has been published in Information and Software Technology](https://www.sciencedirect.com/science/article/abs/pii/S0950584921001324){:target="_blank"}, a very reputable journal in our field. 

<a href="https://www.sciencedirect.com/science/article/abs/pii/S0950584921001324" target="_blank">
![Screenshot of the ScienceDirect website with the official publication]({{ site.baseurl }}/assets/figures/ist-publication.png)
</a>

<p><center><i>
Figure 1 - Our paper published by Elsevier.
</i></center></p>

Many organizations wish to adopt DevOps. However, usually, there is some confusion about the meaning of DevOps: would it be about making developers and infrastructure people closer? Would it be about cross-functional teams? Would it be about creating a DevOps team? Well, we address this issue by observing the current industry practice.

This study investigates how different software-producing organizations structure their development and infrastructure teams, or more precisely: how is the division of labor among these groups and how they interact. After carefully analyzing data collected from 44 interviews with software professionals, we identified four common organizational structures: siloed departments, classical DevOps, cross-functional teams, and platform teams. 

![Diagram presenting the four organizational structures; diagram with boxes and arrows.]({{ site.baseurl }}/assets/figures/taxonomy-ist.png)

<p><center><i>
Figure 2 - High-level view of our taxonomy: discovered organizational structures and their supplementary properties
</i></center></p>

In the following, we briefly provide for each discovered structure: (i) the differentiation between development and infrastructure groups regarding operations activities (deployment, infrastructure setup, and service operation in run-time); and (ii) how these groups interact (integration).

* <strong>Siloed departments</strong>
  * <strong>Development differentiation:</strong> just builds the application package
  * <strong>Infrastructure differentiation:</strong> responsible for all operations activities
  * <strong>Integration:</strong> limited collaboration among the groups
  * <strong>Also called as:</strong>* segregated dev & infra departments

* <strong>Classical DevOps</strong>
  * <strong>Development differentiation:</strong> participates/collaborates in some operations activities
  * <strong>Infrastructure differentiation:</strong> responsible for all operations activities
  * <strong>Integration:</strong> intense collaboration among the groups
  * <strong>Also called as:</strong>* collaborating dev & infra departments

* <strong>Cross-functional teams</strong>
  * <strong>Development differentiation:</strong> Responsible for all operations activities
  * <strong>Infrastructure differentiation:</strong> Does not exist
  * <strong>Integration:</strong> --
  * <strong>Also called as:</strong>* single dev/infra department

* <strong>Platform teams</strong>
  * <strong>Development differentiation:</strong> responsible for all operations activities with the platform support
  * <strong>Infrastructure differentiation:</strong> provides a platform automating much of the operations activities
  * <strong>Integration:</strong> interaction happens for specific situations, not on a daily basis
  * <strong>Also called as:</strong>* API-mediated dev & infra departments

For better understanding these structures, you can also check [the digest of our taxonomy]({{ site.baseurl }}{% post_url en/2021-06-20-structures-digest %}.html){:target="_blank"}, or read the full article.

<br/>
<center>
<a href="https://arxiv.org/abs/2008.08652" target="_blank" class="button">Download the full paper here</a>
</center>
<br/>

The main contribution of this study is a theory in the form of a taxonomy organizing the found structures with their properties. This theory could guide researchers and practitioners to think about how to better structure development and infrastructure professionals in software-producing organizations. It also contributes to improving our understanding of the contemporary phenomenon of software production.

\* The article has just been accepted (June 2021), but the initial submission was almost one year ago (August 2020). During this long time, we have been working to evolve our taxonomy based on its use with practitioners. This is the reason we have two versions to name each of our structures. We expect to present the new version of the taxonomy in a future publication, also considering why different organizations adopt different structures.





