---
layout: post
title: "Second place award - Latin American Doctoral Thesis Contest!"
author: "Leonardo Leite"
lang: en
lang-ref: clei-award
hiden: false
---

We are excited to announce that our thesis ([A grounded theory of organizational structures for development and infrastructure professionals in software-producing organizations]({{ site.baseurl }}{% post_url en/2022-07-13-thesis-published %}.html){:target="_blank"}) won the second place award in the IX Latin American Doctoral Thesis Contest (CLTD 2023) within the XLIX Latin American Conference on Informatics (CLEI 2023)!

![With Bolivian friends]({{ site.baseurl }}/assets/figures/clei10.jpg)

<p><center><i>
Figure 1 - During the contest
</i></center></p>

------------------------

![Containing a table with authorities]({{ site.baseurl }}/assets/figures/clei11.jpg)

<p><center><i>
Figure 2 - Receiving the award
</i></center></p>

------------------------

![]({{ site.baseurl }}/assets/figures/clei12.jpg)

<p><center><i>
Figure 3 - The three finalists - me, Gabriela Cajamarca (Ecuador) and Rodrigo Silva (Unicamp / UFABC)
</i></center></p>

## The conference

The CLEI conferences have been run since 1974 by the Latin American Center for Informatics Studies (CLEI), and are the main Latin American space for the exchange of ideas, experiences, results, and applications among researchers, teachers, and students of Informatics and Computer Science. 

CLEI 2023 was held in la Paz, Bolivia, at the Universidad Mayor de San Andrés (*Computación análisis y semántica, adelante informática!!!*).

[Some interviews about CLEI 2023 can be checked at the TikTok of the Carrera de Informática of UMSA](https://www.tiktok.com/@carrera.informatica/){:target="_blank"}.

![]({{ site.baseurl }}/assets/figures/clei20.jpg)

<p><center><i>
Figure 4 - Friends of Tarija and UMSA
</i></center></p>

------------------------

![]({{ site.baseurl }}/assets/figures/clei21.jpg)

<p><center><i>
Figure 5 - XV Latin American Women in Computing Congress
</i></center></p>

------------------------

![Containing a map showing economic blocks (e.g., Mercosul)]({{ site.baseurl }}/assets/figures/clei22.jpg)

<p><center><i>
Figure 6 - Excellent talk about economic complexity
</i></center></p>

------------------------

![With wine bottles!]({{ site.baseurl }}/assets/figures/clei23.jpg)

<p><center><i>
Figure 7 - Finishing
</i></center></p>

## Cultural 

![Artists playing and dancing]({{ site.baseurl }}/assets/figures/clei50.jpg)

<p><center><i>
Figure 8 - Church party with Huayna Wila
</i></center></p>

------------------------

![Artists playing and dancing; me with them]({{ site.baseurl }}/assets/figures/clei51.jpg)

<p><center><i>
Figure 9 - Church party with Jach'a Malku
</i></center></p>

------------------------

![Artists playing and dancing; me with wine]({{ site.baseurl }}/assets/figures/clei52.jpg)

<p><center><i>
Figure 10 - CLEI opening
</i></center></p>

------------------------

![Me with CLEI people]({{ site.baseurl }}/assets/figures/clei53.jpg)

<p><center><i>
Figure 11 - CLEI dinner and other scenes
</i></center></p>

------------------------

![Artists playing and dancing; me with them]({{ site.baseurl }}/assets/figures/clei54.jpg)

<p><center><i>
Figure 12 - CLEI party with Huayna Wila (yes, again, but now even better!)
</i></center></p>

------------------------

![Artists playing and dancing]({{ site.baseurl }}/assets/figures/clei55.jpg)

<p><center><i>
Figure 13 - CLEI closure
</i></center></p>

## Tourism in La Paz

![]({{ site.baseurl }}/assets/figures/clei60.jpg)

<p><center><i>
Figure 14 - Going to El Alto by cable car (La Paz has the largest cable car network of the world)
</i></center></p>

------------------------

![]({{ site.baseurl }}/assets/figures/clei61.jpg)

<p><center><i>
Figure 15 - Killi Killi lookout with Danner (from Santa Cruz de la Sierra) and Gabriel (USP).
</i></center></p>

------------------------

![Themes: musical instruments, mine working, folkloric masks, and traditional hats]({{ site.baseurl }}/assets/figures/clei62.jpg)

<p><center><i>
Figure 16 - The excellent National Museum of Ethnography and Folklore in La Paz
</i></center></p>

## Souvenirs

![Bolivian themed bracelet on my wrist and toy llama on a baby's hand]({{ site.baseurl }}/assets/figures/clei71.jpg)

<p><center><i>
Figure 17 - Souvenirs
</i></center></p>














