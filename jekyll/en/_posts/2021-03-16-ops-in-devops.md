---
layout: post
title: "The ops in DevOps: operators?"
author: "Leonardo Leite"
lang: en
lang-ref: operators
hiden: false
published: true
---

DevOps is an amalgam of development and operations. But one could also interpret it as an amalgam of developers and operators. However, in this text, I would like to explain why the word "operator" is problematic.

![Women operators in a factory operating large machines, a man supervising, black and white photo, people wearing 1940s or 1950s costume.]({{ site.baseurl }}/assets/figures/operators.jpg)

<br/>

(But before a warning ⚠️: maybe I'm attacking a [strawman](https://en.wikipedia.org/wiki/Straw_man){:target="_blank"} since I think people do not usually hold the job title of "operator" in the IT context. However, some texts bring this word.)

When I write about DevOps, I prefer to use the term "infrastructure professionals" rather than simply operators. But as with many roles and titles in our industry, this is not straightforward. Woods (2016), for example, proposes the following terminology:

* Operations staff: accept and operate new and changed software in production and are responsible for its service levels.
* Infrastructure engineers: provide the infrastructure services the system relies on.

In other industries, it is common to apply the term operator to menial workers who control machines. So we would have engineers projecting the machines and operators operating them. If the "machine" built by software engineers is the software system, operators could be the system users. It may be strange to apply this term in the general context, in which end-users are the clients (e.g., webmail). Still, it seems pretty adequate to situations in which the users are employees of the client (who demanded the system), as would be the case for supermarket cashiers. 

Nonetheless, in the sense of "machine operator," we can consider the IT operator role as proposed by Woods: workers who run scripts to create environments and trigger restart commands when the system goes down. But, of course, the job in infrastructure is closer and closer to the "infrastructure engineer" definition provided by Woods. Providing the infrastructure requires planning, dimensioning, and automation. Moreover, when things are not working, investigative skills are demanded from infrastructure professionals. 

Therefore, in general, I understand that the word "operator" diminishes infrastructure professionals' role. Off-course, some companies may still hire a cheaper workforce to continuously run simple commands, but this is the work mindset that has been overcome by the DevOps movement's proposals.

However, I'm not so sure about my point. If you disagree or have other perspectives on the matter, please, comment on Twitter to [@leonardofl](https://twitter.com/leonardofl){:target="_blank"}. I have some additional thoughts about the "operator role" in [this other post]({{ site.baseurl }}{% post_url en/2021-01-09-future-work %}.html){:target="_blank"} too.

-----------------

## References

Eoin Woods. Operational The Forgotten Architectural View. IEEE Software. May/June 2016. 

## Picture

Photo by <a href="https://unsplash.com/@birminghammuseumstrust?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText" target="_blank">Birmingham Museums Trust</a> on <a href="https://unsplash.com/photos/9GSGllMJCeA" target="_blank">Unsplash</a>.


