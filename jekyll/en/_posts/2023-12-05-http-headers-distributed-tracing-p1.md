---
layout: post
title: "HTTP headers for distributed tracing (observability) - Part 1: Spring Cloud Sleuth"
author: "Leonardo Leite"
lang: en
lang-ref: http-headers-distributed-tracing
hiden: false
---

This series of posts investigates the **HTTP headers** used to support **tracing**, which is one of the pillars of **observability** (also composed of logs and metrics). Although we focus on investigating these headers, these posts also serve as an introduction to distributed tracing and as a presentation of some technological alternatives for its implementation.

In particular, **the question is: could we produce an in-house solution for distributed tracing with HTTP headers compatible with market solutions?** Such an approach could bring portability and interoperability gains.

For this quest, I started investigating **Sleuth** (Spring Boot's distributed tracing subsystem) and **OpenTracing**. In this first post, we will focus on Sleuth.

[The complete post]({{ site.baseurl }}{% post_url pt-br/2023-12-05-headers-http-rastro-distribuido-p1 %}.html){:target="_blank"} is available only in Portuguese.

## Post digest

Sleuth is part of Spring Boot, a very popular framework for developing web applications in Java. A service using Sleuth will have each entry in its log associated with a span ID and a trace ID, with the span ID identifying the processing that occurred in the service, while the trace ID represents the distributed transaction.

The span ID is typically transferred from one service to another via the `X-B3-SpanId` header, while the trace ID is transferred via the `X-B3-TraceId` header. Other data linked to the distributed transaction can be transferred between services via "baggage", which are extra headers with names starting with `baggage-`.

Sleuth can also expose all of these aforementioned values to OpenZipkin, which is a visual interface that allows humans to analyze the details of distributed transactions, especially the processing time spent on each service.

In the [next post]({{ site.baseurl }}{% post_url pt-br/2024-04-01-http-headers-distributed-tracing-p2 %}.html){:target="_blank"} in the series we will analyze OpenTracing.

