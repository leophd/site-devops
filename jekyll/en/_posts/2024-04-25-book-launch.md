---
layout: post
title: "Book launch at Google Campus for Startups!"
author: "Leonardo Leite"
lang: en
lang-ref: book-launch
hiden: false
---

Book launch event at Google Campus for Startups (Apr 11, 2024) with a debate starring Fabricia Doria, Alexandre Freire, Sérgio Lopes, and Leonardo Martins.

The event was conjoined with the 12th anniversary of Casa do Código, the publisher of our book.

![]({{ site.baseurl }}/assets/figures/lancamento-livro1.jpeg)

<p><center><i>
Figure 1 - Authors of the book Como se faz DevOps among the night's debaters
</i></center></p>

------------------------

![]({{ site.baseurl }}/assets/figures/lancamento-livro2.jpeg)

<p><center><i>
Figure 2 - Leonardo giving a very brief summary of the book
</i></center></p>

------------------------

![Auditorium for 100 people almost full]({{ site.baseurl }}/assets/figures/lancamento-livro3.jpeg)

<p><center><i>
Figure 3 - Audience present
</i></center></p>

------------------------

![Fabio speaks into the microphone while standing, while debaters are seated]({{ site.baseurl }}/assets/figures/lancamento-livro4.jpeg)

<p><center><i>
Figure 4 - Debate going on
</i></center></p>

------------------------

![]({{ site.baseurl }}/assets/figures/lancamento-livro5.jpeg)

<p><center><i>
Figure 5 - Leonardo and Fabio with Mr. Carlos from Casa do Código
</i></center></p>

------------------------

![]({{ site.baseurl }}/assets/figures/lancamento-livro6.jpeg)

<p><center><i>
Figure 6 - Leonardo with Vivian, responsible for editing the book Como se faz DevOps
</i></center></p>

------------------------

![]({{ site.baseurl }}/assets/figures/lancamento-livro7.jpeg)

<p><center><i>
Figure 7 - Leonardo with Gabriela, the first to get an autograph!
</i></center></p>

------------------------

![]({{ site.baseurl }}/assets/figures/lancamento-livro8.jpeg)

<p><center><i>
Figure 8 - Leonardo autographing
</i></center></p>

------------------------

![Bag, coffee cup, pad with post-its, pen and bookmark from Casa do Code; and another personalized bookmark (which says "Dante")]({{ site.baseurl }}/assets/figures/lancamento-livro9.jpeg)

<p><center><i>
Figure 9 - Gifts of the night for the audience
</i></center></p>

------------------------

![]({{ site.baseurl }}/assets/figures/lancamento-livro10.jpeg)

<p><center><i>
Figure 10 - Casa do Código books
</i></center></p>












