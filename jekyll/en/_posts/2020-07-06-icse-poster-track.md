---
layout: post
title: "Presenting our research at the ICSE 2020 Poster Track"
author: "Leonardo Leite"
lang: en
lang-ref: icse2020-poster-track
---

Our work "Building a Theory of Software Teams Organization in a Continuous Delivery Context" was accepted for presentation in the Poster Track of the 42nd International Conference on Software Engineering (ICSE 2020).

Our participation at this poster track includes three artifacts and one event:

* A video presentation.
* A poster image.
* An extended abstract of our work.
* A discussion session.

## Video presentation

A video taking 5 minutes, briefly explaining our work about organizational structures for continuous delivery adoption.

<p><iframe title="Our presentation video at Youtube" width="560" height="315" src="https://www.youtube.com/embed/xroC_fyGy5Q" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>

[Other poster videos can be seen here.](https://www.youtube.com/playlist?list=PLh7v-bsdypMHYPyE6DF7slV9DAXiHNlgX){:target="_blank"}

## Poster image

In a regular conference (not in pandemic times), this image would be printed in a physical poster. 

<a href="{{ site.baseurl }}/assets/figures/poster-icse2020.png" target="_blank">
<img alt="Our poster at ICSE 2020." src="{{ site.baseurl }}/assets/figures/poster-icse2020.png"/>
</a>

<center><i>
Figure 1 - Our poster at ICSE 2020.
</i><p/></center>

## Extended abstract

A two-pages documents briefly explaining our research.

<br/>
<center>
<a href="{{ site.baseurl }}/assets/files/theory-software-teams-icse-2020-poster-track-camera-ready-version.pdf" class="button" target="_blank">Download the extended abstract here</a>
</center>
<br/>

## Discussion session

At 9th July (14:10 São Paulo time), there will be a discussion session, where poster presenters (including ourselves) will answer questions posed by ICSE participants.
















