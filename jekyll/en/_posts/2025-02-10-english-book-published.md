---
layout: post
title: "Our book on DevOps has now an English edition!"
author: "Leonardo Leite"
lang: en
lang-ref: english-book-published
---

Exciting news! Our book has now an English edition! It is entitled "How DevOps Works: Organizing People, from Silos to Platform Teams". This book is the result of rigorous and award-winning academic research that systematizes the knowledge shared by practitioners around the world, offering a deep dive into the organizational side of DevOps transformations.

![The book cover]({{ site.baseurl }}/assets/figures/english-edition-cover.jpg)

The book targets impact primarily on the industry. Thus, although it is based on academic research, its writing is adapted so that the text is useful and fluid for industry professionals (devs, ops and managers).

<br/>
<center>
<a href="https://www.amazon.com/How-DevOps-Works-Organizing-Platform-ebook/dp/B0DTR7SCN9/ref=sr_1_fkmr0_1" target="_blank" class="button">Book purchase here (Amazon)</a>
</center>
<br/>






