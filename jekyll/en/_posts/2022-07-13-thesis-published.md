---
layout: post
title: "My PhD thesis on organizing development and infrastructure professionals is published!"
author: "Leonardo Leite"
lang: en
lang-ref: thesis-published
---

I'm happy to announce that my PhD thesis, entitled **A grounded theory of organizational structures for development and infrastructure professionals in software-producing organizations**, has been published in the Digital Library of the University of São Paulo (USP).

![Me holding the printed thesis]({{ site.baseurl }}/assets/figures/tese.jpg)


<br/>
<center>
<a href="https://www.teses.usp.br/teses/disponiveis/45/45134/tde-28062022-132626/en.php" target="_blank" class="button">Full text available here</a>
</center>
<br/>

This doctoral research was advised by:

* Professor Paulo Meirelles - Universidade Federal do ABC (UFABC) 
* Professor Fabio Kon - Universidade de São Paulo (USP) 

I defended the thesis on 31st May 2022, and the following professors were on the examination committee:

* Fabio Kon (President) - Universidade de São Paulo (USP) 
* Jessica Diáz Fernández - Universidad Politécnica de Madrid (UPM)
* Breno Bernard Nicolau de França - Universidade Estadual de Campinas (Unicamp)
* Carolyn Seaman - University Of Maryland, Baltimore County (UMBC)
* Guilherme Horta Travassos - Universidade Federal do Rio de Janeiro (UFRJ)

<br/>
<center>
<a href="https://pt.slideshare.net/leonardoferreiraleite/defesa-de-doutorado-leonardo-leite-usp" class="button" target="_blank">Slides (thesis presentation) here</a>
</center>
<br/>

## Research summary

This doctoral research elaborated a theory comprised of:

* A taxonomy describing the organizational structures used by industry in the real world regarding how the work of developers and infrastructure engineers can be coordinated in the pursuit of continuous delivery. [A digest of this taxonomy is available here]({{ site.baseurl }}{% post_url en/2021-06-20-structures-digest %}.html){:target="_blank"}.
* Theoretical assertions (conditions, causes, avoidance reasons, consequences, and contingencies) associated with each organizational structure of our taxonomy. Such an explanatory dimension of our theory sought to unfold why different organizations adopt different structures. [These theoretical assertions are summarized here]({{ site.baseurl }}{% post_url en/2022-04-06-strong-codes %}.html){:target="_blank"}.






