---
layout: post
title: "Conceptual research for software engineering"
author: "Leonardo Leite"
lang: en
lang-ref: conceptual-research
published: true
---

In this post, I discuss the pertinence of software engineering research to take a stand closer to sciences of knowledge (naturalistic inquires, social sciences, physics), and not only to focus on problem-solving, i.e. seeking to provide solutions for the software industry.

Firstly, what is science? What is the difference between science and engineering? Or between science and art? For sure, there are different answers to such questions. Let's take a look at what Donald Knuth (1974) has said on the subject:

*If we go back to Latin roots, we find ars, artis meaning "skill." It is perhaps significant that the corresponding Greek word τέχνη, is the root of both "technology" and "technique." (...) As civilization and learning developed, the words took on more and more independent meanings, "science" being used to stand for knowledge, and "art" for the application of knowledge. Thus, the science of astronomy was the basis for the art of navigation. The situation was almost exactly like the way in which we now distinguish between "science" and "engineering."*

So, for short, science = knowledge, not the application of such knowledge.

Many software engineering investigators present their research as tools or guidelines ready to be directly applied by practitioners, according to a legitimate desire to improve the means of production in the software industry. I believe this is partly because computer scientists have, in general, a problem-solving background (be as developers or engineers). Please don't take me wrong; this is a noble and necessary goal, especially in emergent countries. Nonetheless, not every research needs to take this stand. Naturalistic inquires, physics, and social sciences do not, for example.

Consider the classification of new species or the creation of taxonomies of living beings. What is the direct practical purpose? Well, the objective is to organize knowledge, enhancing our understanding of the world. Researchers may devise applications for such taxonomies, but this is not the point. As possible applications are not the core driver of research on quantum mechanics.

Taking an example from social sciences, Jessé Souza (2019) provides a taxonomy for politically labeling middle-class Brazilian individuals. Is the purpose of such taxonomy to be "useful"? For sure, an application may be devised, for example, for political marketing during elections. But the primary goal is to understand society, provide a view of the world. And, for sure, regardless of the existence of a direct application, this and many other sociological concepts may impact our lives. For example, the concept of "cultural appropriation" may refrain us from adopting specific costumes. However, the concept was probably created more to describe a problematic situation than to impose a new rule on society. Or as put by Gregor (2006): 

*The mere act of classifying people or things into groups and giving them names (e.g., black versus white) can give rise to stereotypical thinking and have political and social consequences.*

What I mean: software engineering research can also take a stand in seeking to understand the world. Or better: before teaching lessons to the software industry, science must understand how the software production really happens, organizing such phenomenon in categories that students can later learn. This already contributes to improving the software production landscape. Moreover, just creating concepts (e.g., microservices) may impact practitioners, who gain new targets to pursue. Sometimes knowledge is already available, but condensing it in labels has powers, as the agile movement has shown.


-------------------------------

Donald Knuth. **Computer Programming as an Art**. Communications of the ACM, 17 (12). 1974.

Jessé de Souza. **A elite do atraso**: da escravidão a Bolsonaro. Estação Brasil. 2019

Shirley Gregor. **The nature of theory in information systems**. MIS quarterly, 611–642, 2006.
