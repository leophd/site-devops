---
layout: post
title: "Presenting our research at the 6th International Workshop on Rapid Continuous Software Engineering"
author: "Leonardo Leite"
lang: en
lang-ref: rcose2020
---

## About RCoSE

Rapid continuous software engineering (RCoSE) refers to the organizational capability to develop, release and learn from software in rapid parallel cycles, typically hours, days or very small numbers of weeks. 

[RCoSE workshop](http://continuous-se.org/RCoSE2020/){:target="_blank"} is held in conjunction with the International Conference on Software Engineering (ICSE), the most reputable conference on software engineering in the world. 

![Picture is a screen shot from the workshop.]({{ site.baseurl }}/assets/figures/rcose1.jpg)

<center><i>
Figure 1 - RCoSE starting.
</i><p/></center>

## Our presentation

This is our presentation about our accepted paper about [Platform Teams]({{ site.baseurl }}{% post_url en/2020-03-17-platform-teams %}.html){:target="_blank"} at RCoSE 2020:

<iframe width="560" height="315" src="https://www.youtube.com/embed/hAEKzjJBIJA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Discussion about our work

* Question: drivers for choosing structures. Answer: this is the research question for my next PhD phase :) But I provided some hints based on organization size.

* Comment: classical DevOps vs the idea that DevOps would be dev and ops within the same team. Answer: the initial intent of the DevOps movement was to bring collaboration among areas, not unifying them. I did not show at the workshop, but this presentation, one of the first about DevOps, supports our point: [https://www.youtube.com/watch?v=LdOe18KhtT4](https://www.youtube.com/watch?v=LdOe18KhtT4){:target="_blank"}.

* Question: how many companies were in classical DevOps? Answer: I showed our latest data: there are many companies in classical DevOps, but few of them are high performers.

* Question: what interviewees understood by DevOps? Answer: we avoided this question; we asked more objective questions.

* Question: and about measuring the delivery performance metrics (rather than just asking about them)? Answer: OK for one company if you work at this company, but not feasible for my research context.

* Question: causality platform teams => high delivery performance? Answer: it would be a great research, but it does not fit in my PhD; nonetheless, I provided some theoretical explanation for the possible causality (in the sense that platform teams cope to high delivery performance).

* Comment about the Team Topologies book. Answer: I read the book just a few months ago, after the development of the research; there was a lot of convergence between our research and the book.

* Question: which structure would be SRE? Would it be platform teams? Answer: in a first moment, SRE is like an enabler team, providing consulting; after the product is stable, devs hand-over the product for SREs, so it becomes like classical DevOps.

![A picture of me at my home participating in RCoSE.]({{ site.baseurl }}/assets/figures/rcose2.jpg)

<center><i>
Figure 2 - Me, at home, taking part of RCoSE :)
</i><p/></center>

## Other topics of the workshop

* Keynote by Robert Martin (BMW) about multi-stage CI system.
* Presentation about an analytical platform (QRapids) that generates software-engineering indicators from tools like Sonar, Jenkins, SVN, etc.
* Presentation about a planer tool (Sapling) that generates sprints plans considering constrains like dependencies among stories to enable agile planning at large scale organizations.
* Discussion about traceability: benefits, pitfalls, means to create and manage traceability, what to trace and what not to trace.
* Discussion about the state of RCoSE at companies. Why some companies are not interested in become fast? Collaboration of companies in research.





