---
layout: post
title: "The best software engineering thesis of Brazil in 2022!!!"
author: "Leonardo Leite"
lang: en
lang-ref: ctd-es-first-place
hiden: false
---

We are excited to announce that our thesis ([A grounded theory of organizational structures for development and infrastructure professionals in software-producing organizations]({{ site.baseurl }}{% post_url en/2022-07-13-thesis-published %}.html){:target="_blank"}) won the 1st place award in the Software Engineering Doctoral and Master Theses Competition (CTD-ES 2023) of CBSoft (Brazilian Conference on Software: Practice and Theory)!!!

![Leonardo and Paulo holding the award certificate; Paulo celebrating with raised fist; behind a big screen displaying the award certificate.]({{ site.baseurl }}/assets/figures/ctd-es1.jpg)

<p><center><i>
Figure 1 (above) - Leonardo Leite and Paulo Meirelles (advisor) receiving the award at CTD-ES CBSoft 2023
</i></center></p>

------------------------

![Scenes from CTD, including Leonardo presenting and an amigurumi Brazilian jaguar reading the award-winning thesis.]({{ site.baseurl }}/assets/figures/ctd-es2.jpg)

<p><center><i>
Figure 2 (above) - CBSoft 2023 CTD-ES scenes
</i></center></p>

The Software Engineering Doctoral and Master Theses Competition (CTD-ES) aims to disseminate and award the best Doctoral and Master theses in Software Engineering that were concluded, presented, and approved in Brazil in 2022.

The Brazilian Conference on Software: Practice and Theory (CBSoft) aims at fostering the exchange of experience among researchers and practitioners from industry and academia about the most recent research, tendencies, and theoretical and practical innovations on software. 

The 14th edition of CBSoft was held from September 25th to 29th at the Federal University of Mato Grosso do Sul (UFMS), located in Campo Grande (MS), being the first in-person edition of CBSoft since the Covid-19 pandemic. In this way, the event was a privileged forum for meeting and discussion for the community.

![Photos of Leonardo with several CBSoft participants; photo of Leonardo presenting; in the center an amigurumi Brazilian jaguar and a baby t-shirt with a drawing of an alligator.]({{ site.baseurl }}/assets/figures/cbsoft2023-1.jpg)

<p><center><i>
Figure 3 (above) - Scenes from CBSoft 2023 - day 2
</i></center></p>

------------------------

![Photos of Leonardo with several CBSoft participants; photo of the university restaurant with large indigenous-themed drawings.]({{ site.baseurl }}/assets/figures/cbsoft2023-2.jpg)

<p><center><i>
Figure 4 (above) - Scenes from CBSoft 2023 - day 3
</i></center></p>

------------------------

![Photos of Leonardo with a CBSoft participant; several photos with capybaras.]({{ site.baseurl }}/assets/figures/cbsoft2023-3.jpg)

<p><center><i>
Figure 5 (above) - Scenes from CBSoft 2023 - day 4
</i></center></p>

------------------------

![Photos of Leonardo with several CBSoft participants; photo of a sobá plate; photo in an aquarium.]({{ site.baseurl }}/assets/figures/cbsoft2023-4.jpg)

<p><center><i>
Figure 6 (above) - Scenes from CBSoft 2023 - day 5
</i></center></p>

In the context of the Third School of Software Engineering (LATAM School), held in the context of CBSoft, I also made a poster presentation about our research.

<a href="{{ site.baseurl }}/assets/figures/poster-latam-school2023.png" target="_blank">
<img alt="Our poster at the LATAM School" src="{{ site.baseurl }}/assets/figures/poster-latam-school2023.png"/>
</a>

<center><i>
Figure 7 - Our poster at the LATAM School
</i><p/></center>














