---
layout: post
title: "HTTP headers for distributed tracing (observability) - Part 2: OpenTracing"
author: "Leonardo Leite"
lang: en
lang-ref: http-headers-distributed-tracing-pt2
hiden: false
---

This series of posts investigates the **HTTP headers** used to support **tracing**, which is one of the pillars of **observability** (also composed of logs and metrics). Although we focus on investigating these headers, these posts also serve as an introduction to distributed tracing and as a presentation of some technological alternatives for its implementation.

In particular, **the question is: could we produce an in-house solution for distributed tracing with HTTP headers compatible with market solutions?** Such an approach could bring portability and interoperability gains.

For this quest, I started investigating **Sleuth** (Spring Boot's distributed tracing subsystem) and **OpenTracing**. In this second post, we will focus on OpenTracing.

[The complete post]({{ site.baseurl }}{% post_url pt-br/2024-04-01-headers-http-rastro-distribuido-p2 %}.html){:target="_blank"} is available only in Portuguese.

## Post digest

In essence, OpenTracing provides a distributed trace model. I.e., it defines the terminology of this activity, especially the terms "trace" and "span". This terminology is embraced by Spring Cloud Sleuth, so we can say that Sleuth is OpenTracing compatible.

Furthermore, OpenTracing does not define names for HTTP headers that transfer trace data from one service to another. The OpenTracing documentation ends up citing the W3C and B3 standards, which are the options available in Spring Sleuth.

However, OpenTracing is now obsolete and has been absorbed by the OpenTelemetry project, which has an evolved model, so that compatibility with the terminology advocated by OpenTracing does not necessarily still holds. This outcome, in particular, makes us more comfortable adopting the terms we find most convenient in homemade solutions, regardless of the terms used in possible standards, since this seems to be an area that is not yet so consolidated.

In the next part of this series we will point out some more references that can guide the choices of terms and names for HTTP headers in homemade distributed trace solutions.

