---
layout: post
title: "A Digest of Organizational Structures in Quest for Continuous Delivery"
author: "Leonardo Leite, Fabio Kon, Gustavo Pinto, and Paulo Meirelles"
lang: en
lang-ref: structures-digest2
hiden: false
published: false
---

<!-- Para imprimir e não parecer que tem tantas páginas,
alterar linha 12 de _sass/tale/_layout.scss para:
  max-width: 1000px;
Para evitar quebra de linha entre figura e legenda, diminuí o tamanho do título:
    font-size: 2rem; na linha 18 de _sass/tale/_post.scss
Além disso, remover menu superior (mas não autoria) e rodapé.
Imprimir com o Chrome.
-->

The **DevOps movement**, as vividly portrayed in the novel *The Phoenix Project*, came up as a cultural shift to **break down the silos** in large organizations, better integrating development and operations teams through **collaboration**. But this collaboration can happen in different ways from an organizational perspective: developers and infrastructure specialists can be part of different departments or can be together in a single **cross-functional** team. With advancements in PaaS offers, it is possible even to envision developers themselves taking operations responsibilities.

Our current research, at IME-USP (University of São Paulo), aims to investigate how companies pursuing continuous delivery are
organizing their development and operations teams. We are taking this endeavor by interviewing software professionals to understand how things are really happening in the real world. By advancing our research, we hope to provide a theory to support organizations in designing their organizational structures toward continuous delivery and providing guidance on the consequences of a given organization structure choice.

Based on the interviews conducted so far, we started the elaboration of a theory describing the **organizational structures** used by industry in the real world regarding how the work of developers and infrastructure engineers can be coordinated in the pursuit of continuous delivery. In this digest, we present our current understanding of such organizational structures: siloed departments, classical DevOps, cross-functional teams, and platform teams. Each of these structures has **core properties**, which are commonly seen in organizations with a given structure, and **supplementary properties**, which may or may not be present, but, when present, supports the structure explanation. 

![Discovered organizational structures and their supplementary properties; diagram with boxes and arrows.]({{ site.baseurl }}/assets/figures/taxonomy.png)

<center><i>
Figure 1 - Discovered organizational structures and their supplementary properties.
</i><p/></center>

In the following, we briefly present each structure, alongside their core and supplementary properties.

## Siloed departments

Siloed departments is "the pre-DevOps" structure existing in large organizations, presenting limited collaboration among departments and barriers for continuous delivery. This structure is considered to be the problem that DevOps came to solve.

![Figure shows an operator and a developer, each on inside a different circle. They have their backs to each other. Communication flows through letters, more from developer to operator than from operator to developer.]({{ site.baseurl }}/assets/figures/siloed-departments.png)

<center><i>
Figure 2 - With siloed departments, operators and developers are each one in their own bubbles. They do not interact directly too much and communication flows slowly by bureaucratic means (ticket systems).
</i><p/></center>

Core properties:

* Developers and operators have well-defined and different roles. 
* Developers have a minimal vision of what happens in production.
* Monitoring and handling incidents are mostly done by the infrastructure team. 
* Developers often neglect non-functional requirements (NFR).
* There are many conflicts among silos since each department aims its own interests, looking for local optimization, not the global optimization of the whole organization.
* DevOps initiatives are centered on adopting continuous integration tools rather than improving collaboration among silos.
* The lack of proper automated tests can hinder [delivery performance]({{ site.baseurl }}{% post_url en/2019-11-27-delivery-performance %}.html){:target="_blank"} and reliability.

Supplementary properties:

* *Enabler team*: provides consulting and tools for product teams, but does not own any service. Consulting can be, for example, on performance and security. Tools provided by enabler teams include the deployment pipeline, high-availability mechanisms, and monitoring tools. We found them in all organizational structures. 
* *With a platform*: the organization possesses a platform able to provide deployment automation, but without following the patterns of human interaction and collaboration described by the core properties of platform teams. This includes organizations with a single cross-functional team building a platform and organizations having siloed-style interactions between developers and the platform team.
* *With an in-house open-source platform*: the organization have installed in its infrastructure an open-source platform, such as Rancher.


## Classical DevOps

The classical DevOps structure focuses on collaboration among developers and the infrastructure team. It is the way the book "The Phoenix Project" portrays DevOps.

![Figure shows an operator and a developer, each on inside a different circle. They are looking to each other and are holding hands (with some difficulty).]({{ site.baseurl }}/assets/figures/classical-devops.png)

<center><i>
Figure 3 - With classical DevOps, operators and developers in different departments seek to work together, even if not easy, by direct contact and close collaboration.
</i><p/></center>

Core properties:

* There is a culture of collaboration and communication among departments, which work in an aligned way.
* Usually, there are no conflicts regarding who is responsible for each task.
* NFR responsibilities are shared among developers and the infrastructure team. 
* The infrastructure staff is still in the front line of tracking monitoring and incident handling.
* While developers feel alleviated because they can count on the infrastructure team, stress can persist at
high levels for the infrastructure team.
* There is no apparent correlation between the classical DevOps structure and delivery performance.

Supplementary properties:

* *Infra as development collaborator*: infrastructure engineers have advanced coding skills and contribute to the application code-base to improve the non-functional properties of the application.
* *Enabler team*: provides consulting and tools for product teams, but does not own any service. Consulting can be, for example, on performance and security. Tools provided by enabler teams include the deployment pipeline, high-availability mechanisms, and monitoring tools. We found them in all organizational structures. 
* *With a platform*: the organization possesses a platform able to provide deployment automation, but without following the patterns of human interaction and collaboration described by the core properties of platform teams. This includes organizations with a single cross-functional team building a platform and organizations having siloed-style interactions between developers and the platform team.
* *With a platform of its own*: the organization has built its own in-house platform, operating over its physical infrastructure.

## Cross-functional teams

In our context, a cross-functional team is one that takes responsibility both for software development and infrastructure management. It is more aligned with the Amazon motto "*You built it, you run it*," giving more freedom to the team along with a great deal of responsibility.

![Figure shows an operator and a developer, they are inside the same circle.]({{ site.baseurl }}/assets/figures/cross-functional-team.png)

<center><i>
Figure 4 - A cross-functional team contains both operators and developers.
</i><p/></center>

Core properties:

* Communication and standardization among cross-functional teams within a single organization must be well managed to avoid duplicated efforts.
* A challenge in forming cross-functional teams is guaranteeing they have members with the necessary skills.
* Not prevalent in large organizations.

Supplementary properties:

* *With infra expert(s)*: the team has at least one infrastructure specialist.
* *With full-stack engineer(s)*: the team has at least one developer with advanced knowledge of infrastructure.
* *With no infra experts*: even having to manage the infrastructure, the team has no advanced knowledge of infrastructure.
* *Enabler team*: provides consulting and tools for product teams, but does not own any service. Consulting can be, for example, on performance and security. Tools provided by enabler teams include the deployment pipeline, high-availability mechanisms, and monitoring tools. We found them in all organizational structures. 
* *With a platform*: the organization possesses a platform able to provide deployment automation, but without following the patterns of human interaction and collaboration described by the core properties of platform teams. This includes organizations with a single cross-functional team building a platform and organizations having siloed-style interactions between developers and the platform team.
* *Cloud façade*: the organization provides a platform that consumes a public cloud (e.g., AWS, Google Cloud, or Azure), but in such a way that developers may not be aware of it.

## Platform teams

In this case, the infrastructure team (also called the platform team) provides highly-automated infrastructure services ("the platform") to empower product teams.

![Figure shows an operator and a developer, each on inside a different circle. The operator provides an interface that is consumed by the developer (UML notation).]({{ site.baseurl }}/assets/figures/platform-team.png)

<center><i>
Figure 5 - The platform team provides automated services to be used, with little effort, by developers. The platform team and developers are open to hear and support each other.
</i><p/></center>

Core properties:

* The existence of a delivery platform enables the product team to operate their own business services and minimizes the need for infrastructure specialists within the product team. 
* Although the product team becomes fully responsible for NFRs of its services, it is not a significant burden, since the platform abstracts away the underlying infrastructure and handles several NFR concerns.
* The product team is the first one to be called when there is an incident; the infrastructure people are escalated if the problem is related to some infrastructure service. 
* Product teams become decoupled from the members of the platform team.
* Usually, the communication among the development team and the platform team happens when infrastructure members provide consulting for developers or when developers demand new capabilities for the platform. 
* The infrastructure team is no more requested for operational tasks.
* The platform may be not enough to deal with particular requirements; usually, the platform is tailored for the typical case of functionality built in the organization.
* Infrastructure specialists possess coding skills.
* Organizations with platform teams present the best outcomes in terms of delivery performance.
* Not for small organizations.

Supplementary properties:

* *Cloud façade*: the organization provides a platform that consumes a public cloud (e.g., AWS or Google Cloud), but in such a way that developers may not be aware of it.
* *With an in-house open-source platform*: the organization have installed in its infrastructure an open-source platform, such as Rancher.
* *With a platform of its own*: the organization has built its own in-house platform, operating over its physical infrastructure.
* *Enabler team*: provides consulting and tools for product teams, but does not own any service. Consulting can be, for example, on performance and security. Tools provided by enabler teams include the deployment pipeline, high-availability mechanisms, and monitoring tools. We found them in all organizational structures.

## Just a few more considerations

First, if one classifies a specific organization as adhering to one of the above structures, it does not mean that the particular company will necessarily follow all the structure characteristics. Each organization is unique and has some adaptations for its context. Nonetheless, the model can assist reasoning on understanding organizations and on supporting decision-making regarding organizational changes.

For now, we believe our emerging model provides the following primary benefits:

* Offering concepts to distinguish clearly classical DevOps from cross-functional teams as alternative choices. In different scenarios, people use the word DevOps to describe both of them, and this can be confusing and disorient organization wishing to "adopt DevOps."
* Distinguishing the "platform team" as a separate organizational structure, making clear it has a different set of consequences when compared to classical DevOps or cross-functional teams.

Another distinctive characteristic of our model is that it is grounded on data retrieved from software professionals in the real world.





