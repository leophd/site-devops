---
layout: post
title: "Presentation: from commit to production, continuous integration and continuous delivery in the deployment pipeline"
author: "Leonardo Leite"
lang: en
lang-ref: pipeline-presentation
hiden: false
published: true
---

This presentation is based in a class I taught to students of a technical school; it's about:

* The stages in a deployment pipeline
* Continuous integration
* Continuous delivery

<br/>
<center>
<a href="https://pt.slideshare.net/leonardoferreiraleite/do-commit-produo-integrao-contnua-e-entrega-contnua-no-pipeline-de-implantao" class="button" target="_blank">Slides here (in Portuguese)</a>
</center>
<br/>

