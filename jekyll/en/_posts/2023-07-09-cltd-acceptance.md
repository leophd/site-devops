---
layout: post
title: "Our thesis is a finalist in the Latin American Doctoral Thesis Contest!"
author: "Leonardo Leite"
lang: en
lang-ref: cltd
hiden: false
---

We are excited to announce that our thesis ([A grounded theory of organizational structures for development and infrastructure professionals in software-producing organizations]({{ site.baseurl }}{% post_url en/2022-07-13-thesis-published %}.html){:target="_blank"}) has been chosen as **one of the three finalists** in the IX Latin American Doctoral Thesis Contest (CLTD 2023)!!!

![Latin America map with south pointing up, highlighting La Paz, Bolivia]({{ site.baseurl }}/assets/figures/america-latina.jpg)

<p><center><i>
Figure 1 - Our Latin America (by the Uruguayan Antonio Torres Garcia)
</i></center></p>

To encourage the development of doctoral programs, the Latin American Center for Informatics Studies (CLEI) has held the Latin American Doctoral Thesis Contest (CLTD) since 2016, in which doctoral graduates from all CLEI member universities can participate. 

![Email screenshot, writing in Spanish language]({{ site.baseurl }}/assets/figures/aceite-cltd.png)

<p><center><i>
Figure 2 - Finalist communication sent by the CLTD chairs
</i></center></p>

The contest will be held within the XLIX Latin American Conference on Informatics (CLEI 2023) in October at the Universidad Mayor de San Andrés (La Paz, Bolivia).

![Photo of the event location, the Universidad Mayor de San Andrés pitcture]({{ site.baseurl }}/assets/figures/universidad-san-andres.png)

<p><center><i>
Figure 3 - Universidad Mayor de San Andrés picture (La Paz, Bolivia)
</i></center></p>

The CLEI conferences have been run since 1974 by the Latin American Center for Informatics Studies (CLEI), and are the main Latin American space for the exchange of ideas, experiences, results, and applications among researchers, teachers, and students of Informatics and Computer Science. 






