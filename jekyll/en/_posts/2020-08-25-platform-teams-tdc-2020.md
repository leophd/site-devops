---
layout: post
title: "TDC São Paulo 2020 - Platform Teams: state-of-the-art and practice to accelerate continuous delivery"
author: "Leonardo Leite"
lang: en
lang-ref: tdc-2020
hiden: false
published: true
---

Today I had the honor and pleasure of giving a lecture on platform teams at the <a href="https://thedevconf.com/tdc/2020/sampaonline/trilha-devops" target="_blank">DevOps track</a> of The Developer Conference (TDC) São Paulo 2020, the largest software development event in Brazil.

<br/>

![TDC virtual session screenshot; in addition to Leonardo, two men and two women appear; one of the women is holding the book Team Topologies; Smiling people.]({{ site.baseurl }}/assets/figures/tdc2020.jpeg)

<br/>
<center>
<a href="https://pt.slideshare.net/leonardoferreiraleite/times-plataformatdc2020" class="button" target="_blank">Slides here (in Portuguese)</a>
</center>
<br/>

Synopsis of the lecture:

*How to organize (human) interactions between devs and ops in a context of continuous delivery? In a survey of 46 people, from 44 companies, in 8 countries, in my doctorate at USP, I identified 4 organizational structures. Among them, we highlight what we called **platform teams**: the infra team provides highly automated services to empower developers, who in turn become responsible for the operation of their services. Despite the challenges, this scheme contributes to high delivery performance. In this lecture, you will understand what a platform team is and its consequences; in addition to my experience at Serpro, I will discuss this approach for companies.*

<br/>

![TDC logo]({{ site.baseurl }}/assets/figures/tdc-logo.jpg)
