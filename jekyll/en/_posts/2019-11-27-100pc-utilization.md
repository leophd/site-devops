---
layout: post
title: "100% of utilization increases lead time"
author: "Leonardo Leite"
lang: en
lang-ref: 100pc-utilization
---

Excerpt from the book **Accelerate** [1]:

<blockquote>
Queue theory in math tell us that as utilization approaches 100%, 
lead times approach infinity - in other words, once you get to 
very high levels of utilization, it takes teams exponentially 
longer to get anything done.
</blockquote>

Obs: lead time is a measure of how fast work can be completed.

One clue why this happens: when "resources" (workers) are 100% busy and committed to given deadlines, it is impossible to handle (unexpected) incidents **and** keep the deadlines. And (unexpected) incidents happen.

-----------------

[1] Nicole Forsgren, Jez Humble, and Gene Kim. 2018. **Measuring Performance.** In **Accelerate: The Science of Lean Software and DevOps: Building and Scaling High Performing Technology Organizations.** IT Revolution Press.
