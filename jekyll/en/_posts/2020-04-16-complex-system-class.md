---
layout: post
title: "DevOps at the Laboratory of Complex Computational Systems"
author: "Leonardo Leite"
lang: en
lang-ref: complex-systems-2020
hiden: false
published: true
---

I taught a class about DevOps in 2020, 16th October, for the Laboratory of Computational Complex Systems at the University of São Paulo (USP). This post is just for sharing the slides used in the class.

The class is about DevOps, covering mainly: 

* culture, 
* continuous delivery, 
* continuous integration, 
* run-time practices, and
* microservices.


<br/>
<center>
<a href="https://pt.slideshare.net/leonardoferreiraleite/sistemas-complexosdevops20200416" class="button" target="_blank">Slides here (partially in Portuguese)</a>
</center>
<br/>

