---
layout: post
title: "Talking about Continuous Integration in p de Podcast"
author: "Leonardo Leite"
lang: en
lang-ref: ic-p-de-podcast
hiden: false
published: true
---

On October 9th, I had the pleasure of talking about Continuous Integration with Marcio Frayze and Julianno Martins on "p de Podcast", a podcast about software architecture.

![p de Podcast logo]({{ site.baseurl }}/assets/figures/p-de-podcast.png)

<br/>
<center>
<a href="https://open.spotify.com/episode/7ydeQwAw9Kk8HeMl03699o" class="button" target="_blank">Access via Spotify (Portuguese)</a>
</center>
<br/>



