---
layout: post
title: "Our complete theory on organizational structures of software teams has just been published!"
author: "Leonardo Leite"
lang: en
lang-ref: tse-publication
hiden: false
---

We are happy to announce that our article **A theory of organizational structures for development and infrastructure professionals** [has been published in IEEE Transactions on Software Engineering](https://ieeexplore.ieee.org/document/9864071){:target="_blank"}, the most reputable journal in the software engineering field. 

<a href="https://ieeexplore.ieee.org/document/9864071" target="_blank">
![Screenshot of the IEEE Xplore website with the official publication page]({{ site.baseurl }}/assets/figures/tse-publication.png)
</a>

<p><center><i>
Figure 1 - Our paper published by IEEE.
</i></center></p>

In this paper, we report conditions, causes, avoidance reasons, consequences, and contingencies associated with each organizational structure of [our taxonomy]({{ site.baseurl }}{% post_url en/2021-06-20-structures-digest %}.html){:target="_blank"}. Such an explanatory dimension of our theory (previously just a descriptive taxonomy), sought to unfold why different organizations adopt different structures. [You can check a summary of the paper here]({{ site.baseurl }}{% post_url en/2022-04-06-strong-codes %}.html){:target="_blank"}.


<br/>
<center>
<a class="button" href="https://www.techrxiv.org/articles/preprint/A_theory_of_organizational_structures_for_development_and_infrastructure_professionals/19210347" target="_blank">Download the full paper here</a>
</center>
<br/>






