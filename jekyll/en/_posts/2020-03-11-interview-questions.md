---
layout: post
title: "Interview questions"
author: "Leonardo Leite, Fabio Kon, Paulo Meirelles"
lang: en
lang-ref: interview-questions
---

This page presents the set of questions that we used to guide our semi-structured interviews to find out how companies are structuring departments toward continuous delivery. According to Adams[1], "Conducted conversationally with one respondent at a time, the semi-structured interviews (SSI) employs a blend of closed - and open - ended questions, often accompanied by follow - up why or how questions. The dialogue can meander around the topics on the agenda — rather than adhering slavishly to verbatim questions as in a standardized survey — and may delve into totally unforeseen issues."

## Starting the interview

* Research objective.
* Audio recording: kept secret.
* In our publications interviewed companies and people will be anonymous.
* Research results will be sent.

## Main questions

* Please, tell me about your company, your role within your company, and the project in which are currently working.
* Who is responsible for deploying the solution in production?
* In a new project, who builds the new environment?
* And about the Cloud? PaaS / serverless?
* Who is in charge for non-functional requirements?
  * Integrity, availability, flow rate, response time.
  * Capacity planning, load balancing, overload management, timeout and retrials configuration.
  * Deploys without unavailability.
* Who is responsible for configuring monitoring?
* Who is responsible for tracking the monitoring?
* Who is on-call?
  * After-hours frequency
  * Blameless post-mortem?

### Delivery performance

* Time from commit to production
  * < 1h	/    1 week < t < 1 month	/     > 1 month
* Deployment frequency
  * Under demand (many times per day)
  * 1 per week < f < 1 per month
  * f > 1 per month
* Mean time for repair
  * < 1h    /    < 1 day    /    1 day < t < 1 week  /    > 1 week
* Frequency of failures (% of deliveries)
  * 0 - 1% / 1 - 15% / 15 - 30% / 30 - 50% / More than 50%
* Reasons for the above results
* What would you change in this work system of your team/company?
* Why is it hard to implement such changes?
* Why did this problem arise? Why do they remain?



## Specific questions

(Asked depending on the situation)

* Inter-team communication; problem?
  * Little communication? Over communication?
* Are different teams aligned (committed to the project)?
  * How do this happen?
  * Consequences
* Clear definition of responsibilities?
  * If not, any problem?
  * If yes, long hand-offs?
* DevOps team?
* DevOps role?
* If there are no “dedicated ops”: how do software engineers improve their skills on infrastructure?
  * Were they hired with such skills?
  * Everyone in the team knows about infrastructure?
  * How people are chosen for “knowing more” about infrastructure.
*Is there any kind of incentive from the company about continuous education?
community of practices
  * FLOSS
  * internships
  * teams mutation
  * timeshare for studying
* If a team needs knowledge/skills that they do not have, what to do?
  * Study with team mate.
  * Search for help in another teams
  * Search for in-home specialist
  * Search for external consultant
* Does each team have their own specialists? Are there shared specialists?
  * Sharing policy
  * Handoffs problems

## Extra questions

* DBA
  * Is there production problems related to database?
* Security experts
  * Developers’ security awareness 
  * Were vulnerabilities found in production?
* Microservices


## Finishing the interview

* Do you have any question for me or any concluding remarks?

-------------------

[1] William C. Adams. 2010.   **Conducting semi-structured interviews**. In Handbook of Practical Program Evaluation (3rd ed.). Jossey-Bass
