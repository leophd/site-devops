---
layout: post
title: "Class about DevOps and Software Architecture in the Lab XP"
author: "Leonardo Leite"
lang: en
lang-ref: devops-architecture
hiden: false
published: true
---

One year ago, at August 2019, I supported prof. Thatiane de Oliveira Rosa in one class about software architecture, talking about “DevOps and Software Architecture”. The class was to undergraduate and graduate students, in the course Extreme Programming Laboratory, taught by prof. Alfredo Goldman.

Presentation available in Portuguese.

