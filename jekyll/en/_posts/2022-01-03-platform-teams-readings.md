---
layout: post
title: "Recommended readings on platform teams"
author: "Leonardo Leite"
lang: en
lang-ref: platform-teams-readings
---

![Figure shows an operator and a developer, each one inside a different circle. The operator provides an interface that is consumed by the developer (UML notation).]({{ site.baseurl }}/assets/figures/platform-team.png)

<center><i>
Figure 1 - The platform team provides automated services to be used, with little effort, by developers (the platform API mediates the interaction between developers and the platform team). In this way, supported by the platform, developers operate their services. Also, the platform team and developers are open to hear and support each other.
</i><p/></center>

* <a href="https://www.thoughtworks.com/en-us/radar/techniques/platform-engineering-product-teams" target="_blank">Platform engineering product teams.</a> In Thoughtworks Technology Radar. 2017--2021
* Evan Bottcher. <a href="https://martinfowler.com/articles/talk-about-platforms.html" target="_blank">What I Talk About When I Talk About Platforms.</a> In Martin Fowler's blog. 2018.
* Matthew Skelton and Manuel Pais. <a href="https://teamtopologies.com/book" target="_blank">Team Topologies: Organizing Business and Technology Teams for Fast Flow.</a> Book. IT Revolution. 2019.
* Leonardo Leite, Fabio Kon, Gustavo Pinto and Paulo Meirelles. <a href="{{ site.baseurl }}/2020-03-17/platform-teams.html" target="_blank">Platform Teams: An Organizational Structure for Continuous Delivery.</a> In: 6th International Workshop on Rapid Continuous Software Engineering (RCoSE), 2020.
* Alanna Brown, Michael Stahnke and Nigel Kersten. <a href="https://www2.circleci.com/2020-state-of-devops-report.html" target="_blank">2020 State of DevOps Report</a>. 2020.
* Cristóbal García and Chris Ford.  <a href="https://martinfowler.com/articles/platform-prerequisites.html" target="_blank">Mind the platform execution gap.</a> In Martin Fowler's blog. 2021.
 

