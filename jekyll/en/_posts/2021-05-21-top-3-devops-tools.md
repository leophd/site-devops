---
layout: post
title: "The top 3 DevOps tools"
author: "Leonardo Leite"
lang: en
lang-ref: top-3-devops-tools
hiden: false
published: true
---

In this world, there are so many DevOps tools. It's impossible to learn all of them. So, "which ones should I prioritize to study?", may you ask.

![The DevOps Periodic Table with a giant thinking emoji; in the table, it's like each DevOps tool corresponds to a chemical element.]({{ site.baseurl }}/assets/figures/which-devops-tools.png)

<p><center><i>
Figure 1 - The DevOps Periodic Table from XebiaLabs
</i></center></p>


Well, if we focus on tools related to infrastructure provisioning, I have some grounded advice...

In the context of my PhD research, I have conducted several interviews about DevOps with IT professionals (both in development and infrastructure). So far, I have interviewed 65 professionals in 45 companies (in 8 different countries, with different sizes, in a variety of business domains). The focus of these interviews is in human interactions, not in tooling. Nonetheless, some discussion on tools emerged. 

In this context, the three most discussed DevOps tools playing a transformational role in the organizations were:

* Kubernetes

* Kafka

* Terraform

![Logos of Kubernetes, Kafka, and Terraform]({{ site.baseurl }}/assets/figures/top-3-tools.png)

<p><center><i>
Figure 2 - The top 3 DevOps tools
</i></center></p>


Therefore, if you aspire to track the infrastructure path, studying these three technologies brings higher chances of returns over the investment!




