---
layout: post
title: "Finally I understood Bundler and Gemfile!"
author: "Leonardo Leite"
lang: en
lang-ref: bundler
---

Avoiding the *"it works (only) on my machine"* effect is a critical goal of DevOps. And adequately using a system for managing the dependencies of your application is a relevant part of it!

My background in management dependency tools is mostly with Maven for Java and Pip for Python. Once in a while
, I have also done some stuff in Ruby, and I confess I had a hard time using Bundler since I did not understand well some details like: why having `Gemfile.lock`; whether `Gemfile.lock` should be versioned or not; why using `bundle exec` to execute commands; and (since I did not use `bundle exec`) how different applications coexist in the same machine using different library versions.

So, by making this site, finally I found the bits of documentation that elucidated things to me:

`bundle-install` - *Install the dependencies specified in your Gemfile*

*Install the gems specified in your Gemfile. If this is the first time you run bundle install (and a Gemfile.lock does not exist), Bundler will fetch all remote sources, resolve dependencies and install all needed gems.*

*If a Gemfile.lock does exist, and you have not updated your Gemfile, Bundler will fetch all remote sources, but use the dependencies specified in the Gemfile.lock instead of resolving dependencies.*

*If a Gemfile.lock does exist, and you have updated your Gemfile, Bundler will use the dependencies in the Gemfile.lock for all gems that you did not update, but will re-resolve the dependencies of gems that you did update.*

[Original docs on bundle install](https://bundler.io/v2.0/man/bundle-install.1.html){:target="_blank"}

Obs: therefore, I conclude the `Gemfile.lock` must be versioned so that every developer use the same library versions.

`bundle-exec` - *Execute a command in the context of the bundle*

*This command executes the command, making all gems specified in the Gemfile available to require in Ruby programs.*

[Original docs on bundle exec](https://bundler.io/v2.0/man/bundle-exec.1.html){:target="_blank"}

