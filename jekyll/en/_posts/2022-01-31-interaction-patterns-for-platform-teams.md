---
layout: post
title: "Interaction patterns for platform teams"
author: "Leonardo Leite"
lang: en
lang-ref: interaction-patterns-platform-teams
---

<!-- summary: For daily tasks, developers should not bother people in the platform team; they should use self-serviced services. Nonetheless, there are moments in which these teams must interact directly. Let's present these interaction patterns. #devops -->

In our research, we say platform teams and development teams are arranged according to the organizational structure of API-mediated departments. Here, organizational structure defines the division of labor and the interaction modes among these two groups -- the platform team (that is, the infrastructure team) and developers. In this post, let's focus on the issue of interactions.

The API-mediated structure is much more about the "mediated" than it's about the "API". This means that for daily tasks, developers should not ask directly or via ticket to infrastructure people to provide stuff. Developers must use the infrastructure in a self-service manner, i.e., using the automated platform. Such property is what makes API-mediated departments so powerful: in this way, the infrastructure team is no longer a bottleneck in the delivery path, and, thus, delivery performance increases.

![Figure shows an operator and a developer, each one inside a different circle. The operator provides an interface that is consumed by the developer (UML notation).]({{ site.baseurl }}/assets/figures/platform-team.png)

<center><i>
Figure 1 - The platform team provides automated services to be used, with little effort, by developers (the platform API mediates the interaction between developers and the platform team). In this way, supported by the platform, developers operate their services. 
</i><p/></center>

Nonetheless, there are moments in which these teams must interact directly. Let's now present these interaction patterns.

**Solving incidents together**: when hard-to-solve problems arise, the teams work together to circumvent the situation. However, the problem is initially tackled solely by the development team, which summons people in the platform team only if necessary.

**Providing consultancy to developers**: the platform is an inner product, and as a product, it comes with support. Therefore, the infrastructure team spends some time teaching the platform's best practices to developers. Documentation and mailing are approaches to alleviate peer-to-peer tutoring.

**Demanding new capabilities**: developers will eventually demand new capabilities from the platform (e.g., out-of-the-box monitoring), as usually customers require the evolution of any software product. The platform team may also be proactive and probe developers' highest needs through surveys. In one way or another, the infrastructure team curates the platform backlog.

**Conflicting**: course, conflicts among interacting groups will always exist. But this conflict is different from the traditional conflicts in a siloed structure. Without API-mediation, conflicts usually arise after a tragedy, focusing on blaming games. In a more collaborative setting, with a DevOps spirit, conflicts also arise regarding the division of labor: it's not clear who does what. Finally, in an API-mediated structure, we expect conflicts to precede production problems since they will be more around prioritization issues for the platform enhancement. They are, therefore, customer vs. provider conflicts, which happen for any software product.








