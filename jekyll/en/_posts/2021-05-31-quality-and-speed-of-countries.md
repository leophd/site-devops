---
layout: post
title: "Quality and speed of countries to produce software"
author: "Leonardo Leite"
lang: en
lang-ref: quality-and-speed-of-countries
hiden: false
published: true
---

In at least two of the interviews I conducted for my research, the interviewees expressed the idea that software companies in Germany care more about quality than companies in the USA, while those in the USA care more about it than companies in Brazil. On the other hand, such interviewees also claimed that Brazilian companies are more concerned with delivering fast than companies in the USA, while companies in the USA would care more about this speed than German companies.

![In quality: Germany > USA > Brazil. In speed: Brazil > USA > Germany]({{ site.baseurl }}/assets/figures/countries.png)

<p><center><i>
Figure 1 - Different countries caring differently for quality and speed in the software production
</i></center></p>

Another interviewee has reported that companies in Japan struggled to adopt the Agile mindset. In particular, it was hard to accept the release of a MVP, i.e., a non-complete product, what implies more weight in quality than in launch speed. But when we had this conversation, by the beginning of 2019, this problem was already overcome.

Off-course, such reports refer to the experience of such interviewees. But could it be true? Or would it be at least true that national culture could affect the way different countries develop software? If you have some international experience in software production, share at Twitter (@leonardofl) what do you think, please!

Moreover, regardless of your experience, share with us if you believe that quality and speed are indeed a trade-off!



