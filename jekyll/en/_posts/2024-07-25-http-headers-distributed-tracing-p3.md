---
layout: post
title: "HTTP headers for distributed tracing (observability) - Part 3: Grafana Tempo"
author: "Leonardo Leite"
lang: en
lang-ref: http-headers-distributed-tracing-pt3
hiden: false
---

This series of posts investigates the **HTTP headers** used to support **tracing**, which is one of the pillars of **observability** (also composed of logs and metrics). Although we focus on investigating these headers, these posts also serve as an introduction to distributed tracing and as a presentation of some technological alternatives for its implementation. In this post, we will analyze Grafana Tempo, a distributed trace visualization solution integrated with the already highly popular Grafana.

[The complete post]({{ site.baseurl }}{% post_url pt-br/2024-07-25-headers-http-rastro-distribuido-p3 %}.html){:target="_blank"} is available only in Portuguese.

## Post digest

Grafana Tempo seems to be a promising option for visualizing distributed transaction execution traces. Taking only its integration with the popular Grafana, it would be an alternative to consider. But Tempo also brings interesting innovations, mainly the trace query language (TraceQL). However, the use of Tempo must still be combined with another tracing tool to collect data in the application, data that must be sent to the Tempo backend.
