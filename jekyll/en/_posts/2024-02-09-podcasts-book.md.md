---
layout: post
title: "Talking about our book in podcasts and events"
author: "Leonardo Leite"
lang: en
lang-ref: podcasts-book
hiden: false
published: true
---

Since the publication of our book about DevOps, I had the opportunity of talking about it in the podcasts and events listed below.

## Podcast "Tech Leadership Rocks"

Episode #171 (Jan 28, 2024) - *Como se faz DevOps com Leonardo Leite*

![Screenshot of the podcast conversation]({{ site.baseurl }}/assets/figures/podcast-tech-leadership-rocks.png)

<br/>
<center>
<a href="https://techleadership.rocks/2024/01/28/como-se-faz-devops-com-leonardo-leite/" class="button" target="_blank">Audio and video here (in Portuguese)</a>
</center>
<br/>

## Podcast "Frontiers of Software Engineering"

Episode #44 (Mar 20, 2024) - *Como se faz DevOps, com Leonardo Leite, Paulo Meirelles e Fabio Kon*

![Episode publicity image]({{ site.baseurl }}/assets/figures/fes.jpeg)

<br/>
<center>
<a href="https://fronteirases.github.io/episodios/paginas/44" class="button" target="_blank">Audio and video here (in Portuguese)</a>
</center>
<br/>

## 1st Serpro Tech Meetup in 2024

In company event at Serpro (Mar 25, 2024).

![Captura de tela com os participantes do evento]({{ site.baseurl }}/assets/figures/meetup-serpro-livro.png)

## Talk in the WEB3DEV community

Conversation with Daniel Cukier (Apr 2, 2024).

![Screenshot of the talk]({{ site.baseurl }}/assets/figures/web3dev.png)

<br/>
<center>
<a href="https://www.youtube.com/watch?v=yEHrppowovE" class="button" target="_blank">Audio and video here (in Portuguese)</a>
</center>
<br/>

## Podcast "DevOps Culture"

Podcast "Cultura DevOps" T2EP3 - Como se faz DevOps?  (Apr 9, 2024)

![Episode publicity image]({{ site.baseurl }}/assets/figures/podcast-cultura-devops.png)

<br/>
<center>
<a href="https://www.objective.com.br/insights/podcast-cultura-devops-t2ep3-como-se-faz-devops/" class="button" target="_blank">Audio and video here (in Portuguese)</a>
</center>
<br/>

## Book launch at Google Campus for Startups

Book launch event at Google Campus for Startups (São Paulo, Apr 11, 2024).

![Photo with the authors of the book and the debaters at the event]({{ site.baseurl }}/assets/figures/lancamento-livro1.jpeg)

[More book launch pictures here!]({{site.baseurl}}{% post_url en/2024-04-25-book-launch%}.html){:target="_blank"}

## XIV IME-USP Computing Week

Lecture for undergraduate computer science students at IME USP (Apr 22, 2024).

![Photo of Leonardo speaking]({{ site.baseurl }}/assets/figures/xiv-semanaca-computacao-ime-usp.jpg)

## Book launch live

Book launch live on YouTube (Apr 23, 2024).

![Screenshot of the talk]({{ site.baseurl }}/assets/figures/live-lancamento-livro.png)

<br/>
<center>
<a href="https://www.youtube.com/live/vzsxxgEJcIc?si=osVe44YiTlVUt11S" class="button" target="_blank">Audio and video here (in Portuguese)</a>
</center>
<br/>

## Seminar at Unifesp

Seminar for Unifesp postgraduate students (Apr 29, 2024), invited by professor Tiago Silva.

![Seminar screenshot]({{ site.baseurl }}/assets/figures/seminario-unifesp-2024-04-29.png)

## Recording video for Alura's DevOps training

Recording at Alura studios with Lucas Ribeiro Mata and Fabio Kon. Chat relating DevOps technologies to the book's content. 

![Lucas, Leonardo, and Fabio sitting in armchairs in a recording studio; In the background, a light board with "DevOps" written on it.]({{ site.baseurl }}/assets/figures/recording-at-alura.jpeg)

## Seminar at UFC (Federal University of Ceará)

Seminar for UFC undergraduate and postgraduate students (May 7, 2024), invited by professor Carla Bezerra.

![Seminar screenshot]({{ site.baseurl }}/assets/figures/seminario-ufc-2024-05-07.png)

<br/>
<center>
<a href="{{ site.baseurl }}/assets/files/seminario-devops-ufc-2024-05-07.pdf" class="button" target="_blank">Slides here (in Portuguese)</a>
</center>
<br/>

## Podcast "DevOps Culture" (again!)

Podcast "Cultura DevOps" T2EP5 - Times de Plataforma  (Jun 14, 2024)

![Episode publicity image]({{ site.baseurl }}/assets/figures/podcast-cultura-devops2.png)

<br/>
<center>
<a href="https://www.objective.com.br/insights/podcast-cultura-devops-t2ep5-times-de-plataforma/" class="button" target="_blank">Audio and video here (in Portuguese)</a>
</center>
<br/>

