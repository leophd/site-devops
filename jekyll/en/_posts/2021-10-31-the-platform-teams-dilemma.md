---
layout: post
title: "Platform teams dilemmas"
author: "Leonardo Leite"
lang: en
lang-ref: platform-teams-dilemma
hiden: false
published: true
---

Platform teams are infrastructure teams that provide highly automated infrastructure services that can be self-serviced by developers for application deployment. The platform API mediates the interaction between development and infrastructure teams, enabling developers to autonomously operate (deploy, monitor, and so on) their services. Our research [1, 2] has shown that organizations with platform teams have better [delivery performance]({{ site.baseurl }}{% post_url en/2019-11-27-delivery-performance %}.html){:target="_blank"}. 

![Figure shows an operator and a developer, each one inside a different circle. The operator provides an interface that is consumed by the developer (UML notation).]({{ site.baseurl }}/assets/figures/platform-team.png)

<center><i>
Figure 1 - The platform team provides automated services to be used, with little effort, by developers.
</i><p/></center>

Although infrastructure platforms foster productivity, they are usually tailored to the organization's everyday use case. Developers of more innovative projects may feel constrained by the platform. An existing solution is letting developers bypass the platform abstraction and work directly with the underlying infrastructure when needed. This option leverages flexibility, but it may cost the "guardrails" preventing developers from unconsciously disrespecting best practices or company standards. 

Another solution for the platform-application mismatch is developers demanding platform evolution. Such a path benefits not only the demanding developers but all the platform clients. However, it may take some time for the platform team to release new versions.

Another related dilemma is: how abstract should be the infrastructure abstraction provided by the platform? The more abstract, easier and more productive is the platform usage. But again: at the cost of flexibility for the application architecture. 

Besides flexibility, a too high-level infrastructure API has another drawback: developers may not learn basic infrastructure notions and use the platform as a "magic thing." In such a scenario, developers may neglect quality because they trust too much in the platform: for any problem, they blame the platform, not knowing what to do, even for simple issues or problems in the application itself. To handle this issue, the organization should encourage developers to learn how to use the platform correctly and to improve their basic infrastructure skills.

Nonetheless, a premise of adopting a platform is that developers' knowledge on infrastructure could be shallow. This fact leads to another potential drawback: developers become focused on specific corporate needs -- learning a platform that does not exist outside the organization. So, developers do not learn more general and advanced infrastructure knowledge that could foster their careers. This is not a problem for the organization but for the workers' long-term careers. On the other hand, some people do not see problems here: some developers prefer to focus on coding and learn the bare minimum necessary on infrastructure. Moreover, one should consider the evolution of abstractions supporting software development. As today, usually, coders do not need to worry about transistors or machine instructions, the future must encapsulate in layers some concerns that today are exposed to developers.

In summary, deciding the abstraction level of the infrastructure platform and the possibility of bypassing such abstraction bring dilemmas. High abstraction may bring high productivity, low flexibility, and limitations for workers' careers. Such concerns are not necessarily reasons to avoid the adoption of platform teams. However, we hope people aware of such issues will discuss more assertively how to handle them.

<!--	
-- Tweet:
-- The platform teams dilemma - how much should the platform abstract the infrastructure? High abstraction may bring high productivity, low flexibility, and limitations for workers' careers.
-->
-----------------------------------------------------

[1] Leonardo Leite, Fabio Kon, Gustavo Pinto, Paulo Meirelles. [Platform Teams: An Organizational Structure for Continuous Delivery]({{ site.baseurl }}{% post_url en/2020-03-17-platform-teams %}.html){:target="_blank"}. In: 6th International Workshop on Rapid Continuous Software Engineering (RCoSE), 2020.

[2] Leonardo Leite, Gustavo Pinto, Fabio Kon, Paulo Meirelles. [The Organization of Software Teams in the Quest for Continuous Delivery: A Grounded Theory Approach]({{ site.baseurl }}{% post_url en/2021-06-19-organizational-structures-published %}.html){:target="_blank"}. Information and Software Technology, Vol. 139, 2021.
