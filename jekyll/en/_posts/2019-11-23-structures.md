---
layout: post
title: "Organizational Structures in Quest for Continuous Delivery"
author: "Leonardo Leite, Paulo Meirelles, and Fabio Kon"
lang: en
lang-ref: structures
hiden: true
published: false
---

The **DevOps movement**, as vividly portrayed in the novel "The Phoenix Project," came up as a cultural shift to **break down the silos** in large organizations, better integrating development and operations teams through **collaboration**. But this collaboration can happen in different ways from an organizational perspective: developers and infrastructure specialists can be part of different departments or can be together in a single **cross-functional** team. With advancements in PaaS offers, it is possible even to envision developers themselves taking operations responsibilities.

Our current research, at IME-USP (University of São Paulo), aims to investigate how companies pursuing continuous delivery are
organizing their development and operations teams. We are taking this endeavor by interviewing software professionals to understand how things are really happening in the real world.

<!--So far, we have interviewed 28 professionals working in different countries; having distinct genders, ages, and roles (developers, infrastructure specialists, managers, designers); and working for organizations of different sizes and domains.-->

Based on the interviews conducted so far, we started the elaboration of a theory describing the **organizational structures** used by industry in the real world regarding how the work of developers and operators can be coordinated in the pursuit of continuous delivery. In this digest, we present our current understanding of such organizational structures: siloed departments, classical DevOps, cross-functional teams, and platform teams.

By advancing our research, we hope to provide a theory able to support organizations to design their organizational structures toward continuous delivery and to provide guidance on the consequences of a given chosen organization structure.

## Siloed departments

Siloed departments is "the pre-DevOps" structure existing in large organizations, presenting limited collaboration among departments and barriers for continuous delivery. This structure is considered to be the problem that DevOps came to solve.

![Figure shows an operator and a developer, each on inside a different circle. They have their backs to each other. Communication flows through letters, more from developer to operator than from operator to developer.]({{ site.baseurl }}/assets/figures/siloed-departments.png)

<center><i>
Figure 1 - With siloed departments, operators and developers are each one in their own bubbles. They do not interact directly too much and communication flows slowly by bureaucratic means (ticket systems).
</i><p/></center>

Some common characteristics:

* Developers and operators have well-defined and different roles. 
* Developers have a minimal vision of what happens in production.
* Monitoring and handling incidents are mostly done by the infrastructure team. 
* Developers often neglect non-functional requirements (NFR).
* Security can be seen as an infrastructure concern only. 
* DevOps initiatives are centered on adopting continuous integration tools rather than improving collaboration among silos.
* As a consequence, communication and collaboration among teams are hard. 
* The lack of proper automated tests can hinder [delivery performance]({{ site.baseurl }}{% post_url en/2019-11-27-delivery-performance %}.html){:target="_blank"} and reliability.

## Classical DevOps

The classical DevOps structure focuses on collaboration among developers and the infrastructure team. It is the way the book "The Phoenix Project" portrays DevOps.

![Figure shows an operator and a developer, each on inside a different circle. They are looking to each other and are holding hands (with some difficulty).]({{ site.baseurl }}/assets/figures/classical-devops.png)

<center><i>
Figure 2 - With classical DevOps, operators and developers in different departments seek to work together, even if not easy, by direct contact and close collaboration.
</i><p/></center>

Some common characteristics:

* Roles remain well-defined, although developers and operators are closer (e.g., for database management, infrastructure staff creates and tunes the database, whereas developers write queries and manage the schema), which fosters a culture of collaboration. 
* Usually, there are no conflicts regarding who is responsible for each task. 
* DevOps is achieved through a delivery pipeline. 
* NFR responsibilities are shared among developers and the infrastructure team. 
* The infrastructure staff is still in the front line of tracking monitoring and incident handling.
* Success of classical DevOps requires strong alignment among departments.

## Cross-functional teams

In this structure, developers and operators are part of the same functional team (related to some business objectives). It is more aligned with the Amazon motto "*You built it, you run it*," giving more freedom to the team along with a great deal of responsibility.

![Figure shows an operator and a developer, they are inside the same circle.]({{ site.baseurl }}/assets/figures/cross-functional-team.png)

<center><i>
Figure 3 - A cross-functional team contains both operators and developers.
</i><p/></center>

Some common characteristics:

* In this structure, a single team encompasses both developers and infrastructure specialists to take total responsibility for the life cycle of a set of services. 
* This structure is the one that most supports communication and collaboration among people with different skills. 
* Everyone in the team can be assigned to incident handling. 
* The challenge here is to guarantee that each unit has all the necessary skills.

## Platform teams

In this case, the infrastructure team (also called the platform team) provides highly-automated infrastructure services ("the platform") to empower product teams.

![Figure shows an operator and a developer, each on inside a different circle. The operator provides an interface that is consumed by the developer (UML notation).]({{ site.baseurl }}/assets/figures/platform-team.png)

<center><i>
Figure 4 - The platform team provides automated services to be used, with little effort, by developers. The platform team and developers are open to hear and support each other.
</i><p/></center>

Some common characteristics:

* The existence of a delivery platform minimizes the need for product teams having infrastructure specialists. 
* Product teams become decoupled from the members of the platform team. 
* Usually, the communication among the development team and the platform team happens when infrastructure members provide consulting for developers. 
* The product team is the first one to be called when there is an incident; the infrastructure people are escalated if the problem is related to some infrastructure service. 
* Although the product team becomes fully responsible for NFRs of its services, it is not a significant burden, since the platform abstracts away the underlying infrastructure and handles several NFR concerns.

## Just a few more considerations

Firstly, if one classifies a specific organization as adhering one of the above structures, it does not mean that the particular company will necessarily follow all the structure characteristics. Each organization is unique and has some adaptations for its context. Nonetheless, the model can assist reasoning on understanding organizations and on supporting decision-making regarding organizational changes.

For now, we believe our emerging model provides the following primary benefits:

* Offering concepts to distinguish clearly classical DevOps from cross-functional teams as alternative choices. In different scenarios, people use the word DevOps to describe both of them, and this can be confusing and disorient organization wishing to "adopt DevOps."
* Distinguishing the "platform team" as a separate organizational structure, making clear it has a different set of consequences when compared to classical DevOps or cross-functional teams.

Another distinctive characteristic of our model is that it is grounded on data retrieved from software professionals in the real world.





