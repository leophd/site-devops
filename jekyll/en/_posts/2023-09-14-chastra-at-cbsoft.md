---
layout: post
title: "Improving microservices operations - CBSoft 2023 Industry Track"
author: "Leonardo Leite"
lang: en
lang-ref: chastra-cbsoft-2023
hiden: false
---

We are excited to announce that our article *Microservices, why so hard? A patterns catalog to create good-to-operate services* was accepted to presentation in the Industry Track of CBSoft 2023 (Brazilian Conference on Software: Practice and Theory)!!!

![CBSoft 2023 logo]({{ site.baseurl }}/assets/figures/cbsoft2023-logo.png)

<p><center><i>
Figure 1 - CBSoft 2023 - Brazilian Conference on Software: Practice and Theory
</i></center></p>

## About the article

Authors: Leonardo Leite and Alberto Marianno - both are with Serpro (the Brazilian Federal Service for Data Processing).

Abstract. *Here we present a patterns catalog to improve service operation, thus reducing time to repair. This catalog is based on the practice of teams at Serpro, a federal state technology company. However, there is a considerable onus to apply all these patterns. There is also the possibility of misapplying or forgetting them. Thus, the catalog brings reflections on the difficulties in producing microservices (many services with constant updating). Such reflections can subsidize future research.*

With the use of [internal infrastructure platforms]({{site.baseurl}}{% post_url en/2020-03-17-platform-teams%}.html){:target="_blank"}, developers are now responsible for incidents involving their services. From the developers' point of view, this is an undesirable activity that interrupts the development flow, compromising delivery deadlines. So the question is: what can the development team do to increase its operational capacity? That is, how to develop a service so that it has fewer calls (incidents, questions, etc.) and that the calls can be responded to more quickly? It is this problem that Chastra, our catalog of patterns presented in the article, tackles.

<br/>
<center>
<a href="https://sol.sbc.org.br/index.php/cbsoft_estendido/article/view/26042" class="button" target="_blank">Download the full text here (in Portuguese)</a>
</center>
<br/>

It is also worth highlighting that repair time for incidents is a factor of [delivery performance]({{site.baseurl}}{% post_url en/2019-11-27-delivery-performance%}.html){:target="_blank"}, a capability that correlates with the achievement of organizations' high-level goals, both for commercial and non-commercial purposes.

The full pattern catalog itself is not yet public. But for CBSoft, we have already prepared a service exemplifying the implementing of some patterns from the catalog. For each implemented pattern, there is an automated test that performs the demonstration. [The Chastra Service is here](https://gitlab.com/serpro/chastra-service){:target="_blank"}.

![Interface screenshot (Swagger) of Chastra Service]({{ site.baseurl }}/assets/figures/chastra-service.png)

<p><center><i>
Figure 2 - Chastra Service, the service with demonstrations of patterns to well operate microservices
</i></center></p>

The patterns demonstrated in the Chastra Service at the moment are:

* readme with URLs
* setup doc
* local database
* swagger
* envvars checker
* binary suppression
* idempotence
* exception handler
* trace id
* health check
* audit
* business monitoring

## About the conference

The CBSoft 2023 Industry Track fosters cooperation between academia, the market, and the government with lectures, articles, and discussions. It aims to bring together professionals from the three sectors to share their knowledge and experiences with the community. It is an excellent opportunity to establish cooperative relationships between academic and professional communities. It is also an opportunity to establish applied research relationships, fostering market development and innovation through technology transfer.

The Brazilian Conference on Software: Practice and Theory (CBSoft) is an event annually promoted by the Brazilian Computing Society (SBC) aiming at fostering the exchange of experience among researchers and practitioners from industry and academia about the most recent research, tendencies, and theoretical and practical innovations on software. Held since 2010 as an aggregating conference of Brazilian symposia promoted by SBC on software, CBSoft has become one of the main conferences of the Brazilian scientific community on Computing.

The 14th edition of CBSoft will be held from September 25th to 29th at the Federal University of Mato Grosso do Sul, located in Campo Grande, MS, Brazil. It will integrate four traditional symposia annualy promoted by the Brazilian community on Software Engineering: the 37th Brazilian Symposium on Software Engineering (SBES), the premier conference on Software Engineering in Latin America; the 27th Brazilian Symposium on Programming Languages (SBLP); the 17th Brazilian Symposium on Software Components, Architectures, and Reuse (SBCARS); and the 8th Brazilian Symposium on Systematic Automated Software Testing (SAST).

![Photo of the event location, the Federal University of Mato Grosso do Sul]({{ site.baseurl }}/assets/figures/ufms.jpg)

<p><center><i>
Figure 3 - Federal University of Mato Grosso do Sul picture (Campo Grande, MS, Brazil)
</i></center></p>

The CBSoft programme will include technical sessions with presentation of scientific papers, keynotes by nationally and internationally renowned Brazilian and foreign researchers, discussion panels, workshops, and tool demonstrations. All these activites of interest of the Software Engineering community and fields related to the development of software systems go towards the diffusion of knowledge and discussion of important issues related to research, development, and innovation in both Brazil and the world.

