---
layout: post
title: "The DevOps conceptual maps"
author: "Leonardo Leite"
lang: en
lang-ref: devops-conceptual-maps
---

In our [Survey of DevOps Concepts and Challenges]({{ site.baseurl }}{% post_url en/2019-12-04-devops-survey-published %}.html){:target="_blank"} we thoroughly analyze 50 relevant peer-reviewed articles on DevOps. From this analysis grounded in the literature, we structured a set of relevant concepts on DevOps in the format of five **conceptual maps**.

In our article, we used these maps for:

* Studying and classify DevOps tools; 
* Identifying practical DevOps implications and segment such implications for engineers, managers, and researchers; and
* Discussing the main DevOps open challenges.

These conceptual maps can be used by practitioners (managers and engineers) for:

* Guiding their continuing education on DevOps (what to study next?);
* Grasping a conceptual view of DevOps tools, supporting the process of choosing new tools in the organization;
* Supporting reflection and continuous improvements (the DevOps journey retrospective).

## The DevOps conceptual maps

![DevOps overall conceptual map (You can read the map concepts by downloading the PDF version)]({{ site.baseurl }}/assets/figures/concepts-categories.png)

<center><i>
Figure 1 - DevOps overall conceptual map
</i><p/></center>

<br/><br/>

![Process conceptual map (You can read the map concepts by downloading the PDF version)]({{ site.baseurl }}/assets/figures/concepts-process.png)

<center><i>
Figure 2 - Process conceptual map
</i><p/></center>

<br/><br/>

![People conceptual map (You can read the map concepts by downloading the PDF version)]({{ site.baseurl }}/assets/figures/concepts-people.png)

<center><i>
Figure 3 - People conceptual map
</i><p/></center>

<br/><br/>

![Delivery conceptual map (You can read the map concepts by downloading the PDF version)]({{ site.baseurl }}/assets/figures/concepts-delivery.png)

<center><i>
Figure 4 - Delivery conceptual map
</i><p/></center>

<br/><br/>

![Runtime conceptual map (You can read the map concepts by downloading the PDF version)]({{ site.baseurl }}/assets/figures/concepts-runtime.png)

<center><i>
Figure 5 - Runtime conceptual map
</i><p/></center>

## The DevOps journey retrospective

Each turn is defined by the combination of a team member and one of the four big conceptual maps (process, people, delivery, and runtime). People and the maps are cycled sequentially. 

In her turn, the team member chooses a concept from the given map. All the team members draw one of the following cards:

* We achieved it and we are happy because of it.
* We achieved it, but it seems it doesn't matter too much.
* We didn't achieve it, but we want it!
* We didn't achieve it, but we don't see value on it.

So people discuss on this.

The turns go on until the end of the predefined timing box (e.g. one hour).

<br/><br/>

![The four cards for the DevOps retrospective]({{ site.baseurl }}/assets/figures/devops-retrospective-cards.png)

<center><i>
Figure 6 - Cards for the DevOps retrospective
</i><p/></center>

Example: 

* Team has Alice, Bob, Case, and Dony. 
* First round Alice has to choose a concept from the Process map.
* She selects "Short feedback cycles".
* Everyone draw their cards.
* Everyone agree that the team is not able to work in short feedback cycles...
* But Alice, Bob, and Case believe it's a necessary goal.
* On the other hand, Dony believes it's not feasible to bother the client with too much feedback requests.
* So the team discuss on this to find out what makes sense for the context team.
* Next turn: Bob has to choose one concept from the People map.
* And so on...

For this retrospective session you can use the maps in PDF version; it's better for zooming the map!

<br/>
<center>
<a href="{{ site.baseurl }}/assets/files/devops-conceptual-maps.zip" class="button">Download the conceptual maps in PDF format</a>
</center>
<br/>




