---
layout: post
title: "TDC São Paulo 2022 - Interaction between platform teams and developers: how should it be?"
author: "Leonardo Leite"
lang: en
lang-ref: tdc-2022
hiden: false
published: true
---

This week I had the honor and pleasure of giving a talk about platform teams on the <a href="https://thedevconf.com/tdc/2022/business/trilha-devops" target="_blank">DevOps</a> track of The Developer Conference (TDC) São Paulo 2022, the biggest event on software development in Brazil. I thank very much Fabricia Dora and all the coordination for welcoming me, as well as I thank all the support staff. It was fantastic! In particular, we were able to hold a highly interactive session, in which the audience shared their experiences with platform teams.

<br/>

![TDC photo panel with Leonardo lecturing, Leonardo alongside other participants and audience watching the lecture.]({{ site.baseurl }}/assets/figures/tdc2022-1.jpg)

<br/>

![TDC photo panel with Leonardo alongside other participants.]({{ site.baseurl }}/assets/figures/tdc2022-2.jpg)

<br/>

Synopsis of the lecture:

*Platform teams have been an option for dealing with the quandary of division of labor between infrastructure and development groups, resulting in the acceleration of continuous delivery. In this paradigm, developers autonomously use the platform to deploy and operate their services. So they don't need to interact with the platform team on a daily basis. However, even so, there is no point in having the platform without certain patterns of collaboration between the platform team and developers. These patterns of collaboration, presented in this lecture based on my doctoral research at USP, are what make the paradigm of platform teams healthy and sustainable.*

<br/>
<center>
<a href="https://pt.slideshare.net/leonardoferreiraleite/interao-entre-times-de-plataforma-e-desenvolvedores-como-deve-ser" class="button" target="_blank">Slides here (in Portuguese)</a>
</center>
<br/>

![Logo do TDC]({{ site.baseurl }}/assets/figures/slides-tdc-2022.png)

## Video

<iframe width="560" height="315" src="https://www.youtube.com/embed/luibfvF2lY0?si=DyOr-uH6sWi0YGhG" title="TDC talk video" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>


