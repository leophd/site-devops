---
layout: post
title: "Our DevOps Survey has just been published!"
author: "Leonardo Leite"
lang: en
lang-ref: devops-survey-published
---

We are happy to announce that our article **A Survey of DevOps Concepts and Challenges** has been published in the [Volume 52, Issue 6 of ACM Computing Surveys](https://dl.acm.org/citation.cfm?id=3359981){:target="_blank"}, a very reputable journal in our field. 

<br/>
<center>
<a href="https://dl.acm.org/doi/10.1145/3359981?cid=81413601887" target="_blank" class="button">Download the full text here</a>
</center>
<br/>


In this Survey, we thoroughly analyze 50 relevant peer-reviewed articles on DevOps, while also considering other sources of information, to:

* provide a definition of DevOps (<i>a collaborative and multidisciplinary effort within an organization to automate continuous delivery of new software versions, while guaranteeing their correctness and reliability</i>);
* develop a set of DevOps conceptual maps (Figure 1), which can assist people from industry and academia to make sense of the area;
* relate DevOps tools to the discovered DevOps concepts, emphasizing that tools adoption must be supported by conceptual decisions;
* discuss practical implications of DevOps for engineers, managers, and researchers;
* present four relevant and unresolved DevOps challenges:
  * How to re-design systems toward continuous delivery
  * How to deploy DevOps in an organization
  * How to assess the quality of DevOps practices in organizations
  * How to qualify engineers for DevOps practice

<br/>

![A diagram with 4 quadrants. Left side: engineering perspective. Right side: management perspective. 1st quadrant (top-right): people. 2nd quadrant (bottom-right): process. 3rd quadrant (bottom-left) delivery (dev related). 4th quadrant (top-left): runtime (ops related). ]({{ site.baseurl }}/assets/figures/concepts-categories.png)

<center><i>
Figure 1 - DevOps overall conceptual map (in the article, each quadrant expands to another map).
</i><p/></center>





