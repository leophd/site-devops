---
layout: post
title: "Feedback on the DevOps organizational structures"
author: "Leonardo Leite, Fabio Kon, Paulo Meirelles"
lang: en
lang-ref: feedback-form
---

After starting the formulation of our theory grounded on interviews with IT professionals, we sent the interviewees a feedback form to check the resonance of our emerging theory. We sent version 1 of the form to the 20 first interviewees and version 2 to the interviewees from 21 to 37.

## Feedback form version 1

This data will be completely anonymized before any publication. We're asking your name for the internal organization of the researchers only.

* Your name
* The classification proposed for your context, at the time of the interview, is adequate.
  * Strongly agree
  * Weakly agree
  * I do not know
  * Weakly disagree
  * Strongly disagree
* Especially in case of disagreement, please, explain why you disagree. Would you classify it differently?
* Would you add more DevOps organization structures to our model? Which Ones?
* The presented model is comprehensive regarding the ways companies are coordinating the work of developers and operations staff.
  * Strongly agree
  * Weakly agree
  * I do not know
  * Weakly disagree
  * Strongly disagree
* Would you have any other thoughts or criticism regarding the proposed model?

## Feedback form version 2

This data will be completely anonymized before any publication. We're asking your name for the internal organization of the researchers only.

* Your name
* The classification  of *organizational structure* proposed for your context, at the time of the interview, is adequate.
  * Strongly agree
  * Weakly agree
  * I do not know
  * Weakly disagree
  * Strongly disagree
* Especially in case of disagreement, please, explain why you disagree. Would you classify it differently?
* The classification  of *supplementary property(ies)* proposed for your context, at the time of the interview, is adequate.
  * Strongly agree
  * Weakly agree
  * I do not know
  * Weakly disagree
  * Strongly disagree
* Especially in case of disagreement, please, explain why you disagree. 
* Would you add more DevOps organization structures or supplementary properties to our model? Which Ones?
* The presented model is comprehensive regarding the ways companies are coordinating the work of developers and operations staff.
  * Strongly agree
  * Weakly agree
  * I do not know
  * Weakly disagree
  * Strongly disagree
* Would you have any other thoughts or criticism regarding the proposed model?
