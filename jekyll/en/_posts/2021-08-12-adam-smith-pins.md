---
layout: post
title: "Adam Smith and the work organization in the software production"
author: "Leonardo Leite"
lang: en
lang-ref: adam-smith
hiden: false
published: true
---

In our research, we found [four structures]({{ site.baseurl }}{% post_url en/2021-06-20-structures-digest %}.html){:target="_blank"} to organize the division of labor and the interaction between development and infrastructure professionals: segregated departments, collaborative departments, single departments, and API-mediated departments. We also found that the API mediation structure, with its platform teams, seems to deliver better results in terms of [delivery performance]({{ site.baseurl }}{% post_url en/2019-11-27-delivery-performance %}.html){:target="_blank"}. However, it is not entirely clear *why* this occurs. In this post, I provide an opinion on the matter.

![A 20 pounds bill figuring Adam Smith]({{ site.baseurl }}/assets/figures/20pounds.jpeg)

<center><i>
Figure 1 - "The division of labour in pin manufacturing: (and the great increase in the quantity of work that results)"
</i><p/></center>

<p/>

In a class on "Adam Smith's Pin Factory" [1], Professor Paulo Gala explains that productivity depends not on the amount of individual work, but on how production is organized. More specifically, productivity results from at least two factors present in the manufacturing: 1) **productive specialization** (or "increased workers' dexterity") and 2) **infrequent context switch**. Thus, 15 organized workers, each with their own task and with infrequent context switch, produce much more pins per capita than 15 workers making pins individually.

We know that caution is needed when comparing software production with factory production. The "software factory" metaphor has already caused problems in our "industry," which, by the way, even economists find it difficult to classify between industry and services. However, lessons can be learned; the *lean* [2] principles, for example, came from the automotive industry. Thus, I think it makes sense to explain the observed differences in productivity for the different organizational structures identified by our research (here assuming delivery performance as a productivity metric) in terms of the productivity factors pointed out by Adam Smith.

In **segregated departments**, we have a high level of specialization in the search for "resources" optimization. However, transferring work between areas, usually via the ticket system, implies a large amount of context switching for developers who interrupt their workflow when opening a ticket for the infrastructure area.

**Collaborative departments** seek to reduce waiting time, and thus context switching, through direct collaboration between development and infrastructure professionals. Here the hope is that a new culture of collaboration will make the difference. But without enough infrastructure professionals available to collaborate, which happens, no culture will operate a miracle: while one infrastructure professional collaborates with one developer, the other developer must wait. Furthermore, the collaboration system reduces specialization, as some responsibilities' separation becomes blurred.

In **single departments**, the attempt is to totally eliminate waiting time by making a single group responsible for both development and infrastructure. However, in the first place, this reduces the specialization of the group developing the software. Second, although the team doesn't have to wait for the infrastructure center to resolve the ticket, now it's the team itself that must switch context (from "dev mode" to "infra mode") and invest time in infrastructure issues.

Finally, the **mediation by APIs** structure seems to provide a good balance between specialization and context switching reduction. Specialization remains: the infrastructure team offers a platform encapsulating the infrastructure knowledge, while the developers develop the application. At the same time, context switching is low for developers as they operate their services autonomously through the infrastructure platform. We consider here that the platform provides an abstracted and easy way for developers to operate the infrastructure.

Note that for API-mediated departments, specialization and context switching reduction are not maximized. Each team focuses on a specialty, but developers must understand a minimum of infrastructure to operate the platform. On the other side, the platform team needs a development mindset and some dev skills as they offer the platform as a product. That is, there is less specialization than in segregated departments, but more specialization than in single departments. At the same time, developers must interrupt their coding and testing work to operate their services through the platform; however, the platform seeks to facilitate this operation as much as possible. That is, there is, at least, less context switching than in segregated departments.

In short:

* Segregated departments favor specialization, but increase context switch.
* Collaborative departments try to reduce context switching, but with some reduction of specialization.
* Single departments reduce context switching, but undermine specialization.
* API-mediated departments provide a better balance between specialization and reduction of context switching.

If you want to give your opinion, tweet to [@leonardofl](https://twitter.com/leonardofl){:target="_blank"}.

PS: maybe single departments with dedicated infrastructure professionals can maximize both specialization and context switch reduction. But, probably, such a structure is less adopted by organizations because it seems less cost-effective to hire one infrastructure professional for each development team.

-----------------

[1] Prof. Paulo Gala. **A fábrica de alfinetes de Adam Smith**. 2021. In Portuguese. [https://www.youtube.com/watch?v=MQ9uNGMatw8&t=1s](https://www.youtube.com/watch?v=MQ9uNGMatw8&t=1s){:target="_blank"}

[2] Mary Poppendieck, Tom Poppendieck. **Implementing Lean Software Development**: From Concept to Cash. Addison-Wesley Professional. 2006.

