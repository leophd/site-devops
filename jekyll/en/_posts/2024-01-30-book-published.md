---
layout: post
title: "Our book on DevOps is published!"
author: "Leonardo Leite"
lang: en
lang-ref: book-published
---

I'm happy to announce that our book, derived from our research results and entitled **Como se faz DevOps: Organizando pessoas, dos silos aos times de plataforma** (*How DevOps is done: Organizing people, from silos to platform teams*), has been published by Casa do Código (*"Code House"*), the most popular book publisher among Brazilian IT folks!

![Me holding the book]({{ site.baseurl }}/assets/figures/foto-livro-devops.jpg)


<br/>
<center>
<a href="https://www.casadocodigo.com.br/products/livro-como-se-faz-devops" target="_blank" class="button">Book purchase here</a>
</center>
<br/>

The book is written in Portuguese, targeting impact primarily on the Brazilian industry. Thus, although the book is based on academic research results, the writing (in addition to being in Portuguese) is adapted so that the text is useful and fluid for industry professionals (devs, ops and managers).

![The book cover]({{ site.baseurl }}/assets/figures/capa-livro-devops.jpg)



