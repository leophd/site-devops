---
layout: post
title: "Platform Teams: An Organizational Structure for Continuous Delivery"
author: "Leonardo Leite"
lang: en
lang-ref: platform-teams
---

We are happy to announce that our article **Platform Teams: An Organizational Structure
for Continuous Delivery** has been accepted for publication in the [6th International Workshop on Rapid Continuous Software Engineering (RCoSE 2020)](http://continuous-se.org/RCoSE2020/){:target="_blank"}.

After interviewing 27 IT professionals and carefully analyzing these conversations, we started the elaboration of a taxonomy with four patterns of organizational structures: (1) siloed departments, (2) classical DevOps, (3) cross-functional teams, and (4) platform teams. **Platform teams are infrastructure teams that provide highly automated infrastructure services that can be self-serviced by developers for the deployment of new services.** We observed that the platform team structure is the most distinctive classification of our taxonomy, and it has promising results regarding [delivery performance]({{ site.baseurl }}{% post_url en/2019-11-27-delivery-performance %}.html){:target="_blank"}. Thus, **in this paper, we focus on describing the platform team structure** (Figure 1). We are the first ones to present this emerging concept in an academic publication.

![Conceptual illustration of platform teams; diagram with boxes and arrows]({{ site.baseurl }}/assets/figures/platform-teams-concepts.png)

<center><i>
Figure 1 - Conceptual illustration of platform teams
</i><p/></center>

<br/>
<center>
<a href="https://dl.acm.org/doi/10.1145/3387940.3391455?cid=81413601887" class="button" target="_blank">Download the full text here</a>
</center>
<br/>

RCoSE workshop is held in conjunction with the International Conference on Software Engineering (ICSE), the most reputable conference on software engineering in the world. Our work has also been accepted for the <a href="https://conf.researchr.org/track/icse-2020/icse-2020-poster" target="_blank">ICSE 2020 Poster Track</a>.

ICSE and RCoSE were planned for May in South Korea. However, due to the Covid-19 crisis, the events were postponed to October.


