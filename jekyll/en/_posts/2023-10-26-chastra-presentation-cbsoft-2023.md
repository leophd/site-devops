---
layout: post
title: Presenting about "improving microservices operations" at CBSoft 2023 Industry Track"
author: "Leonardo Leite"
lang: en
lang-ref: chastra-presentation-cbsoft-2023
hiden: false
---

With joy, we presented our article *Microservices, why so hard? A patterns catalog to create good-to-operate services* in the Industry Track of CBSoft 2023 (Brazilian Conference on Software: Practice and Theory).

![Leonardo and Alberto, presenters of the work, with a CBSoft banner]({{ site.baseurl }}/assets/figures/chastra-cbsoftt2023-1.jpg)

<p><center><i>
Figure 1 - Leonardo Leite and Alberto Marianno, authors and presenters of the refereed paper
</i></center></p>

## Slides of the presentation

<iframe src="https://www.slideshare.net/slideshow/embed_code/key/rURWEECQtmIcqo?hostedIn=slideshare&page=upload" width="714" height="600" frameborder="0" marginwidth="0" marginheight="0" scrolling="no"></iframe>

## Pictures of the presentation

![Picture of the presentation]({{ site.baseurl }}/assets/figures/chastra-cbsoftt2023-2.jpg)

![Picture of the presentation]({{ site.baseurl }}/assets/figures/chastra-cbsoftt2023-3.jpg)

![Picture of the presentation]({{ site.baseurl }}/assets/figures/chastra-cbsoftt2023-4.jpg)

![Picture of the presentation]({{ site.baseurl }}/assets/figures/chastra-cbsoftt2023-5.jpg)

## About the article

Authors: Leonardo Leite and Alberto Marianno - both are with Serpro (the Brazilian Federal Service for Data Processing).

Abstract. *Here we present a patterns catalog to improve service operation, thus reducing time to repair. This catalog is based on the practice of teams at Serpro, a federal state technology company. However, there is a considerable onus to apply all these patterns. There is also the possibility of misapplying or forgetting them. Thus, the catalog brings reflections on the difficulties in producing microservices (many services with constant updating). Such reflections can subsidize future research.*

With the use of [internal infrastructure platforms]({{site.baseurl}}{% post_url en/2020-03-17-platform-teams%}.html){:target="_blank"}, developers are now responsible for incidents involving their services. From the developers' point of view, this is an undesirable activity that interrupts the development flow, compromising delivery deadlines. So the question is: what can the development team do to increase its operational capacity? That is, how to develop a service so that it has fewer calls (incidents, questions, etc.) and that the calls can be responded to more quickly? It is this problem that Chastra, our catalog of patterns presented in the article, tackles.

<br/>
<center>
<a href="https://sol.sbc.org.br/index.php/cbsoft_estendido/article/view/26042" class="button" target="_blank">Download the full text here (in Portuguese)</a>
</center>
<br/>

[More about our patterns catalog to improve microservices operation is in our previous post about the refereed paper]({{site.baseurl}}{% post_url en/2023-09-14-chastra-at-cbsoft%}.html){:target="_blank"}.

## About the conference

The CBSoft 2023 Industry Track fosters cooperation between academia, the market, and the government with lectures, articles, and discussions. It aims to bring together professionals from the three sectors to share their knowledge and experiences with the community. It is an excellent opportunity to establish cooperative relationships between academic and professional communities. It is also an opportunity to establish applied research relationships, fostering market development and innovation through technology transfer.

The Brazilian Conference on Software: Practice and Theory (CBSoft) is an event annually promoted by the Brazilian Computing Society (SBC) aiming at fostering the exchange of experience among researchers and practitioners from industry and academia about the most recent research, tendencies, and theoretical and practical innovations on software. Held since 2010 as an aggregating conference of Brazilian symposia promoted by SBC on software, CBSoft has become one of the main conferences of the Brazilian scientific community on Computing. The 14th edition of CBSoft was held in September 2023 at the Federal University of Mato Grosso do Sul, located in Campo Grande, MS, Brazil.

