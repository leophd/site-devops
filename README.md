# Site do grupo DevOps do CCSL

## Dia a dia

Subindo o site localmente: `./run.sh`

Pra atualizar no servidor:: `./deploy.sh`

## Execução local

Requisitos:

* Docker
* Docker Compose

Basta executar o script `run.sh`.

Obs: testado com Docker v20.10.21 + Docker Compose v2.12.2 e com Docker v23.0.6 + Docker Compose v.2.17.3.

