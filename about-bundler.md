
## Bundle install

`bundle-install` - Install the dependencies specified in your Gemfile

Install the gems specified in your Gemfile. If this is the first time you run bundle install (and a Gemfile.lock does not exist), Bundler will fetch all remote sources, resolve dependencies and install all needed gems.

If a Gemfile.lock does exist, and you have not updated your Gemfile, Bundler will fetch all remote sources, but use the dependencies specified in the Gemfile.lock instead of resolving dependencies.

If a Gemfile.lock does exist, and you have updated your Gemfile, Bundler will use the dependencies in the Gemfile.lock for all gems that you did not update, but will re-resolve the dependencies of gems that you did update. 

https://bundler.io/v2.0/man/bundle-install.1.html

Obs: portanto, Gemfile.lock deve ser versionado para que todos os desenvolvedores utilizem as mesmas versões das gems.

## Bundle exec

`bundle-exec` - Execute a command in the context of the bundle

This command executes the command, making all gems specified in the Gemfile available to require in Ruby programs.

